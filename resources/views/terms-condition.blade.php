@extends('layouts.terms&conditions-layout')

@section('title', 'Page Title')

@section('header')
  @parent

@endsection

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="account-title"><i class="fa fa-user" aria-hidden="true"></i> Terms & Conditions of WTA</h3>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-10 well">
				<p class="text-mutted">
					1.	Pay your entry fee at Week 1. Once registered and logged in players must make their first selection straight away. 
					<br><br>					
					2.	The competition will begin when the full amount of required participants has been reached.
					<br><br>
					3.	Each week, players must select one team to win. If the team selected wins, you go through; if the team selected loses or draws then you will be eliminated from the competition.
					<br><br>
					4.	Participants can only choose a team to win once during the entire competition. Once a team is selected the team will not be available for selection again. 
					<br><br>
					5.	A player can change their selection multiple times prior to the deadline, but not after the deadline has expired.
					<br><br>
					6.	All players must submit their selection 3 hours before the first fixture of the game week. 
					<br><br>
					7.	If there are any postponements due to weather or any unforeseen circumstances to one or more fixtures, the game week will still be valid and all results stand. Any team or teams involved in cancelled fixtures that have been selected by players will be recorded as victories and players will proceed to the following week. 
					<br><br>
					8.	Rounds of fixtures may take place on weekends or mid-week.
					<br><br>
					9.	The ‘Winner Takes All’ game is an “eliminative” competition. Participants are allowed to enter different games running at the same time. 
					<br><br>
					10.	The entrance fee to games will vary across the board, with competitions running with different sized jackpots.
					<br><br>
					11.	The winner of the game will receive the displayed jackpot shown on entrance.
					<br><br>
					12.	If a player is likely to be away during a game week and will miss the cut-off, it is solely the Players’ responsibility to make their selection and no intervention from the Administration can be expected or requested:
					<br><br>
					13.	If a player misses the stated deadline then the administrator will assign a default team. This will be the team that occupies the 20th position in the table - prior to first kick-off for the Game Week - regardless of whether they are playing Home or Away. If that team has already been selected/assigned then the team in 19th position will be assigned and so on.
					<br><br>
					14.	In the unlikely event that every fixture in the Gameweek results in a draw, that particular Gameweek will be rolled over and each participant will automatically progress through to the next round.
					<br><br>
					15.	If fixtures are cancelled & there is not 7 fixtures or more in a Gameweek, the competiton will be void & rolled over to the following week.
					<br><br>
					16.	In the event that there is no outright winner by the close of the season. The jackpot will be split equally amongst the remaining players. 
					<br><br>
					17.	The last Winner Takes All competition will take place 6 game weeks prior to the end of the season e.g. Premier League Gameweek 33 will be the last new competition of that season.
					<br><br>
					18.	If an individual self excludes before the first Gameweek of the competition begins, their money will be returned to them and they will be removed from the competition. 
					<br><br>
					19.	Rules are subject to change by the Administration at any given time.Any decision made by the Administration is final and not subject to appeal.
				</p>

			</div>
			<div class="col-md-1"></div>
		</div>
	</div>

  <script type="text/javascript">
    $( document ).ready(function() {    
    });   
  </script>
@stop