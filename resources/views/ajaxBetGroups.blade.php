            @section('ajaxContent')      
              @if (Auth::guest())
                <form role="form" id="playnow-form" action="{{ url('/registernow')}}" method="post">
              @else
                <form role="form" id="playnow-form" action="{{ url('/joinBetGroup') }}" method="post">
              @endif
                {{ csrf_field() }}
                  <input type="hidden" name="idGroup" value="" id="valueGroup">
                  <div class="form-group row" id="betting">
                    @for ($i = count($result['activeBetGroups'])-1; $i >= 0 ; $i--)                    
                    <div class="col-md-3">
                      <input type="radio" id="option{{ $i + 1 }}" name="bet" class="form-control" value="{{ $result['activeBetGroups'][$i]->amount }}" data-index="{{ $result['activeBetGroups'][$i]->id }}"><label for="option{{ $i + 1 }}">£ {{ $result['activeBetGroups'][$i]->amount }}</label>
                    </div>
                    @endfor
                  </div>
                  <div class="row">
                    @if (isset($result['groupDetails']) && ($result['groupDetails'][0]->amount > 0))
                    <div class="col-md-6">
                      <div class="current-participants">
                        <div class="progress">
                          <div class="progress-bar" role="progressbar" aria-valuenow="{{$result['groupDetails'][0]->current_participants}}" aria-valuemin="0" aria-valuemax="{{$result['groupDetails'][0]->participants_number}}" style="width:70%">
                          </div>
                        </div>
                      </div>
                      <div class="participants-overview">
                        <div class="participants-img">
                          <img src="{{asset('img/playnow/participants.png')}}" alt="">
                        </div>
                        <div class="participants-number">
                          <h3>Current Participants</h3>
                          <h2 id="current">{{$result['groupDetails'][0]->current_participants}}/{{$result['groupDetails'][0]->participants_number}}</h2>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="current-jackpot">
                         <div class="jackpot-overview">
                          <div class="jackpot-img">
                            <img src="{{asset('img/playnow/jackpot.png')}}" alt="">
                          </div>
                          <div class="jackpot-number">
                            <h3>Jackpot</h3>
                            <h2 id="jackpot">£ {{ (($result['groupDetails'][0]->participants_number)*($result['groupDetails'][0]->amount))*0.85 }}</h2>
                          </div>
                        </div>
                      </div>                     
                    </div>
                    @endif
                  </div>
                  @if(auth()->user() != null)
                  <div class="row">
                     <div class="button-fix">
                        <button type="submit" id="join" class="btn btn-default">Join Game</button>
                    </div>
                  </div>
                  @endif
                </form>
              </div>            
    <script type="text/javascript">
      $(document).ready(function() {        
        var amount = "{{ $result['groupDetails'][0]->amount }}";
        $("input[name=bet]").each(function(index){
          if($(this).attr("value") == amount){
            $(this).attr("checked","checked");
          }
        });
        $("#fixtures").click(function() {
          var url = "{{ url('/team-selection-guest') }}";
          window.location.href = url;
        });
        $("#join").click(function(e) {
          e.preventDefault();
          if($("input[name=bet]").is(":checked")){
            $("input[name=bet]").each(function(index){
              if($(this).is(":checked")){                
                $("#valueGroup").attr("value",$(this).data("index"));
              }
            });
            $("#playnow-form").submit();
          }          
        });

        $("input[name=bet]").on("click",function(){
          $("#ajaxContent").spin({lines: 10, length: 18, width: 8, radius: 15}, '#edbb1a');
          var index = $(this).data("index");
          amount = $(this).attr("value");
          $.ajax({
            type:"POST",
            data: { _token: '{{ csrf_token() }}', idGroup: index},
            dataType: 'json',
            url: "{{url('/resumeGroupTest')}}",
            success: function(result){              
              $("#ajaxContent").spin(false);
              $('#ajaxContent').html(result['result']);
              $("#betting").buttonset();
            }
          });
        });

      });
    </script>            
    @endsection