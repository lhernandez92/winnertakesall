@extends('layouts.playnowlayout')

@section('title', 'Page Title')

@section('header')
  @parent

@endsection

@section('content')

@endsection

@section('scripts')  

	<script type="text/javascript">
		$( document ).ready(function() {			 		
			$("#betting").buttonset();	
	        $("#fixtures").click(function() {
	          var url = "{{ url('/team-selection-guest') }}";
	          window.location.href = url;
	        });
	        $("#joinGame").click(function() {
	          var url = "{{ url('/playnow') }}";
	          window.location.href = url;
	        });			 				
		});		
	</script>
@stop