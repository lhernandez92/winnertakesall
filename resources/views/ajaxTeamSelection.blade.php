			@section('ajaxContent')      
			<div class="row">			
				<div class="select-team-container">
					<div class="col-lg-9 col-md-9">
						<div class="row" id="chargeSpin">							
							<div class="customNavigation">
							  	<div class="col-md-2">
							  		<a href="" class="btn prev"><i class="fa fa-chevron-left fa-2x" aria-hidden="true"></i></a>
							  	</div>
							  	<div class="col-md-8">
									<div class="select-team-header">
										<span>GameWeek 9-10 May 17:00</span>
									</div>
								</div>
							  	<div class="col-md-2">
							  		<a href="" class="btn next"><i class="fa fa-chevron-right fa-2x" aria-hidden="true"></i></a>
								</div>
							</div>
						</div>
						<div id="owl-demo" class="owl-carousel owl-theme">
						  	<form action="{{ url('/team-selection') }}" method="post" id="joinForm">	
						  	
						  	{{ csrf_field() }}				  		
						  	@if(isset($result['idBetGroup']))
						  		<input type="hidden" value="{{ $result['idBetGroup'] }}" name="idBetGroup" id="idBetGroup">
						  	@endif
						  	<input type="hidden" value="{{ $result['week'] }}" name="currentWeek" id="currentWeek">
						  	<input type="hidden" value="" name="type" id="type">
						  	<input type="hidden" value="{{ $result['weekPivot'] }}" name="weekPivot" id="weekPivot">							

				  			<div class="item">
						  	@for ($i = 0; $i < count($result['arrayFix']); $i++)
								<div class="row item-fix">
									<div class="cc-selector">
							    		<div class="col-sm-3 date-fix">
							    			<span class="match-date">{{ strtoupper(Date("d-M",strtotime($result['arrayFix'][$i]->date))) }} {{$result['arrayFix'][$i]->hour}}</span>
							    		</div>
							    		<div class="col-sm-3">
											<div class="team-left">
							    				<label class="team-name team-left selected-team" for="{{$result['teams'][$result['arrayFix'][$i]->id_home_team]}}">{{$result['arrayFix'][$i]->name_team($result['arrayFix'][$i]->id_home_team)}}</label>
							    			</div>
							    		</div>
								    	<div class="col-sm-3 flag-fix">
								    		<input id="{{$result['teams'][$result['arrayFix'][$i]->id_home_team]}}" class="left-flag" type="radio" name="match" value="{{$result['arrayFix'][$i]->id_home_team}}" {{ $result['arrayHomeTeam'][$i] }}>
									        <label class="drinkcard-cc {{$result['teams'][$result['arrayFix'][$i]->id_home_team]}} left-team" for="{{$result['teams'][$result['arrayFix'][$i]->id_home_team]}}"></label>
											<span class="vs">VS.</span>
									        <input id="{{$result['teams'][$result['arrayFix'][$i]->id_visit_team]}}" class="right-flag" type="radio" name="match" value="{{$result['arrayFix'][$i]->id_visit_team}}" {{ $result['arrayVisitTeam'][$i] }}>
									        <label class="drinkcard-cc {{$result['teams'][$result['arrayFix'][$i]->id_visit_team]}} right-team" for="{{$result['teams'][$result['arrayFix'][$i]->id_visit_team]}}"></label>
								    	</div>
										<div class="col-sm-3">
											<div class="team-right">
												<label class="team-name" for="{{$result['teams'][$result['arrayFix'][$i]->id_visit_team]}}">{{$result['arrayFix'][$i]->name_team($result['arrayFix'][$i]->id_visit_team)}}</label>
											</div>
										</div>
								    </div>
								</div>
					  		@endfor
					  		</div>
						  	</form>
						</div>
					</div>
					<div class="col-lg-3 col-md-3">
						<div class="row">
							<div class="barclays-brand">
								<img src="{{asset('img/teams/premier-league-logo.png')}}" alt="">
							</div>
						</div>
						<div class="row">
							<div class="table-leaderboard">
									<div class="top-club"></div>
									<div class="champions-club"></div>
									<div class="decline-club"></div>
									<div class="col-md-6">
										<div class="table-header table-data">
											<span> </span>
										</div>
									</div>
									<div class="col-md-3">
										<div class="table-data table-header">
											<span> </span>
										</div>
									</div>
									<div class="col-md-3">
										<div class="table-data table-header">
											<span> </span>
										</div>
									</div>
								<div class="table-row table-row-header">
									<div class="col-md-6 table-border">
										<div class="table-header table-data">
											<span>Club</span>
										</div>
										@for ($i = 0; $i < count($result['tableLeague']); $i++)
											<div class="table-data">
												<span>{{ str_replace("FC","",$result['tableLeague'][$i]->teamName) }}</span>
											</div>										
										@endfor
									</div>
									<div class="col-md-3 table-border">
										<div class="table-data table-header">
											<span>Pld</span>
										</div>
										@for ($i = 0; $i < count($result['tableLeague']); $i++)
											<div class="table-data">
												<span>{{ $result['tableLeague'][$i]->playedGames }}</span>
											</div>										
										@endfor										
									</div>
									<div class="col-md-3 table-border">
										<div class="table-data table-header">
											<span>Pts</span>
										</div>
										@for ($i = 0; $i < count($result['tableLeague']); $i++)
											<div class="table-data">
												<span>{{ $result['tableLeague'][$i]->points }}</span>
											</div>										
										@endfor										
									</div>
								</div>							
							</div>
						</div>
					</div>
				</div>
			</div>
			@if(isset($result['validateGroup']) && ($result['validateGroup'] != null))
				@if(($result['week'] == $result['weekPivot']) && ((($result['validateGroup']->first_match <= $result['week']) && ($result['validateGroup']->last_match >= $result['week'])) || (($result['validateGroup']->first_match == 0) && ($result['validateGroup']->last_match == 0))) && ($result['banderaButton']))
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="button-fix teams-fix">
			                    <button type="submit" id="join" class="btn btn-default"> Confirm Selection</button>
			                </div>
						</div>
					</div>
				</div>
				@endif		
			@endif
			<script type="text/javascript">
				$(document).ready(function() { 

				  	$("#join").click(function() {
			        	$("#joinForm").submit();
			        });
			        					
					$(".next").click(function(e){
						e.preventDefault();
						$("#type").attr("value","next");

						$("#chargeSpin").spin({lines: 10, length: 18, width: 8, radius: 15}, '#edbb1a');
							var index = $("#optGroups").val();
							var weekPivotHide = $("#weekPivot").val();
						$.ajax({
							type:"POST",
							data: { _token: '{{ csrf_token() }}', idGroup: index, weekPivot: weekPivotHide, type: 'next'},
							dataType: 'json',
							url: "{{url('/selectNextWeek')}}",
							success: function(result){           
								if(result['result'] == "msgErrorFailPrediction"){
						          	var url = "{{ url('/showMsgFailPrediction') }}";
						          	window.location.href = url;						
								}else{
									$("#ajaxContent").spin(false);
									$("#ajaxContent").html(result['result']);
									$("#optGroups").each(function(index){
										if(index == $("#idBetGroup").val()){
											$(this).attr("selected","selected");
										}
										var owl = $("#owl-demo");
										owl.owlCarousel({
											items:	1
										});						
									});
									var headerweek = $("#weekPivot").val();
									$("#headerWeek").text("/ Week " + headerweek);						
								}				   
							}
						});			
						//$("#joinForm").submit();
						//owl.trigger('owl.next');
					});
					$(".prev").click(function(e){
						e.preventDefault();			
						$("#type").attr("value","prev");

						$("#chargeSpin").spin({lines: 10, length: 18, width: 8, radius: 15}, '#edbb1a');
							var index = $("#optGroups").val();
							var weekPivotHide = $("#weekPivot").val();
						$.ajax({
							type:"POST",
							data: { _token: '{{ csrf_token() }}', idGroup: index, weekPivot: weekPivotHide, type: 'prev'},
							dataType: 'json',
							url: "{{url('/selectNextWeek')}}",
							success: function(result){    
								if(result['result'] == "msgErrorFailPrediction"){
						          	var url = "{{ url('/showMsgFailPrediction') }}";
						          	window.location.href = url;						
								}else{
									$("#ajaxContent").spin(false);
									$("#ajaxContent").html(result['result']);
									$("#optGroups").each(function(index){
										if(index == $("#idBetGroup").val()){
											$(this).attr("selected","selected");
										}
										var owl = $("#owl-demo");
										owl.owlCarousel({
											items:	1
										});						
									});
									var headerweek = $("#weekPivot").val();
									$("#headerWeek").text("/ Week " + headerweek);											
								}				          
							}
						});			
						//$("#joinForm").submit();
						//owl.trigger('owl.prev');
				  	});
				});
			</script>
			@endsection