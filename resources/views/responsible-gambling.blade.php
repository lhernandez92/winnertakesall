@extends('layouts.terms&conditions-layout')

@section('title', 'Page Title')

@section('header')
  @parent

@endsection

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="account-title"><i class="fa fa-users" aria-hidden="true"></i> <u>Responsible Gambling</u></h3>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-10 well">
				<p class="text-mutted">Winner Takes All would like you to enjoy your gambling experience. We recognise the need for a responsible attitude towards gambling and we have in place a number of measures to promote and ensure responsible gambling. We genuinely want our customers to have a great time on our site but not at the expense of other areas of their life so if ever you feel your gambling is not fun anymore please contact us via chat email or phone to discuss and one of our team will be available 24/7.</p>
			</div>
			<div class="col-md-1"></div>
		</div>
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10 well">
				<p class="text-mutted"><strong>Problem Gambling Check List</strong></p>
				<ul class="text-primary">
					<li>Do you stay away from work or studying in order to gamble?</li>
					<li>Do you gamble to escape from boredom or unhappiness?</li>
					<li>When gambling and you run out of money, do you feel lost and in despair and need to gamble again as soon as possible?</li>
					<li>Do you gamble until all your money is lost?</li>
					<li>Have you ever lied to cover up the amount of money or time you have spent gambling?</li>
					<li>Have you ever lied borrowed or stolen to maintain betting habits?</li>
					<li>Are you reluctant to spend gambling money on anything else?</li>
					<li>Have you lost interest in your family, friends or hobbies?</li>
					<li>After losing, do you feel you must try and win back your losses as soon as possible?</li>
				</ul>
			</div>
			<div class="col-md-1"></div>
		</div>
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10 well">
				<p>The more yes answers you have to these questions the more likely it is that you have a gambling problem.</p>
				<p>If you are concerned about your answers please look at the following sections for help with your gambling:</p>
				<ul class="text-primary">
					<li>Self Exclusion</li>
					<li>Setting Limits</li>
					<li>Taking Time Out</li>
				</ul><br>
				<p><strong>Organisations That Can Help</strong></p>
				<p> Gamcare: <a href="http://www.gamcare.org.uk" target="_blank" class="text-primary">www.gamcare.org.uk</a> 0808 8020 133</p>
				<p> Gamblers Anonymous: <a href="http://www.gamblersanonymous.org.uk" target="_blank" class="text-primary">www.gamblersanonymous.org.uk</a> 08700 50 88 80</p>
			</div>
			<div class="col-md-1"></div>
		</div>
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10 well">
				<p class="text-primary"><strong><u>Self Exclusion</u></strong></p>
				<p>Self Exclusion is for customers who would like help to prevent them gambling. Self-Exclusion has been designed to allow players to close accounts where needed and receive our support to prevent any bets being placed during the designated time period.</p>
				<p>At the end of the exclusion period we will an email confirmation from you in order to operate an account with us and following receipt of your instruction we will leave at least 24 hours before opening to give you a chance to reconsider your decision, if during this 24 hour period you change your mind you have the opportunity to self-exclude again.  During this time ‘cash out’ will be unavailable.</p>
				<p>Customers who make use of our self-exclusion policy will have their account closed and Winner Takes All will make every effort to prevent new accounts being opened by that customer for a minimum period of 6 months. Any customer wishing to be prevented from using his/her account should always use the self-exclusion facility, as accounts closed through other means may be re-opened at the discretion of WTA. Please be sure to use the email registered on your account so we can act on your request as quickly as possible. Self-exclusion will not affect transactions (including deposits, transfers, bets and withdrawals) made prior to exclusion period.  Once an account is closed for self-exclusion ‘cash out’ will not be available.</p>
			</div>
			<div class="col-md-1"></div>
		</div>
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10 well">
				<p class="text-primary"><strong><u>Setting Limits</u></strong></p>
				<p>It helps some customers to manage their gaming to be able to set a limit on the amount they can deposit into their Winner Takes All account over a specified time period. The deposit limit facility allows you to limit the amount of money that you are able to deposit in to your account weekly (Monday 00h to the subsequent Monday 00h UK Time). These amounts maybe reduced or increased at any time for the following week. You are able to set your deposit limits either when registering your account on the registration page or if you are an existing customer by going to "My Account" "Settings and limits". This will also show you the remaining amount available to deposit. Customers can have accounts across more than one brand on the same software, in the event that a customer requires a limit to carry across more than one brand, limits must be placed on every account individually to achieve the required total deposit limit.</p>
				<p>Please note: Deposit limits are set to the individual account number and not your payment method. In the event of a customer creating a duplicate account and not setting up deposit limits the customer shall be liable for any deposits on the account.</p>
			</div>
			<div class="col-md-1"></div>
		</div>
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10 well">
				<p class="text-primary"><strong><u>Taking Time Out</u></strong></p>
				<p>You may feel in control of your gambling but would like a break to consider your gambling or maybe you are in the middle of a busy time and don’t want to spend time or energy on gambling. If this is the case a time out period may help you.</p>
				<p>A time out period (a maximum of 6 weeks) is set and controlled by you, so if you want to re-open your account at any point you can do so, if you think you have a gambling problem then you should also consider self exclusion so we can help you with preventing future gambling within set periods.</p>
				<p>During a time out period (maximum of 6 weeks) we will stop contacting you with marketing offers and we will not open your account until we receive your instructions. Another way of taking some time away from on line gambling is to put a “blocker” on your computer: two well known ones are Gamblock (<a href="http://www.gamblock.com" target="_blank" class="text-primary">www.gamblock.com</a>) and Betfilter (<a href="http://www.betfilter.com" target="_blank" class="text-primary">www.betfilter.com</a>)</p>
				<p>A time out may not be the most appropriate option if you think you have a gambling problem. If this is the case then you should also consider self exclusion so we can help with preventing future gambling within set time periods to allow you time to take a complete break and consider you're gambling.</p>
			</div>
			<div class="col-md-1"></div>
		</div>
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10 well">
				<p><strong><u>Preventing Under Age Gambling</u></strong></p>
				<p>It is illegal for anybody under the age of 18 to open an account or to place a bet with WTA. We take our responsibilities in this area very seriously and undertake a number of steps to verify our customers are over 18. If we are unable to confirm a customer’s age we will suspend the customer’s accounts until we are able to ascertain the account holder is definitely over 18. In some circumstances documents will be required.</p>
				<p>Anyone under the age of 18 found using this site will have winnings forfeited and the account will be closed with immediate effect.</p>
				<p>We also advise that if your computer is used by children that you keep your account number/username confidential and you may wish to make use of parental control software such as "netnanny" (<a href="http://www.netnanny.com" class="text-primary" target="_blank">www.netnanny.com</a>) or "cybersitter" (<a href="http://www.cybersitter.com" class="text-primary" target="_blank">www.cybersitter.com</a>).</p>
				<p>Gamcare offer specialist advice to young people between 12 and 18 on their <a href="http://www.bigdeal.org.uk/" class="text-primary" target="_blank">Big Deal website</a></p>
			</div>
			<div class="col-md-1"></div>
		</div>
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10 well">
				<p><strong><u>Friends And Family</u></strong></p>
				<p>Sometimes gambling can have a negative impact on the family and friends of a problem gambler which may lead to need for support for those people close to the problem gambler. If you are in this situation you may well need help as well to try and be there to support your friend or family member, Gamcare can provide support in these circumstances and help you to cope with the difficulties you may encounter.</p><br>
				<p>*Please note that Winner Takes All takes no responsibility for the performance or quality of the software suggested on this page but not supplied by us.</p>
			</div>
			<div class="col-md-1"></div>
		</div>													
	</div>
@stop