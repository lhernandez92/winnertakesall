@extends('layouts.leaderboarduser-layout')

@section('title', 'Page Title')

@section('header')
  @parent

@endsection

@section('content')
<div class="container">
  <form action="{{ url('/leaderboardUser') }}" method="post" id="leaderboardForm">
  {{ csrf_field() }}
  <div class="row">
    <div class="col-md-4"><h3 class="select-team-title">Week {{$currentWeek}}</h3></div>
    <div class="col-md-4">
      <h3 class="select-team-title">
        <select name="optGroups" id="optGroups" class="form-control">             
          @if(count($arrayGroup) > 0)                            
            <option value="" selected>Select a bet group</option>
            @for ($i = 0; $i < count($arrayGroup); $i++)
              @if(isset($idBetGroup))
                @if($arrayGroup[$i]->id == $idBetGroup)
                  <option value="{{$arrayGroup[$i]->id}}" selected>Amount: £{{$arrayGroup[$i]->amount}} / group #: {{$arrayGroup[$i]->id}}</option>                
                @else
                  <option value="{{$arrayGroup[$i]->id}}">Amount: £{{$arrayGroup[$i]->amount}} / group #: {{$arrayGroup[$i]->id}}</option>
                @endif                                                
              @else
                @if($i == 0)
                  <option value="{{$arrayGroup[$i]->id}}" selected>Amount: £{{$arrayGroup[$i]->amount}} / group #: {{$arrayGroup[$i]->id}}</option>                
                @else
                  <option value="{{$arrayGroup[$i]->id}}">Amount: £{{$arrayGroup[$i]->amount}} / group #: {{$arrayGroup[$i]->id}}</option>
                @endif                                  
              @endif
            @endfor
          @else
            <option value="">There is not bet groups to show this season.</option>
          @endif
        </select>      
      </h3>          
    </div>
    <div class="col-md-4"></div>
  </div>
  <div id="table-players" class="contents">    
    <!--INICIA EL CONTENIDO DE LA TABLA DEL LEADERBOARD-->
    @for ($i = 0; $i < $countPaginator; $i++)
    <div class="row table-content">
      <div class="col-md-3 players-table-border">
        <div class="table-players-data">
          <span>Member \ Week</span>
        </div>
        <div class="table-players-data table-players-data-top">
          @if($arrayMembers[($i*10)][0]->winner != null)
            <img  class="leaderboard-icon" src="img/leaderboard/top-player-icon.png" alt="">
          @else
            <img  class="leaderboard-icon" src="img/leaderboard/player-icon.png" alt="">
          @endif
          <span>
            @if(isset($arrayMembers[($i*10)]))  
              {{$arrayMembers[($i*10)][0]->name}} {{$arrayMembers[($i*10)][0]->surname}}
            @endif
          </span>
        </div>
        <div class="table-players-data">
          @if($arrayMembers[(($i*10)+1)][0]->winner != null)
            <img  class="leaderboard-icon" src="img/leaderboard/top-player-icon.png" alt="">
          @else
            <img  class="leaderboard-icon" src="img/leaderboard/player-icon.png" alt="">
          @endif
          <span>
            @if(isset($arrayMembers[(($i*10)+1)]))  
              {{$arrayMembers[(($i*10)+1)][0]->name}} {{$arrayMembers[(($i*10)+1)][0]->surname}}
            @endif
          </span>
        </div>
        <div class="table-players-data">
          <img class="leaderboard-icon" src="img/leaderboard/player-icon.png" alt="">
          <span>
            @if(isset($arrayMembers[(($i*10)+2)]))  
              {{$arrayMembers[(($i*10)+2)][0]->name}} {{$arrayMembers[(($i*10)+2)][0]->surname}}
            @endif
          </span>
        </div>
        <div class="table-players-data">
          <img class="leaderboard-icon" src="img/leaderboard/player-icon.png" alt="">
          <span>
            @if(isset($arrayMembers[(($i*10)+3)]))  
              {{$arrayMembers[(($i*10)+3)][0]->name}} {{$arrayMembers[(($i*10)+3)][0]->surname}}
            @endif             
          </span>
        </div>
        <div class="table-players-data">
          <img class="leaderboard-icon" src="img/leaderboard/player-icon.png" alt="">
          <span>
            @if(isset($arrayMembers[(($i*10)+4)]))  
              {{$arrayMembers[(($i*10)+4)][0]->name}} {{$arrayMembers[(($i*10)+4)][0]->surname}}
            @endif
          </span>
        </div>
        <div class="table-players-data">
          <img class="leaderboard-icon" src="img/leaderboard/player-icon.png" alt="">
          <span>
            @if(isset($arrayMembers[(($i*10)+5)]))  
              {{$arrayMembers[(($i*10)+5)][0]->name}} {{$arrayMembers[(($i*10)+5)][0]->surname}}
            @endif
          </span>
        </div>
        <div class="table-players-data">
          <img class="leaderboard-icon" src="img/leaderboard/player-icon.png" alt="">
          <span>
            @if(isset($arrayMembers[(($i*10)+6)]))  
              {{$arrayMembers[(($i*10)+6)][0]->name}} {{$arrayMembers[(($i*10)+6)][0]->surname}}
            @endif
          </span>
        </div>
        <div class="table-players-data">
          <img class="leaderboard-icon" src="img/leaderboard/player-icon.png" alt="">
          <span>
            @if(isset($arrayMembers[(($i*10)+7)]))  
              {{$arrayMembers[(($i*10)+7)][0]->name}} {{$arrayMembers[(($i*10)+7)][0]->surname}}
            @endif
          </span>
        </div>
        <div class="table-players-data">
          <img class="leaderboard-icon" src="img/leaderboard/player-icon.png" alt="">
          <span>
            @if(isset($arrayMembers[(($i*10)+8)]))  
              {{$arrayMembers[(($i*10)+8)][0]->name}} {{$arrayMembers[(($i*10)+8)][0]->surname}}
            @endif
          </span>
        </div>
        <div class="table-players-data">
          <img class="leaderboard-icon" src="img/leaderboard/player-icon.png" alt="">
          <span>
            @if(isset($arrayMembers[(($i*10)+9)]))  
              {{$arrayMembers[(($i*10)+9)][0]->name}} {{$arrayMembers[(($i*10)+9)][0]->surname}}
            @endif
          </span>
        </div>
      </div>
      <div class="col-md-1 players-table-border">
        <div class="table-players-data">
          <span>1</span>
        </div>  
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[($i*10)][1][$i*10]))  
              {{$arrayMembers[($i*10)][1][$i*10]->selection_team}}
            @endif
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+1)][1][$i*10]))  
              {{$arrayMembers[(($i*10)+1)][1][$i*10]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+2)][1][$i*10]))  
              {{$arrayMembers[(($i*10)+2)][1][$i*10]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+3)][1][$i*10]))  
              {{$arrayMembers[(($i*10)+3)][1][$i*10]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+4)][1][$i*10]))  
              {{$arrayMembers[(($i*10)+4)][1][$i*10]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+5)][1][$i*10]))  
              {{$arrayMembers[(($i*10)+5)][1][$i*10]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+6)][1][$i*10]))  
              {{$arrayMembers[(($i*10)+6)][1][$i*10]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+7)][1][$i*10]))  
              {{$arrayMembers[(($i*10)+7)][1][$i*10]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+8)][1][$i*10]))  
              {{$arrayMembers[(($i*10)+8)][1][$i*10]->selection_team}}
            @endif            
          </span>
        </div>
        <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+9)][1][$i*10]))  
              {{$arrayMembers[(($i*10)+9)][1][$i*10]->selection_team}}
            @endif            
          </span>
        </div>
      </div>
      <div class="col-md-1 players-table-border">
        <div class="table-players-data">
          <span>2</span>
        </div>  
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[($i*10)][1][(($i*10)+1)]))  
              {{$arrayMembers[($i*10)][1][(($i*10)+1)]->selection_team}}
            @endif
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+1)][1][(($i*10)+1)]))  
              {{$arrayMembers[(($i*10)+1)][1][(($i*10)+1)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+2)][1][(($i*10)+1)]))  
              {{$arrayMembers[(($i*10)+2)][1][(($i*10)+1)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+3)][1][(($i*10)+1)]))  
              {{$arrayMembers[(($i*10)+3)][1][(($i*10)+1)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+4)][1][(($i*10)+1)]))  
              {{$arrayMembers[(($i*10)+4)][1][(($i*10)+1)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+5)][1][(($i*10)+1)]))  
              {{$arrayMembers[(($i*10)+5)][1][(($i*10)+1)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+6)][1][(($i*10)+1)]))  
              {{$arrayMembers[(($i*10)+6)][1][(($i*10)+1)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+7)][1][(($i*10)+1)]))  
              {{$arrayMembers[(($i*10)+7)][1][(($i*10)+1)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+8)][1][(($i*10)+1)]))  
              {{$arrayMembers[(($i*10)+8)][1][(($i*10)+1)]->selection_team}}
            @endif            
          </span>
        </div>
        <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+9)][1][(($i*10)+1)]))  
              {{$arrayMembers[(($i*10)+9)][1][(($i*10)+1)]->selection_team}}
            @endif            
          </span>
        </div>
      </div>
      <div class="col-md-1 players-table-border">
        <div class="table-players-data">
          <span>3</span>
        </div>  
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[($i*10)][1][(($i*10)+2)]))  
              {{$arrayMembers[($i*10)][1][(($i*10)+2)]->selection_team}}
            @endif
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+1)][1][(($i*10)+2)]))  
              {{$arrayMembers[(($i*10)+1)][1][(($i*10)+2)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+2)][1][(($i*10)+2)]))  
              {{$arrayMembers[(($i*10)+2)][1][(($i*10)+2)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+3)][1][(($i*10)+2)]))  
              {{$arrayMembers[(($i*10)+3)][1][(($i*10)+2)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+4)][1][(($i*10)+2)]))  
              {{$arrayMembers[(($i*10)+4)][1][(($i*10)+2)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+5)][1][(($i*10)+2)]))  
              {{$arrayMembers[(($i*10)+5)][1][(($i*10)+2)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+6)][1][(($i*10)+2)]))  
              {{$arrayMembers[(($i*10)+6)][1][(($i*10)+2)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+7)][1][(($i*10)+2)]))  
              {{$arrayMembers[(($i*10)+7)][1][(($i*10)+2)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+8)][1][(($i*10)+2)]))  
              {{$arrayMembers[(($i*10)+8)][1][(($i*10)+2)]->selection_team}}
            @endif            
          </span>
        </div>
        <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+9)][1][(($i*10)+2)]))  
              {{$arrayMembers[(($i*10)+9)][1][(($i*10)+2)]->selection_team}}
            @endif            
          </span>
        </div>
      </div>
      <div class="col-md-1 players-table-border">
        <div class="table-players-data">
          <span>4</span>
        </div>  
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[($i*10)][1][(($i*10)+3)]))  
              {{$arrayMembers[($i*10)][1][(($i*10)+3)]->selection_team}}
            @endif
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+1)][1][(($i*10)+3)]))  
              {{$arrayMembers[(($i*10)+1)][1][(($i*10)+3)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+2)][1][(($i*10)+3)]))  
              {{$arrayMembers[(($i*10)+2)][1][(($i*10)+3)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+3)][1][(($i*10)+3)]))  
              {{$arrayMembers[(($i*10)+3)][1][(($i*10)+3)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+4)][1][(($i*10)+3)]))  
              {{$arrayMembers[(($i*10)+4)][1][(($i*10)+3)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+5)][1][(($i*10)+3)]))  
              {{$arrayMembers[(($i*10)+5)][1][(($i*10)+3)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+6)][1][(($i*10)+3)]))  
              {{$arrayMembers[(($i*10)+6)][1][(($i*10)+3)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+7)][1][(($i*10)+3)]))  
              {{$arrayMembers[(($i*10)+7)][1][(($i*10)+3)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+8)][1][(($i*10)+3)]))  
              {{$arrayMembers[(($i*10)+8)][1][(($i*10)+3)]->selection_team}}
            @endif            
          </span>
        </div>
        <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+9)][1][(($i*10)+3)]))  
              {{$arrayMembers[(($i*10)+9)][1][(($i*10)+3)]->selection_team}}
            @endif            
          </span>
        </div>
      </div>
      <div class="col-md-1 players-table-border">
        <div class="table-players-data">
          <span>5</span>
        </div>  
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[($i*10)][1][(($i*10)+4)]))  
              {{$arrayMembers[($i*10)][1][(($i*10)+4)]->selection_team}}
            @endif
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+1)][1][(($i*10)+4)]))  
              {{$arrayMembers[(($i*10)+1)][1][(($i*10)+4)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+2)][1][(($i*10)+4)]))  
              {{$arrayMembers[(($i*10)+2)][1][(($i*10)+4)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+3)][1][(($i*10)+4)]))  
              {{$arrayMembers[(($i*10)+3)][1][(($i*10)+4)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+4)][1][(($i*10)+4)]))  
              {{$arrayMembers[(($i*10)+4)][1][(($i*10)+4)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+5)][1][(($i*10)+4)]))  
              {{$arrayMembers[(($i*10)+5)][1][(($i*10)+4)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+6)][1][(($i*10)+4)]))  
              {{$arrayMembers[(($i*10)+6)][1][(($i*10)+4)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+7)][1][(($i*10)+4)]))  
              {{$arrayMembers[(($i*10)+7)][1][(($i*10)+4)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+8)][1][(($i*10)+4)]))  
              {{$arrayMembers[(($i*10)+8)][1][(($i*10)+4)]->selection_team}}
            @endif            
          </span>
        </div>
        <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+9)][1][(($i*10)+4)]))  
              {{$arrayMembers[(($i*10)+9)][1][(($i*10)+4)]->selection_team}}
            @endif            
          </span>
        </div>
      </div>
      <div class="col-md-1 players-table-border">
        <div class="table-players-data">
          <span>6</span>
        </div>  
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[($i*10)][1][(($i*10)+5)]))  
              {{$arrayMembers[($i*10)][1][(($i*10)+5)]->selection_team}}
            @endif
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+1)][1][(($i*10)+5)]))  
              {{$arrayMembers[(($i*10)+1)][1][(($i*10)+5)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+2)][1][(($i*10)+5)]))  
              {{$arrayMembers[(($i*10)+2)][1][(($i*10)+5)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+3)][1][(($i*10)+5)]))  
              {{$arrayMembers[(($i*10)+3)][1][(($i*10)+5)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+4)][1][(($i*10)+5)]))  
              {{$arrayMembers[(($i*10)+4)][1][(($i*10)+5)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+5)][1][(($i*10)+5)]))  
              {{$arrayMembers[(($i*10)+5)][1][(($i*10)+5)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+6)][1][(($i*10)+5)]))  
              {{$arrayMembers[(($i*10)+6)][1][(($i*10)+5)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+7)][1][(($i*10)+5)]))  
              {{$arrayMembers[(($i*10)+7)][1][(($i*10)+5)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+8)][1][(($i*10)+5)]))  
              {{$arrayMembers[(($i*10)+8)][1][(($i*10)+5)]->selection_team}}
            @endif            
          </span>
        </div>
        <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+9)][1][(($i*10)+5)]))  
              {{$arrayMembers[(($i*10)+9)][1][(($i*10)+5)]->selection_team}}
            @endif            
          </span>
        </div>
      </div>
      <div class="col-md-1 players-table-border">
        <div class="table-players-data">
          <span>7</span>
        </div>  
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[($i*10)][1][(($i*10)+6)]))  
              {{$arrayMembers[($i*10)][1][(($i*10)+6)]->selection_team}}
            @endif
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+1)][1][(($i*10)+6)]))  
              {{$arrayMembers[(($i*10)+1)][1][(($i*10)+6)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+2)][1][(($i*10)+6)]))  
              {{$arrayMembers[(($i*10)+2)][1][(($i*10)+6)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+3)][1][(($i*10)+6)]))  
              {{$arrayMembers[(($i*10)+3)][1][(($i*10)+6)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+4)][1][(($i*10)+6)]))  
              {{$arrayMembers[(($i*10)+4)][1][(($i*10)+6)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+5)][1][(($i*10)+6)]))  
              {{$arrayMembers[(($i*10)+5)][1][(($i*10)+6)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+6)][1][(($i*10)+6)]))  
              {{$arrayMembers[(($i*10)+6)][1][(($i*10)+6)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+7)][1][(($i*10)+6)]))  
              {{$arrayMembers[(($i*10)+7)][1][(($i*10)+6)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+8)][1][(($i*10)+6)]))  
              {{$arrayMembers[(($i*10)+8)][1][(($i*10)+6)]->selection_team}}
            @endif            
          </span>
        </div>
        <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+9)][1][(($i*10)+6)]))  
              {{$arrayMembers[(($i*10)+9)][1][(($i*10)+6)]->selection_team}}
            @endif            
          </span>
        </div>
      </div>
      <div class="col-md-1 players-table-border">
        <div class="table-players-data">
          <span>8</span>
        </div>  
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[($i*10)][1][(($i*10)+7)]))  
              {{$arrayMembers[($i*10)][1][(($i*10)+7)]->selection_team}}
            @endif
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+1)][1][(($i*10)+7)]))  
              {{$arrayMembers[(($i*10)+1)][1][(($i*10)+7)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+2)][1][(($i*10)+7)]))  
              {{$arrayMembers[(($i*10)+2)][1][(($i*10)+7)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+3)][1][(($i*10)+7)]))  
              {{$arrayMembers[(($i*10)+3)][1][(($i*10)+7)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+4)][1][(($i*10)+7)]))  
              {{$arrayMembers[(($i*10)+4)][1][(($i*10)+7)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+5)][1][(($i*10)+7)]))  
              {{$arrayMembers[(($i*10)+5)][1][(($i*10)+7)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+6)][1][(($i*10)+7)]))  
              {{$arrayMembers[(($i*10)+6)][1][(($i*10)+7)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+7)][1][(($i*10)+7)]))  
              {{$arrayMembers[(($i*10)+7)][1][(($i*10)+7)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+8)][1][(($i*10)+7)]))  
              {{$arrayMembers[(($i*10)+8)][1][(($i*10)+7)]->selection_team}}
            @endif            
          </span>
        </div>
        <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+9)][1][(($i*10)+7)]))  
              {{$arrayMembers[(($i*10)+9)][1][(($i*10)+7)]->selection_team}}
            @endif            
          </span>
        </div>
      </div>
      <div class="col-md-1 players-table-border">
        <div class="table-players-data">
          <span>9</span>
        </div>  
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[($i*10)][1][(($i*10)+8)]))  
              {{$arrayMembers[($i*10)][1][(($i*10)+8)]->selection_team}}
            @endif
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+1)][1][(($i*10)+8)]))  
              {{$arrayMembers[(($i*10)+1)][1][(($i*10)+8)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+2)][1][(($i*10)+8)]))  
              {{$arrayMembers[(($i*10)+2)][1][(($i*10)+8)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+3)][1][(($i*10)+8)]))  
              {{$arrayMembers[(($i*10)+3)][1][(($i*10)+8)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+4)][1][(($i*10)+8)]))  
              {{$arrayMembers[(($i*10)+4)][1][(($i*10)+8)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+5)][1][(($i*10)+8)]))  
              {{$arrayMembers[(($i*10)+5)][1][(($i*10)+8)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+6)][1][(($i*10)+8)]))  
              {{$arrayMembers[(($i*10)+6)][1][(($i*10)+8)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+7)][1][(($i*10)+8)]))  
              {{$arrayMembers[(($i*10)+7)][1][(($i*10)+8)]->selection_team}}
            @endif            
          </span>
        </div>
         <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+8)][1][(($i*10)+8)]))  
              {{$arrayMembers[(($i*10)+8)][1][(($i*10)+8)]->selection_team}}
            @endif            
          </span>
        </div>
        <div class="table-players-data">
          <span>
            @if(isset($arrayMembers[(($i*10)+9)][1][(($i*10)+8)]))  
              {{$arrayMembers[(($i*10)+9)][1][(($i*10)+8)]->selection_team}}
            @endif            
          </span>
        </div>
      </div>
    </div>
    <!--CIERRA LA SECCION DE PAGINACION DEL LEADERBOAR-->
    @endfor
  </div><!-- Cierra el cuadro del leaderboar-->
  </form>
</div>
@endsection

@section('scripts')
  <script src="js/smartpaginator.js" type="text/javascript"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      $("#players-paginator").smartpaginator({ totalrecords: {{$countPaginator}}, recordsperpage: 1, datacontainer: 'table-players', dataelement: '.table-content', initval: 0, next: 'Next', prev: 'Prev', first: 'First', last: 'Last', theme: 'green' });
      $("#optGroups").on("change", function(){
        var optSelected = $("#optGroups").val();
        if(optSelected != ""){
          $("#leaderboardForm").submit();          
        }
      });
    });
  </script>
@stop