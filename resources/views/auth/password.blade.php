@extends('layouts.password-layout')

@section('title', 'Page Title')

@section('header')
  @parent

@endsection

@section('content')	
	<form id="registro" action="{{ url('password/email') }}" method="post">
		{{ csrf_field() }}
        <div class="row">
        	<div class="col-md-3"></div>
            <div class="col-md-6">
            	<div class="form-left">
            		<div class="row">
            			<div class="col-md-12">
	            			<div class="form-header">
	            				<h2>RESET YOUR PASSWORD</h2>
	            			</div>
            			</div>
            		</div>
	                <div class="row">
	                	<div class="col-md-3">
	                		<label for="title"><span>*</span>Email Address:</label>
	                	</div>
	                	<div class="col-md-9">
		                	<div class="form-group">
		                		<input type="text" name="email" class="form-control">
		                	</div>
	                	</div>
	                </div>
	            </div>
            </div>
            <div class="col-md-3"></div>
        </div>
		<div class="row">
			<div class="col-md-12">
				<div class="submit-register">
					<button type="submit" class="btn btn-default">Reset Password</button>
				</div>
			</div>
		</div>
    </form>
	<div class="row"></div>
@endsection

@section('scripts')

	<script type="text/javascript">
		$( document ).ready(function() {
			$('div.alert').not('.alert-important').delay(5000).fadeOut(350);
		});		
	</script>

@stop