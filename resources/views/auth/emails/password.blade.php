<!DOCTYPE html>
<html>
	<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">  		
	</head>
	<body>
		<p><strong>Click the next link to reset your password: </strong>{{ url('password/reset/'.$token) }}</p>
		<p><strong>Best regards, </strong> <br> Team WinnerTakesAll.</p>
	</body>
</html>