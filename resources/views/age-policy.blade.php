@extends('layouts.terms&conditions-layout')

@section('title', 'Page Title')

@section('header')
  @parent

@endsection

@section('content')
	<div class="container">

		<div class="row">
			<div class="col-md-12">
				<h3 class="account-title"><i class="fa fa-user" aria-hidden="true"></i> Age Verification Policy</h3>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-10 well">
				<p class="text-mutted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus distinctio debitis quia molestias velit amet ad quasi sed recusandae mollitia consectetur ratione animi, adipisci magni laboriosam hic, impedit eos sunt.</p>
				<p class="text-mutted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus distinctio debitis quia molestias velit amet ad quasi sed recusandae mollitia consectetur ratione animi, adipisci magni laboriosam hic, impedit eos sunt.</p>
				<p class="text-mutted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus distinctio debitis quia molestias velit amet ad quasi sed recusandae mollitia consectetur ratione animi, adipisci magni laboriosam hic, impedit eos sunt.</p>
				<p class="text-mutted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus distinctio debitis quia molestias velit amet ad quasi sed recusandae mollitia consectetur ratione animi, adipisci magni laboriosam hic, impedit eos sunt.</p>
				<p class="text-mutted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus distinctio debitis quia molestias velit amet ad quasi sed recusandae mollitia consectetur ratione animi, adipisci magni laboriosam hic, impedit eos sunt.</p>
			</div>
			<div class="col-md-1"></div>
		</div>
	</div>

  <script type="text/javascript">
    $( document ).ready(function() {    
    });   
  </script>
@stop