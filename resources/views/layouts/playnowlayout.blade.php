<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}">

        <title>Winner Takes All - @yield('title')</title>
    </head>
    <body class="playnow">
        @section('header')
          <nav class="navbar navbar-default">
            <div class="container">
              <div class="row">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="{{ url('/') }}"><img src="{{asset('img/index/logo.png')}}"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                
                 <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right"><br>
                  <!--<li class="dropdown dropdown-language">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">EN <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="#">ES</a></li>
                      </ul>
                  </li>-->
                  @if (Auth::guest())
                  <li class="dropdown dropdown-user">
                    <a href="{{ url('/account') }}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Guest - <span class="money-text">£0</span> <span class="caret"></span></a>
                    <ul id="dropdown-test" class="dropdown-menu">
                      <li><a href="{{ url('/') }}">Login</a></li>
                    </ul>
                  </li>
                  @else
                  <li class="dropdown dropdown-user">
                    <a href="{{ url('/account') }}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> {{ Auth::user()->name }} - <span class="money-text">£{{ Auth::user()->balance }}</span> <span class="caret"></span></a>
                    <ul id="dropdown-test" class="dropdown-menu">
                      <li><a href="{{ url('/account') }}">My Account</a></li>
                      <li><a href="{{ url('/logout') }}">Logout</a></li>
                    </ul>
                  </li>                  
                  @endif 
                </ul>
              </div><!-- /.navbar-collapse -->
              </div>
            </div>
          </nav>

          <div class="home-background">
            <div class="header-links"> 
              <div class="container">
                  <div class="row">
                    <div class="col-md-12">
                      <ul class="list-inline">
                        <li><button id="joinGame" class="btn-primary">Join Game</button></li>
                        <li><button id="howPlay" class="btn-primary">How To Play</button></li>
                        <li><button id="fixtures" class="btn-primary">Fixtures</button></li>
                      </ul>
                    </div>
                  </div>
              </div>
            </div>            
          @if(!isset($msg))
            <div class="container" id="ajaxContent">
            @section('ajaxContent')
              @if (Auth::guest())
                <form role="form" id="playnow-form" action="{{ url('/registernow')}}" method="post">
              @else
                <form role="form" id="playnow-form" action="{{ url('/joinBetGroup') }}" method="post">
              @endif
                {{ csrf_field() }}
                  <input type="hidden" name="idGroup" value="" id="valueGroup">
                  <div class="form-group row" id="betting">
                    @for ($i = count($activeBetGroups)-1; $i >= 0 ; $i--)                    
                    <div class="col-md-3">
                      <input type="radio" id="option{{ $i + 1 }}" name="bet" class="form-control" value="{{ $activeBetGroups[$i]->amount }}" data-index="{{ $activeBetGroups[$i]->id }}"><label for="option{{ $i + 1 }}">£ {{ $activeBetGroups[$i]->amount }}</label>
                    </div>
                    @endfor
                  </div>
                  <div class="row">
                    @if (isset($groupDetails) && ($groupDetails[0]->amount > 0))
                    <div class="col-md-6">
                      <div class="current-participants">
                        <div class="progress">
                          <div class="progress-bar" role="progressbar" aria-valuenow="{{$groupDetails[0]->current_participants}}" aria-valuemin="0" aria-valuemax="{{$groupDetails[0]->participants_number}}" style="width:70%">
                          </div>
                        </div>
                      </div>
                      <div class="participants-overview">
                        <div class="participants-img">
                          <img src="{{asset('img/playnow/participants.png')}}" alt="">
                        </div>
                        <div class="participants-number">
                          <h3>Current Participants</h3>
                          <h2 id="current">{{$groupDetails[0]->current_participants}}/{{$groupDetails[0]->participants_number}}</h2>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="current-jackpot">
                         <div class="jackpot-overview">
                          <div class="jackpot-img">
                            <img src="{{asset('img/playnow/jackpot.png')}}" alt="">
                          </div>
                          <div class="jackpot-number">
                            <h3>Jackpot</h3>
                            <h2 id="jackpot">£ {{ (($groupDetails[0]->participants_number)*($groupDetails[0]->amount))*0.85 }}</h2>
                          </div>
                        </div>
                      </div>                     
                    </div>
                    @endif
                  </div>
                  @if(auth()->user() != null)
                  <div class="row">
                     <div class="button-fix">
                        <button type="submit" id="join" class="btn btn-default">Join Game</button>
                    </div>
                  </div>
                  @endif
                </form>
              @show
              </div>
            @else
              <br><br>
              <div class="container">
                <div class="col-md-2"></div>
                  <div class="col-md-8">
                    @include('flash::message')                    
                  </div>
                    <div class="col-md-2"></div>
              </div>
            @endif
            </div>
            @show     
            <div id="footer">
              <div class="container">
                <footer>
                    <div class="row">
                      <div class="col-md-5">
                        <div class="footer-logo">
                          <a class="navbar-brand" href="{{ url('/') }}"><img src="img/index/logo.png"></a>
                        </div>
                      </div>
                      <div class="col-md-7">
                        <div class="col-md-3">
                          <ul class="list-group">
                            <li class="list-group-item"><a href="{{ url('/aboutus') }}" target="_blank">About Us</a></li>
                            <li class="list-group-item"><a href="https://www.youtube.com/watch?v=oDhNYfkgs2Y" target="_blank">How to play</a></li>
                            @if (Auth::guest())
                              <li class="list-group-item"><a href="{{ url('/team-selection-guest') }}">Fixtures</a></li>
                            @else
                              <li class="list-group-item"><a href="{{ url('/team-selection') }}">Fixtures</a></li>
                            @endif
                            <li class="list-group-item"><a href="{{ url('/contactus') }}">Contact Us</a></li>
                          </ul>
                        </div>
                        <div class="col-md-3">
                          <ul class="list-group">
                              <li class="list-group-item"><a href="{{ url('/terms&conditions') }}" target="_blank">Terms of use</a></li>
                              <li class="list-group-item"><a href="{{ url('/privacy&policy') }}" target="_blank">Privacy and security</a></li>
                              <li class="list-group-item"><a href="{{ url('/responsiblegambling') }}" target="_blank">Responsible Gambling</a></li>
                          </ul>
                        </div>
                        <div class="col-md-offset-1 col-md-5">
                          <ul class="list-group">
                              <li class="list-group-item winner"><a href="{{ url('/') }}">Winnertakesall.bet</a></li>
                              <li class="list-group-item"><div class="text-muted"><strong>©<?php echo date("Y"); ?></strong></div></li>
                              <li class="list-group-item">
                                <ul class="list-inline">
                                    <li class="list-group-item"><a class="footer-icon footer-icon-facebook" href=""><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a></li>
                                    <li class="list-group-item"><a class="footer-icon footer-icon-twitter" href="https://twitter.com/WTA_bet" target="_blank"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a></li>
                                    <li class="list-group-item"><a class="footer-icon footer-icon-youtube" href="https://www.youtube.com/channel/UC0e5Cg4ZlpDQ-jS2FnCim_A" target="_blank"><i class="fa fa-youtube-play fa-2x" aria-hidden="true"></i></a></li>
                                </ul>
                              </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                </footer>
              </div>
            </div>            
            @yield('footer')

    </body>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="{{asset('/js/spin.js')}}"></script>
    <script src="{{asset('/js/jquery.spin.js')}}"></script>
    <script type="text/javascript">         
    </script>
    <script type="text/javascript">
      $(document).ready(function() {       
        $("#betting").buttonset();  
        var amount = "{{ $groupDetails[0]->amount }}";
        $("input[name=bet]").each(function(index){
          if($(this).attr("value") == amount){
            $(this).attr("checked","checked");
          }
        });
        $("#fixtures").click(function() {     
          var url = "{{ url('/team-selection') }}";
          window.location.href = url;
        });
        $("#howPlay").click(function(){
          var url = "https://www.youtube.com/watch?v=oDhNYfkgs2Y";
          window.open(url,'_blank');
        });        
        $("#joinGame").click(function() {
          var url = "{{ url('/playnow') }}";
          window.location.href = url;
        }); 
        $("#join").click(function(e) {
          e.preventDefault();
          if($("input[name=bet]").is(":checked")){
            $("input[name=bet]").each(function(index){
              if($(this).is(":checked")){                
                $("#valueGroup").attr("value",$(this).data("index"));
              }
            });
            $("#playnow-form").submit();
          }          
        });

        $("input[name=bet]").on("click",function(){         
          $("#ajaxContent").spin({lines: 10, length: 18, width: 8, radius: 15}, '#edbb1a');
          var index = $(this).data("index");
          amount = $(this).attr("value");
          $.ajax({
            type:"POST",
            data: { _token: '{{ csrf_token() }}', idGroup: index},
            dataType: 'json',
            url: "{{url('/resumeGroupTest')}}",
            success: function(result){              
              $("#ajaxContent").spin(false);
              $('#ajaxContent').html(result['result']);
              $("#betting").buttonset();
            }
          });
        });

      });
    </script>
            @yield('scripts')
</html>