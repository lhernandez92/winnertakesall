<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/main.css">

        <title>Winner Takes All - @yield('title')</title>
    </head>
    <body>
        @section('header')
          <nav class="navbar navbar-default">
            <div class="container">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span 
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/') }}"><img src="img/index/logo.png"></a>
              </div>

              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right"><br>
                  <!--<li class="dropdown dropdown-language">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">EN <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="#">ES</a></li>
                      </ul>
                  </li>-->
                  <li class="dropdown dropdown-user">
                    <a href="{{ url('/account') }}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">@if (Auth::guest()) Guest1234 @else {{ Auth::user()->name }} @endif - <span class="money-text">£150</span> <span class="caret"></span></a>
                    <ul id="dropdown-test" class="dropdown-menu">
                      <li><a href="{{ url('/account') }}">My Account</a></li>
                      <li><a href="{{ url('/logout') }}">Logout</a></li>
                    </ul>
                  </li>
                </ul>
              </div><!-- /.navbar-collapse -->
            </div>
          </nav>

          <div class="header-links"> 
            <div class="container">
                <div class="row">
                  <div class="col-md-12">
                  <ul class="list-inline">
                    <li><button class="btn-primary">Join Game</button></li>
                    <li><button class="btn-primary">How To Play</button></li>
                    <li><button id="fixtures" class="btn-primary">Fixtures</button></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        @show

        <div class="container">
            @yield('content')
        </div>
    </body>
    <script   src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script type="text/javascript">
      $(document).ready(function() {
        $("#fixtures").click(function() {
          var url = "{{ url('/team-selection') }}";
          window.location.href = url;
        })
      });
    </script>
            @yield('scripts')
</html>