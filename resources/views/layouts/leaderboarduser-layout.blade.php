<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <link href="{{asset('css/custom.css')}}" rel="stylesheet">

          <!-- Owl Carousel Assets -->
        <link href="{{asset('css/owl.carousel.css')}}" rel="stylesheet">
        <link href="{{asset('css/owl.theme.css')}}" rel="stylesheet">
        <link href="{{asset('css/prettify.css')}}" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}">

        <title>Winner Takes All - @yield('title')</title>
    </head>
    <body class="playnow">
        @section('header')
          <nav class="navbar navbar-default">
            <div class="container">
              <div class="row">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="{{ url('/') }}"><img src="{{asset('img/index/logo.png')}}"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right"><br>
                  <!--<li class="dropdown dropdown-language">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">EN <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="#">ES</a></li>
                      </ul>
                  </li>-->
                  @if (Auth::guest())
                  <li class="dropdown dropdown-user">
                    <a href="{{ url('/account') }}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Guest - <span class="money-text">£0</span> <span class="caret"></span></a>
                    <ul id="dropdown-test" class="dropdown-menu">
                      <li><a href="{{ url('/') }}">Login</a></li>
                    </ul>
                  </li>
                  @else
                  <li class="dropdown dropdown-user">
                    <a href="{{ url('/account') }}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> {{ Auth::user()->name }} - <span class="money-text">£{{ Auth::user()->balance }}</span> <span class="caret"></span></a>
                    <ul id="dropdown-test" class="dropdown-menu">
                      <li><a href="{{ url('/account') }}">My Account</a></li>
                      <li><a href="{{ url('/logout') }}">Logout</a></li>
                    </ul>
                  </li>                  
                  @endif 
                </ul>
              </div><!-- /.navbar-collapse -->
              </div>
            </div>
          </nav>

          <div class="home-background teams-background">
            <div class="header-links"> 
              <div class="container">
                  <div class="row">
                    <div class="col-md-12">
                      <ul class="list-inline">
                        <li><button id="joinGame" class="btn-primary">Join Game</button></li>
                        <li><button id="howPlay" class="btn-primary">How To Play</button></li>
                        <li><button id="fixtures" class="btn-primary">Fixtures</button></li>
                      </ul>
                    </div>
                  </div>
              </div>
            </div>
            @yield('content')

          </div>
            @show     
            <div id="footer">
              <div class="container">
                <footer>
                    <div class="row">
                      <div class="col-md-5">
                        <div class="footer-logo">
                          <a class="navbar-brand" href="{{ url('/') }}"><img src="img/index/logo.png"></a>
                        </div>
                      </div>
                      <div class="col-md-7">
                        <div class="col-md-3">
                          <ul class="list-group">
                            <li class="list-group-item"><a href="{{ url('/aboutus') }}" target="_blank">About Us</a></li>
                            <li class="list-group-item"><a href="https://www.youtube.com/watch?v=oDhNYfkgs2Y" target="_blank">How to play</a></li>
                            @if (Auth::guest())
                              <li class="list-group-item"><a href="{{ url('/team-selection-guest') }}">Fixtures</a></li>
                            @else
                              <li class="list-group-item"><a href="{{ url('/team-selection') }}">Fixtures</a></li>
                            @endif
                            <li class="list-group-item"><a href="{{ url('/contactus') }}">Contact Us</a></li>
                          </ul>
                        </div>
                        <div class="col-md-3">
                          <ul class="list-group">
                              <li class="list-group-item"><a href="{{ url('/terms&conditions') }}" target="_blank">Terms of use</a></li>
                              <li class="list-group-item"><a href="{{ url('/privacy&policy') }}" target="_blank">Privacy and security</a></li>
                              <li class="list-group-item"><a href="{{ url('/responsiblegambling') }}" target="_blank">Responsible Gambling</a></li>
                          </ul>
                        </div>
                        <div class="col-md-offset-1 col-md-5">
                          <ul class="list-group">
                              <li class="list-group-item winner"><a href="{{ url('/') }}">Winnertakesall.bet</a></li>
                              <li class="list-group-item"><div class="text-muted"><strong>©<?php echo date("Y"); ?></strong></div></li>
                              <li class="list-group-item">
                                <ul class="list-inline">
                                    <li class="list-group-item"><a class="footer-icon footer-icon-facebook" href=""><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a></li>
                                    <li class="list-group-item"><a class="footer-icon footer-icon-twitter" href="https://twitter.com/WTA_bet" target="_blank"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a></li>
                                    <li class="list-group-item"><a class="footer-icon footer-icon-youtube" href="https://www.youtube.com/channel/UC0e5Cg4ZlpDQ-jS2FnCim_A" target="_blank"><i class="fa fa-youtube-play fa-2x" aria-hidden="true"></i></a></li>
                                </ul>
                              </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                </footer>
              </div>
            </div>            
            @yield('footer')

    </body>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="{{asset('js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-collapse.js')}}"></script>
    <script src="{{asset('js/bootstrap-transition.js')}}"></script>
    <script src="{{asset('js/bootstrap-tab.js')}}"></script>
    <script src="{{asset('js/prettify.js')}}"></script>
    <script src="{{asset('js/application.js')}}"></script>
    <script src="{{asset('js/spin.js')}}"></script>
    <script src="{{asset('js/jquery.spin.js')}}"></script>    
    
    <script type="text/javascript">
      $(document).ready(function() {
        $("#fixtures").click(function() {
          var url = "{{ url('/team-selection') }}";
          window.location.href = url;
        });
        $("#howPlay").click(function(){
          var url = "https://www.youtube.com/watch?v=oDhNYfkgs2Y";
          window.open(url,'_blank');
        });
        $("#joinGame").click(function() {
          var url = "{{ url('/playnow') }}";
          window.location.href = url;
        });      
      });
    </script>

    <script type="text/javascript"></script>
            @yield('scripts')
</html>