<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}">

        <title>Winner Takes All - @yield('title')</title>
    </head>
    <body id="register">
        @section('header')
          <nav class="navbar navbar-default">
            <div class="container">
              <div class="row">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="{{ url('/') }}"><img src="{{asset('img/index/logo.png')}}"></a>
                </div>
              </div>
            </div>
          </nav>
        @show

        
          <div class="container well">
            @yield('content')
          </div>
        

            <div id="footer">
              <div class="container">
                <footer>
                    <div class="row">
                      <div class="col-md-5">
                        <div class="footer-logo">
                          <img src="{{asset('img/index/logo.png')}}" alt="">
                        </div>
                      </div>
                      <div class="col-md-7">
                        <div class="col-md-3">
                          <ul class="list-group">
                            <li class="list-group-item"><a href="">About Us</a></li>
                            <li class="list-group-item"><a href="https://www.youtube.com/watch?v=oDhNYfkgs2Y">How to play</a></li>
                            @if (Auth::guest())
                              <li class="list-group-item"><a href="{{ url('/team-selection-guest') }}">Fixtures</a></li>
                            @else
                              <li class="list-group-item"><a href="{{ url('/team-selection') }}">Fixtures</a></li>
                            @endif
                            <li class="list-group-item"><a href="{{ url('/contactus') }}">Contact Us</a></li>
                          </ul>
                        </div>
                        <div class="col-md-3">
                          <ul class="list-group">
                              <li class="list-group-item"><a href="{{ url('/terms&conditions') }}" target="_blank">Terms of use</a></li>
                              <li class="list-group-item"><a href="{{ url('/privacy&policy') }}" target="_blank">Privacy and security</a></li>
                              <li class="list-group-item"><a href="{{ url('/responsiblegambling') }}" target="_blank">Responsible Gambling</a></li>
                          </ul>
                        </div>
                        <div class="col-md-offset-1 col-md-5">
                          <ul class="list-group">
                              <li class="list-group-item winner"><a href="{{ url('/') }}">Winnertakesall.bet</a></li>
                              <li class="list-group-item"><div class="text-muted"><strong>©<?php echo date("Y"); ?></strong></div></li>
                              <li class="list-group-item">
                                <ul class="list-inline">
                                    <li class="list-group-item"><a class="footer-icon footer-icon-facebook" href=""><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a></li>
                                    <li class="list-group-item"><a class="footer-icon footer-icon-twitter" href="https://twitter.com/WTA_bet" target="_blank"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a></li>
                                    <li class="list-group-item"><a class="footer-icon footer-icon-youtube" href="https://www.youtube.com/channel/UC0e5Cg4ZlpDQ-jS2FnCim_A" target="_blank"><i class="fa fa-youtube-play fa-2x" aria-hidden="true"></i></a></li>
                                </ul>
                              </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                </footer>
              </div>
            </div>

            
            @yield('footer')

    </body>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script type="text/javascript">
      $(document).ready(function() {
        $("#fixtures").click(function() {
          var url = "{{ url('/team-selection') }}";
          window.location.href = url;
        })
      });
    </script>
            @yield('scripts')
</html>