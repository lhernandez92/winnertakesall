@extends('layouts.betgroups-layout')

@section('title', 'Page Title')

@section('header')
  @parent

@endsection

@section('content')
	<div class="container">
		<form action="{{ url('/register-group') }}" method="post" name="form-betgroups" id="form-betgroups">			
			{{ csrf_field() }}
			<input type="hidden" name="idBetGroup" value="" id="idBetGroup">
		<div class="row">
			<div class="col-md-6">
				<h4 class="account-title"><i class="fa fa-users" aria-hidden="true"></i> Season: {{ $actualSeason }} <a href="#" id="processWeek" class="btn btn-primary">Next Week</a></h4>
			</div>
			<div class="col-md-1"></div>	
			<div class="col-md-5">
				<h4 class="account-title"><i class="fa fa-list" aria-hidden="true"></i> Bet Groups, List Registered</h4>
			</div>							
		</div>
		<div class="row">
			<div class="col-md-5 well">				
				<div class="row">		
					<div class="col-md-2"></div>			
					<div class="col-md-4 text-center">
						<label class="text-mutted">Amount (£):</label>
					</div>
					<div class="col-md-5 text-center">
						<label class="text-mutted">Players Required:</label>
					</div>	
					<div class="col-md-1"></div>
				</div>
				<div class="row">
					<div class="col-md-2"></div>
					<div class="col-md-4 text-center">
						<div class="input-group">
						  <span class="input-group-addon">£</span>
						  <input type="number" name="amount" value="5" min="5" step="5" class="form-control">
						</div>						
					</div>
					
					<div class="col-md-1"></div>
					<div class="col-md-3 text-center">
						<input type="number" name="players" value="25" min="25" step="25" class="form-control">
					</div>
					<div class="col-md-2"></div>
				</div><br>
				<div class="row">
					<div class="col-md-12 text-center">
						<div class="submit-register">
							<button type="submit" id="addBetGroup" class="btn btn-default">Create Bet Group</button>
						</div>
					</div>
				</div>				
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-5 well">
				@include('flash::message')				
					@if(count($activeBetGroups) != 0)
						@for ($i = 0; $i < count($activeBetGroups); $i++)
						<div class="row">
							<div class="col-md-12 text-center">
								<button type="button" id="close{{ $activeBetGroups[$i]->id }}" name="deleteBetGroup" class="close" data-id="{{ $activeBetGroups[$i]->id }}" aria-label="Close">&times;</button>
								<label class="text-warning"> Amount: £{{ $activeBetGroups[$i]->amount }} | Activity Register: {{ $activeBetGroups[$i]->current_participants }} / {{ $activeBetGroups[$i]->participants_number }} | Groups Filled: #{{ $activeBetGroups[$i]->number_group }}</label>
							</div>
						</div><br>
						@endfor
					@else
						<div class="row">
							<div class="col-md-12 text-center">
								<label class="text-warning">Sorry, there is not bet groups registered.</label>
							</div>													
						</div>
					@endif			
			</div>							
		</div>	
		</form>		
	</div>
@stop