@extends('layouts.terms&conditions-layout')

@section('title', 'Page Title')

@section('header')
  @parent

@endsection

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="account-title"><i class="fa fa-user" aria-hidden="true"></i> Privacy Policy</h3>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-10 well">
				<p class="text-mutted"><strong>Collection of Information</strong></p>
				<p>At Winner Takes All we take your privacy and your information security very seriously. Winner Takes All will not sell or rent your personally identifiable information to anyone. As an entrant into a competition we do not give out any of your personal details without your express permission. In order to enter a competition it is required that you create a WTA account requiring your name and email and country of residence, and age. If you are based in the United Kingdom we are required to verify that you are over 18. As such, we require further details such as your date of birth and your address to confirm you are over 18. It is not possible to enter a competition anonymously in order to enhance the community aspect of the competitions and create an atmosphere within the organization.</p>
				<p>Every time you join a competition through a competition embedded into a third party site, you will be given the option to receive further information from the organization and to be included on their mailing list or the mailing list of the advertising partner the game is being run in conjunction with (if any). Winner Takes All declines all responsibility for the subsequent use of your personal information by the organization. If you wish to stop receiving further communications outside Winner Takes All from an organization, please contact them directly.</p>
				<p>The personal data provided is stored on our secure computerized database to enable us to respond to your requests and to allow you to safely and securely play in competitions, organise your own competitions and to cash out winnings or money raised. We may communicate with you in the future by mail, email and telephone.</p>
			</div>
			<div class="col-md-1"></div>
		</div>
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10 well">
				<p class="text-mutted"><strong>Security</strong></p>
				<p>Personal information (such as name, e mail address and country of residence) is protected on a computerized system to ensure that loss, misuse, unauthorized access or disclosure, alteration or destruction of this information is not probable. Sensitive information (such as credit card number and ID’s) are protected by secure server software. It prevents anyone else reading your personal and sensitive information while your entry is being processed online. It is your responsibility to keep your password secure at all times to avoid unauthorized use of your WTA account.</p>
			</div>
			<div class="col-md-1"></div>			
		</div>
	</div>

  <script type="text/javascript">
    $( document ).ready(function() {    
    });   
  </script>
@stop