@extends('layouts.register-layout')

@section('title', 'Page Title')

@section('header')
  @parent

@endsection

@section('content')	
	<div class="row">
		<div class="register-calltoaction">
			<h2>Register to play</h2>
			<h1> Winner Takes All</h1>
		</div>
	</div>
	<form id="registro" action="{{ url('action/users') }}" method="post">
		{{ csrf_field() }}
        <div class="row">
        	<div class="col-md-2"></div>
        	<div class="col-md-8">
        		<br>@include('flash::message')
        	</div>
        	<div class="col-md-2"></div>
            <div class="col-md-6">
            	<div class="form-left">
            		<div class="row">
            			<div class="col-md-12">
	            			<div class="form-header">
	            				<h2>PERSONAL DETAILS</h2>
	            			</div>
            			</div>
            		</div>
	                <div class="row">
	                	<div class="col-md-3">
	                		<label for="title"><span>*</span>Title and Name:</label>
	                	</div>
	                	<div class="col-md-3">
	                		<div class="form-group">						  
							  <select class="form-control" name="title">
							    <option selected>Mr.</option>
							    <option>Miss.</option>
							    <option>Mrs.</option>
							    @if(old('title') != null)
							    	<option selected>{{ old('title') }}</option>
							    @endif
							  </select>
							</div>
	                	</div>
	                	<div class="col-md-6">
	                		<input type="text" class="form-control" value="{{ old('name') }}" name="name">
	                	</div>
	                </div>
	                <div class="row">
	                	<div class="col-md-3">
	                		<label for="title"><span>*</span>Surname:</label>
	                	</div>
	                	<div class="col-md-9">
	                		<div class="form-group">
	                			<input type="text" class="form-control" value="{{ old('surname') }}" name="surname">
	                		</div>
	                	</div>
	                </div>
	                 <div class="row">
	                	<div class="col-md-3">
	                		<label for="title"><span>*</span>Date of Birth:</label>
	                	</div>
	                	<div class="col-md-3">
	                		<div class="form-group">
							  <select class="form-control" name="day">
							    <option value="-1">Day</option>
							    @for($i = 1; $i <= 31; $i++)
							    	@if(old('day') == $i)
								    	<option value="{!! $i !!}" selected>
								    	{{ $i }}
								    	</option>
							    	@else
								    	<option value="{!! $i !!}">
								    	{{ $i }}
								    	</option>
							    	@endif
							    @endfor
							  </select>
							</div>
	                	</div>
	                	<div class="col-md-3">
	                		<div class="form-group">
							  <select class="form-control" name="month">
							    <option value="-1">Month</option>
							    @for($i = 0; $i < 12; $i++)	
							    	@if(old('month') == $i)
								    	<option value="{!! $i !!}" selected>									
								    	{{ $months[$i] }}
								    	</option>
							    	@else
								    	<option value="{!! $i !!}">									
								    	{{ $months[$i] }}
								    	</option>							    	
							    	@endif
							    @endfor							    
							  </select>
							</div>
	                	</div>
	                	<div class="col-md-3">
	                		<div class="form-group">
							  <select class="form-control" name="year">
							    <option value="-1">Year</option>
							    @for($i = ($year - 17); $i > ($year - 100); $i--)	
							    	@if(old('year') == $i)
								    	<option value="{!! $i !!}" selected>									
								    	{{ $i }}
								    	</option>
							    	@else
							    		<option value="{!! $i !!}">									
								    	{{ $i }}
								    	</option>							    	
							    	@endif
							    @endfor								    
							  </select>
							</div>
	                	</div>
	                </div>
	                <div class="row">
	                	<div class="col-md-3">
	                		<label for="title"><span>*</span>Email Address:</label>
	                	</div>
	                	<div class="col-md-9">
		                	<div class="form-group">
		                		<input type="text" name="email" value="{{ old('email') }}" class="form-control">
		                	</div>
	                	</div>
	                </div>
	                 <div class="row">
	                	<div class="col-md-3">
	                		<label for="title"><span>*</span>Phone Number:</label>
	                	</div>
	                	<div class="col-md-9">
	                		<div class="form-group">
	                			<input type="tel" name="phone_number" value="{{ old('phone_number') }}" class="form-control" id="phone">
	                		</div>
	                	</div>
	                </div>
	                <div class="row">
	                	<div class="col-md-3">
	                		<label for="title"><span>*</span>Country:</label>
	                	</div>
	                	<div class="col-md-9">
		                	<div class="form-group">
								<input type="text" name="country" value="{{ old('country') }}" class="form-control" id="country">
		                	</div>
	                	</div>
	                </div>
	            </div>
            </div>
           	<div class="col-md-6">
            	<div class="form-right">
            		<div class="row">
            			<div class="col-md-12">
	            			<div class="form-header">
	            				<h2>ACCOUNT DETAILS</h2>
	            			</div>
            			</div>
            		</div>
            		<div class="row">
	                	<div class="col-md-4">
	                		<label for="title"><span>*</span>Username:</label>
	                	</div>
	                	<div class="col-md-8">
		                	<div class="form-group">
		                		<input type="text" name="username" value="{{ old('username') }}" class="form-control">
		                	</div>
		                </div>
	                </div>
	                <div class="row">
	                	<div class="col-md-4">
	                		<label for="title"><span>*</span>Password:</label>
	                	</div>
	                	<div class="col-md-8">
	                		<div class="form-group">
		                		<input type="password" name="password" class="form-control">
		                	</div>
	                	</div>
	                </div>
	                 <div class="row">
	                	<div class="col-md-4">
	                		<label for="title"><span>*</span>Confirm Password:</label>
	                	</div>
	                	<div class="col-md-8">
	                		<div class="form-group">
		                		<input type="password" name="confirmpassword" class="form-control">
		                	</div>
	                	</div>
	                </div>
	                <div class="row">
	                	<div class="col-md-4">	                		
		                		<label for="title"><span>*</span>Security Question:</label>
		                </div>	                	
	                	<div class="col-md-8">
	                		<div class="form-group">
							  <select class="form-control" name="security_question">
							    <option value="What is your mother's maiden name?" selected>What is your mother's maiden name?</option>
							    <option value="What was the name of your favourite teacher at school?">What was the name of your favourite teacher at school?</option>
							    <option value="What was the name of your first pet?">What was the name of your first pet?</option>
							    @if(old('security_question') != null)
							    	<option value="{{ old('security_question') }}" selected>{{ old('security_question') }}</option>
							    @endif
							  </select>
							</div>
	                	</div>
	                </div>
	                <div class="row">
	                	<div class="col-md-4">
	                		<label for="title"><span>*</span>Security Answer</label>
	                	</div>
	                	<div class="col-md-8">
		                	<div class="form-group">
		                		<input type="text" name="security_answer" value="{{ old('security_answer') }}" class="form-control">
		                	</div>
	                	</div>
	                </div>
	                <div class="row">
	                	<div class="col-md-12">
	                		<div class="disclaimer">
	                			<span>*</span>Fields must be completed
	                		</div>
	                	</div>
	                </div>
	            </div>
            </div>
        </div>
		<div class="row">
			<div class="col-md-12">
				<div class="terms">
					<label for="terms"><input type="checkbox" name="state_id" id="termsname" class="form-control">I'm over 18 years of age and I accept the <br> <a href="{{ url('/terms&conditions') }}" target="_blank">Terms and Conditions</a>, <a href="{{ url('/privacy&policy') }}" target="_blank">privacy policy</a>, <a href="{{ url('/verificationage') }}" target="_blank">age verification policy</a><br>and betting rules as published on this website.</label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="submit-register">
					<button type="submit" onclick="validarPassword();" class="btn btn-default">Register</button>
				</div>
			</div>
		</div>
    </form>
	<div class="row"></div>
@endsection

@section('scripts')

	<script type="text/javascript">
		function validarPassword(){
			var pass = document.getElementsByName('password')[0].value;
			var confirmPass = document.getElementsByName('confirmpassword')[0].value;
			if(pass != confirmPass){
				document.getElementsByName('password')[0].value = "confirmpassword123!";
			}
		}
		$( document ).ready(function() {
			$("#country").countrySelect();
			$("#phone").intlTelInput();
			$('div.alert').not('.alert-important').delay(5000).fadeOut(350);
		});		
	</script>

@stop