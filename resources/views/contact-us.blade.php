@extends('layouts.terms&conditions-layout')

@section('title', 'Page Title')

@section('header')
  @parent

@endsection

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="account-title"><i class="fa fa-send" aria-hidden="true"></i> Contact Us</h3>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-10 well">
				<div class="row">
					<div class="col-md-12 text-center">
						<form action="#" method="post" name="contact">
							{{ csrf_field() }}
							<label for="">
								<h2>We'd love to hear from you!</h2>
							</label>
							<br>
							<div class="row">
								<div class="col-md-2"></div>
								<div class="col-md-8">
									<label for="" class="text-muted">
										<p>Whether you have a question about how best to raise funds or wondering how we can help you and your organisation, please get in touch.</p><br>
									</label>

									<div class="row text-left">
										<div class="col-md-2"></div>
										<div class="col-md-8 text-warning">
											<label for="">Your Name</label>
										</div>
										<div class="col-md-2"></div>
									</div>

									<div class="row text-left">
										<div class="col-md-2"></div>
										<div class="col-md-8">
											<input type="text" name="name" class="form-control">
										</div>
										<div class="col-md-2"></div>
									</div>	
									<br>
									<div class="row text-left">
										<div class="col-md-2"></div>
										<div class="col-md-8 text-warning">
											<label for="">Email Adress</label>
										</div>
										<div class="col-md-2"></div>
									</div>
									
									<div class="row text-left">
										<div class="col-md-2"></div>
										<div class="col-md-8">
											<input type="text" name="email" class="form-control">
										</div>
										<div class="col-md-2"></div>
									</div>
									<br>
									<div class="row text-left">
										<div class="col-md-2"></div>
										<div class="col-md-8 text-warning">
											<label for="">Subject</label>
										</div>
										<div class="col-md-2"></div>
									</div>
									
									<div class="row text-left">
										<div class="col-md-2"></div>
										<div class="col-md-8">
											<input type="text" name="subject" class="form-control">
										</div>
										<div class="col-md-2"></div>
									</div>										
									<br>
									<div class="row text-left">
										<div class="col-md-2"></div>
										<div class="col-md-8 text-warning">
											<label for="">Message</label>
										</div>
										<div class="col-md-2"></div>
									</div>
									
									<div class="row text-left">
										<div class="col-md-2"></div>
										<div class="col-md-8">
											<textarea name="message" cols="30" rows="7" class="form-control">
												
											</textarea>
										</div>
										<div class="col-md-2"></div>
									</div>
									<br><br>
									<div class="row text-center">
										<div class="col-md-2"></div>
										<div class="col-md-8">
											<input type="submit" name="send" class="btn btn-warning" value="Send Message">
										</div>
										<div class="col-md-2"></div>
									</div>									

								</div>
								<div class="col-md-2"></div>
							</div>
						</form>							
					</div>
				</div>
			</div>
			<div class="col-md-1"></div>
		</div>
	</div>

  <script type="text/javascript">
    $( document ).ready(function() {    
    });   
  </script>
@stop