@extends('layouts.teams-layout')

@section('title', 'Page Title')

@section('header')
  @parent

@endsection

@section('content')
	@if (isset($arrayFix) && isset($week))	
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="select-team-title">
						<div class="row">
							<div class="col-md-2" id="headerWeek"> Week {{ $weekPivot }}</div>
						</div>
					</h3>						
				</div>
			</div>
		</div>			
		<div class="container" id="ajaxContent">
		@section('ajaxContent')			
			<div class="row">			
				<div class="select-team-container">
					<div class="col-lg-9 col-md-9">
						<div class="row" id="chargeSpin">							
							<div class="customNavigation">
							  	<div class="col-md-2">
							  		<a href="" class="btn prev"><i class="fa fa-chevron-left fa-2x" aria-hidden="true"></i></a>
							  	</div>
							  	<div class="col-md-8">
									<div class="select-team-header">
										<span>GameWeek 9-10 May 17:00</span>
									</div>
								</div>
							  	<div class="col-md-2">
							  		<a href="" class="btn next"><i class="fa fa-chevron-right fa-2x" aria-hidden="true"></i></a>
								</div>
							</div>
						</div>
						<div id="owl-demo" class="owl-carousel owl-theme">
						  	<form action="{{ url('/team-selection') }}" method="post" id="joinForm">	
						  	
						  	{{ csrf_field() }}			  		
						  	<input type="hidden" value="{{ $week }}" name="currentWeek" id="currentWeek">
						  	<input type="hidden" value="" name="type" id="type">
						  	<input type="hidden" value="{{ $weekPivot }}" name="weekPivot" id="weekPivot">							

				  			<div class="item">
						  	@for ($i = 0; $i < count($arrayFix); $i++)
								<div class="row item-fix">
									<div class="cc-selector">
							    		<div class="col-sm-3 date-fix">
							    			<span class="match-date">{{ strtoupper(Date("d-M",strtotime($arrayFix[$i]->date))) }} {{$arrayFix[$i]->hour}}</span>
							    		</div>
							    		<div class="col-sm-3">
											<div class="team-left">
							    				<label class="team-name team-left selected-team" for="{{$teams[$arrayFix[$i]->id_home_team]}}">{{$arrayFix[$i]->name_team($arrayFix[$i]->id_home_team)}}</label>
							    			</div>
							    		</div>
								    	<div class="col-sm-3 flag-fix">
								    		<input id="{{$teams[$arrayFix[$i]->id_home_team]}}" class="left-flag" type="radio" name="match" value="{{$arrayFix[$i]->id_home_team}}" {{ $arrayHomeTeam[$i] }}>
									        <label class="drinkcard-cc {{$teams[$arrayFix[$i]->id_home_team]}} left-team" for="{{$teams[$arrayFix[$i]->id_home_team]}}"></label>
											<span class="vs">VS.</span>
									        <input id="{{$teams[$arrayFix[$i]->id_visit_team]}}" class="right-flag" type="radio" name="match" value="{{$arrayFix[$i]->id_visit_team}}" {{ $arrayVisitTeam[$i] }}>
									        <label class="drinkcard-cc {{$teams[$arrayFix[$i]->id_visit_team]}} right-team" for="{{$teams[$arrayFix[$i]->id_visit_team]}}"></label>
								    	</div>
										<div class="col-sm-3">
											<div class="team-right">
												<label class="team-name" for="{{$teams[$arrayFix[$i]->id_visit_team]}}">{{$arrayFix[$i]->name_team($arrayFix[$i]->id_visit_team)}}</label>
											</div>
										</div>
								    </div>
								</div>
					  		@endfor
					  		</div>
						  	</form>
						</div>
					</div>
					<div class="col-lg-3 col-md-3">
						<div class="row">
							<div class="barclays-brand">
								<img src="{{asset('img/teams/premier-league-logo.png')}}" alt="">
							</div>
						</div>
						<div class="row">
							<div class="table-leaderboard">
									<div class="top-club"></div>
									<div class="champions-club"></div>
									<div class="decline-club"></div>
									<div class="col-md-6">
										<div class="table-header table-data">
											<span> </span>
										</div>
									</div>
									<div class="col-md-3">
										<div class="table-data table-header">
											<span> </span>
										</div>
									</div>
									<div class="col-md-3">
										<div class="table-data table-header">
											<span> </span>
										</div>
									</div>
								<div class="table-row table-row-header">
									<div class="col-md-6 table-border">
										<div class="table-header table-data">
											<span>Club</span>
										</div>
										@for ($i = 0; $i < count($tableLeague); $i++)
											<div class="table-data">
												<span>{{ str_replace("FC","",$tableLeague[$i]->teamName) }}</span>
											</div>										
										@endfor
									</div>
									<div class="col-md-3 table-border">
										<div class="table-data table-header">
											<span>Pld</span>
										</div>
										@for ($i = 0; $i < count($tableLeague); $i++)
											<div class="table-data">
												<span>{{ $tableLeague[$i]->playedGames }}</span>
											</div>										
										@endfor										
									</div>
									<div class="col-md-3 table-border">
										<div class="table-data table-header">
											<span>Pts</span>
										</div>
										@for ($i = 0; $i < count($tableLeague); $i++)
											<div class="table-data">
												<span>{{ $tableLeague[$i]->points }}</span>
											</div>										
										@endfor										
									</div>
								</div>							
							</div>
						</div>
					</div>
				</div>
			</div>				
		@show
		</div>
	@else
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<h1 class="text-warning text-center">Ups! something is wrong ):</h1>	
			</div>
			<div class="col-md-2"></div>
		</div>                
	@endif

@endsection

@section('scripts')

  <script type="text/javascript">
    $(document).ready(function() { 
		var owl = $("#owl-demo");

		owl.owlCarousel({
			items:	1
		});

		// Custom Navigation Events
		$(".next").click(function(e){
			e.preventDefault();
			$("#type").attr("value","next");

			$("#chargeSpin").spin({lines: 10, length: 18, width: 8, radius: 15}, '#edbb1a');
				var weekPivotHide = $("#weekPivot").val();
			$.ajax({
				type:"POST",
				data: { _token: '{{ csrf_token() }}', weekPivot: weekPivotHide, type: 'next'},
				dataType: 'json',
				url: "{{url('/selectNextWeekGuest')}}",
				success: function(result){           
					$("#ajaxContent").spin(false);
					$("#ajaxContent").html(result['result']);
					var owl = $("#owl-demo");
					owl.owlCarousel({
						items:	1
					});
					var headerweek = $("#weekPivot").val();
					$("#headerWeek").text(" Week " + headerweek);		   
				}
			});			
			//$("#joinForm").submit();
			//owl.trigger('owl.next');
		});
		$(".prev").click(function(e){
			e.preventDefault();			
			$("#type").attr("value","prev");

			$("#chargeSpin").spin({lines: 10, length: 18, width: 8, radius: 15}, '#edbb1a');
				var weekPivotHide = $("#weekPivot").val();
			$.ajax({
				type:"POST",
				data: { _token: '{{ csrf_token() }}', weekPivot: weekPivotHide, type: 'prev'},
				dataType: 'json',
				url: "{{url('/selectNextWeekGuest')}}",
				success: function(result){    
					$("#ajaxContent").spin(false);
					$("#ajaxContent").html(result['result']);
					var owl = $("#owl-demo");
					owl.owlCarousel({
						items:	1
					});
					var headerweek = $("#weekPivot").val();
					$("#headerWeek").text(" Week " + headerweek);				          
				}
			});			
			//$("#joinForm").submit();
			//owl.trigger('owl.prev');
	  	});
	  	$("#fixtures").click(function() {
          	var url = "{{ url('/team-selection-guest') }}";
          	window.location.href = url;
        })
	  	$("#join").click(function() {
        	$("#joinForm").submit();
        });
	});
  </script>
@stop