@extends('layouts.terms&conditions-layout')

@section('title', 'Page Title')

@section('header')
  @parent

@endsection

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="account-title"><i class="fa fa-user" aria-hidden="true"></i> About Us</h3>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-10 well">
				<p>Winnertakesall.bet was established in 2016 in London, England. It enables people from all around the world to test their wits against each other through an online sports prediction competition.</p>
				<p>Our web based competitions are run alongside live sporting fixtures, whereby entrants participating in our game will be able to pay their entry fee on a secure payments platform, and make their selection for the subsequent fixtures.</p>
				<p>Our aim is to provide an enjoyable and social betting experience with the opportunity to win large cash jackpots from small stakes.</p>
			</div>
			<div class="col-md-1"></div>
		</div>
	</div>

  <script type="text/javascript">
    $( document ).ready(function() {    
    });   
  </script>
@stop