@extends('layouts.account-layout')

@section('title', 'Page Title')

@section('header')
  @parent

@endsection

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="account-title"><i class="fa fa-user" aria-hidden="true"></i>Account Overview</h3>
				<div class="row">
					<div class="col-md-6">@include('flash::message')</div>
					<div class="col-md-6"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<div class="account-container">
	                <ul class="nav nav-pills nav-stacked col-md-3">
	                	<li class="account-section">Banking</li>
	                    <li><a href="#a" data-toggle="tab">Deposit</a></li>
	                    <li><a href="#b" data-toggle="tab">Manage Payment Methods</a></li>
	                    <li><a href="#c" data-toggle="tab">Withdraw</a></li>
	                    <li class="account-section account-section-separator">History</li>
	                    <li><a href="#d" data-toggle="tab">All</a></li>
	                    <li class="active"><a href="#e" data-toggle="tab">Open Bets</a></li>
	                    <li><a href="#f" data-toggle="tab">Settled Bets</a></li>
	                    <li class="account-section account-section-separator">Account Details</li>
	                    <li><a href="#h" data-toggle="tab">Deposit Limits</a></li>
	                    <li><a href="#i" id="leaderboardUser">Check Table Leaderboard</a></li>
	                    <li><a href="#j" data-toggle="tab">Change Password</a></li>
	                </ul>
	                <div class="tab-content col-md-9">
	                    <div class="tab-pane" id="a"></div>
	                    <div class="tab-pane" id="b">Secondo sed ac orci quis tortor imperdiet venenatis. Duis elementum auctor accumsan. Aliquam in felis sit amet augue.</div>
	                    <div class="tab-pane" id="c">Thirdamuno, ipsum dolor sit amet, consectetur adipiscing elit. Duis elementum auctor accumsan. Duis pharetra
	                    varius quam sit amet vulputate. Quisque mauris augue, molestie tincidunt condimentum vitae. </div>
	                    <div class="tab-pane" id="d">
	                    	<div class="account-subtitle">
	                    		<h5>Account</h5>
	                    	</div>
	                    	<div class="account-number">
	                    		<span>AA1990 No. 5584276</span>
	                    	</div>
	                    	<div class="account-subtitle">
	                    		<h5>Total Balance</h5>
	                    	</div>
	                    	<div class="account-balance">
	                    		@if(Auth::guest())
	                    			<span>£0</span>
	                    		@else
									<span>£{{ Auth::user()->balance }}</span>
	                    		@endif
	                    	</div>
	                    	<hr>
	                    	<div class="account-current-games">
	                    		<h5>Current Games</h5>
	                    	</div>
	                    	<table class="table table-striped"> 
	                    		<thead> 
	                    			<tr> 
	                    				<th>Game Entry Date</th> 
	                    				<th>Game Code</th> 
	                    				<!--<th>Game Description</th> -->
	                    				<th>Entry Fee</th>
	                    				<th>Participants</th>
	                    				<th>Jackpot</th>
	                    				<th>Leaderboard</th>
	                    				<th>Make Next Selection</th>
	                    			</tr> 
	                    		</thead> 
	                    		<tbody>
	                    			@for ($i = 0; $i < count($registerGroups); $i++)  
		                    		<tr>
	                    				<td>{{ $registerGroupsDate[$i] }}</td>
	                    				<td>#{{ $gameCode[$i] }}</td>
	                    				<!--<td>{{ $actualSeason->season }}</td>-->
	                    				<td>£{{$registerGroups[$i]->amount}}</td>
	                    				<td>{{$registerGroups[$i]->current_participants}}/{{$registerGroups[$i]->participants_number}}</td>
	                    				<td>£{{($registerGroups[$i]->participants_number)*($registerGroups[$i]->amount)}}</td>
	                    				@if(($registerGroups[$i]->current_participants == $registerGroups[$i]->participants_number))
	                    				<td><a href="{{url('/leaderboardUser/'.$registerGroups[$i]->id)}}" class="account-next-game">
	                    					Show Results	                    					
	                    				</a></td>
	                    				@else
	                    				<td><a href="#" class="account-next-game">
	                    					Awaiting Start                    					
	                    				</a></td>	                    				
	                    				@endif	                    				
	                    				@if(($registerGroups[$i]->current_participants == $registerGroups[$i]->participants_number) && (($registerGroups[$i]->first_match <= $actualSeason->current_match) && ($registerGroups[$i]->last_match >= $actualSeason->current_match)))
	                    				<td><a href="{{url('/team-selection/'.$registerGroups[$i]->id)}}" class="account-next-game">
	                    					@if($stateGame[$i])
	                    						Awaiting Result
	                    					@else
	                    						Play Next Game
	                    					@endif	                    					
	                    				</a></td>
	                    				@else
	                    					@if(($registerGroups[$i]->first_match == 0) && ($registerGroups[$i]->last_match == 0))
		                    				<td><a href="{{url('/team-selection/'.$registerGroups[$i]->id)}}" class="account-next-game">
		                    					@if($stateGame[$i])
		                    						Awaiting Result
		                    					@else
		                    						Play Next Game
		                    					@endif	                    					
		                    				</a></td>
	                    					@endif
	                    				@endif
		                    		</tr>
		                    		@endfor
		                    		
	                    			@for ($i = 0; $i < count($registerGroupsInactive); $i++)  
		                    		<tr>
	                    				<td>{{ $registerGroupsDateInactive[$i] }}</td>
	                    				<td>#{{ $gameCodeInactive[$i] }}</td>
	                    				<!--<td>{{ $seasonInactive[$i] }}</td>-->
	                    				<td>£{{$registerGroupsInactive[$i]->amount}}</td>
	                    				<td>{{$registerGroupsInactive[$i]->current_participants}}/{{$registerGroupsInactive[$i]->participants_number}}</td>
	                    				<td>£{{($registerGroupsInactive[$i]->participants_number)*($registerGroupsInactive[$i]->amount)}}</td>
	                    				<td><a href="#" class="account-next-game">Leaderboard Closed</a></td>	                    				
	                    				<td><a href="#" class="account-next-game">Group Closed - {{$stateGameInactive[$i]}}</a></td>
		                    		</tr>
		                    		@endfor		                    		
	                    		</tbody> 
	                    	</table>	                    	
	                    </div>
	                    <div class="tab-pane active" id="e">
	                    	<div class="account-subtitle">
	                    		<h5>Account</h5>
	                    	</div>
	                    	<div class="account-number">
	                    		<span>AA1990 No. 5584276</span>
	                    	</div>
	                    	<div class="account-subtitle">
	                    		<h5>Total Balance</h5>
	                    	</div>
	                    	<div class="account-balance">
	                    		@if(Auth::guest())
	                    			<span>£0</span>
	                    		@else
									<span>£{{ Auth::user()->balance }}</span>
	                    		@endif
	                    	</div>
	                    	<hr>
	                    	<div class="account-current-games">
	                    		<h5>Current Games</h5>
	                    	</div>
	                    	<table class="table table-striped"> 
	                    		<thead> 
	                    			<tr> 
	                    				<th>Game Entry Date</th> 
	                    				<th>Game Code</th> 
	                    				<!--<th>Game Description</th>-->
	                    				<th>Entry Fee</th>
	                    				<th>Participants</th>
	                    				<th>Jackpot</th>
	                    				<th>Leadeboard</th> 
	                    				<th>Make Next Selection</th>
	                    			</tr> 
	                    		</thead> 
	                    		<tbody>
	                    			@for ($i = 0; $i < count($registerGroups); $i++)  
		                    		<tr>
	                    				<td>{{ $registerGroupsDate[$i] }}</td>
	                    				<td>#{{ $gameCode[$i] }}</td>
	                    				<!--<td>{{ $actualSeason->season }}</td>-->
	                    				<td>£{{$registerGroups[$i]->amount}}</td>
	                    				<td>{{$registerGroups[$i]->current_participants}}/{{$registerGroups[$i]->participants_number}}</td>
	                    				<td>£{{($registerGroups[$i]->participants_number)*($registerGroups[$i]->amount)}}</td>
	                    				@if(($registerGroups[$i]->current_participants == $registerGroups[$i]->participants_number))
	                    				<td><a href="{{url('/leaderboardUser/'.$registerGroups[$i]->id)}}" class="account-next-game">
	                    					Show Results	                    					
	                    				</a></td>
	                    				@else
	                    				<td><a href="#" class="account-next-game">
	                    					Awaiting Start                    					
	                    				</a></td>	                    				
	                    				@endif
	                    				@if(($registerGroups[$i]->current_participants == $registerGroups[$i]->participants_number) && (($registerGroups[$i]->first_match <= $actualSeason->current_match) && ($registerGroups[$i]->last_match >= $actualSeason->current_match)))
	                    				<td><a href="{{url('/team-selection/'.$registerGroups[$i]->id)}}" class="account-next-game">
	                    					@if($stateGame[$i])
	                    						Awaiting Result
	                    					@else
	                    						Play Next Game
	                    					@endif	                    					
	                    				</a></td>
	                    				@else
	                    					@if(($registerGroups[$i]->first_match == 0) && ($registerGroups[$i]->last_match == 0))
		                    				<td><a href="{{url('/team-selection/'.$registerGroups[$i]->id)}}" class="account-next-game">
		                    					@if($stateGame[$i])
		                    						Awaiting Result
		                    					@else
		                    						Play Next Game
		                    					@endif	                    					
		                    				</a></td>
	                    					@endif
	                    				@endif
		                    		</tr>
		                    		@endfor
	                    		</tbody> 
	                    	</table>	                    	
	                    </div>
	                    <div class="tab-pane" id="f">
	                    	<div class="account-subtitle">
	                    		<h5>Account</h5>
	                    	</div>
	                    	<div class="account-number">
	                    		<span>AA1990 No. 5584276</span>
	                    	</div>
	                    	<div class="account-subtitle">
	                    		<h5>Total Balance</h5>
	                    	</div>
	                    	<div class="account-balance">
	                    		@if(Auth::guest())
	                    			<span>£0</span>
	                    		@else
									<span>£{{ Auth::user()->balance }}</span>
	                    		@endif
	                    	</div>
	                    	<hr>
	                    	<div class="account-current-games">
	                    		<h5>Settled Games</h5>
	                    	</div>
	                    	<table class="table table-striped"> 
	                    		<thead> 
	                    			<tr> 
	                    				<th>Game Entry Date</th> 
	                    				<th>Game Code</th> 
	                    				<!--<th>Game Description</th> -->
	                    				<th>Entry Fee</th>
	                    				<th>Participants</th>
	                    				<th>Jackpot</th>
	                    				<th>Leaderboard</th>
	                    				<th>Make Next Selection</th>
	                    			</tr> 
	                    		</thead> 
	                    		<tbody>
	                    			@for ($i = 0; $i < count($registerGroupsInactive); $i++)  
		                    		<tr>
	                    				<td>{{ $registerGroupsDateInactive[$i] }}</td>
	                    				<td>#{{ $gameCodeInactive[$i] }}</td>
	                    				<!--<td>{{ $seasonInactive[$i] }}</td>-->
	                    				<td>£{{$registerGroupsInactive[$i]->amount}}</td>
	                    				<td>{{$registerGroupsInactive[$i]->current_participants}}/{{$registerGroupsInactive[$i]->participants_number}}</td>
	                    				<td>£{{($registerGroupsInactive[$i]->participants_number)*($registerGroupsInactive[$i]->amount)}}</td>
	                    				<td><a href="#" class="account-next-game">Leaderboard Closed</a></td>
	                    				<td><a href="#" class="account-next-game">Group Closed - {{$stateGameInactive[$i]}}</a></td>
		                    		</tr>
		                    		@endfor
	                    		</tbody> 
	                    	</table>	                    	
	                    </div>
	                    <div class="tab-pane" id="g">Lorem ipsum dolor sit amet, consectetur adipisicing elit. </div>
	                    <div class="tab-pane" id="h">
	                    	<hr>	
	                    	<form method="post" action="{{ url('/depositlimit') }}" id="form-depositlimit">
	                    		{{ csrf_field() }}
		                    	<div class="row">
		                    		<div class="col-md-4"></div>
		                    		<div class="col-md-4 text-center">
				                    	<div class="account-subtitle">
				                    		<h5>Set a Deposit Limit</h5>
				                    	</div>
		                    		</div>
		                    		<div class="col-md-4"></div>
		                    	</div>
		                    	<div class="row">
		                    		<div class="col-md-5"></div>
		                    		<div class="col-md-2">	                    			
		                    				<input type="number" value="5" min="5" step="5" name="depositlimit" class="form-control">	                    			                    		
		                    		</div>
		                    		<div class="col-md-5"></div>
		                    	</div>
		                    	<div class="row">
		                    		<div class="col-md-2"></div>
									<div class="col-md-8 text-center">
										<br>
										<div class="submit-register">
											<button type="submit" id="depositLimit" class="btn btn-warning">Confirm Amount</button>
										</div>								
									</div>
		                    		<div class="col-md-2"></div>	                    		
		                    	</div>
	                    	</form>
	                    	<hr>	                    	
	                    </div>
	                    <div class="tab-pane" id="i">Lorem ipsum dolor sit amet, consectetur adipisicing elit. </div>
	                    <div class="tab-pane" id="j">Lorem ipsum dolor sit amet, consectetur adipisicing elit. </div>
	                </div>		            
				</div>
			</div>
		</div>
	</div>

@endsection

@section('scripts')  

  <script type="text/javascript">
    $( document ).ready(function() {
       $("#betting").buttonset();     
       $('div.alert').not('.alert-important').delay(5000).fadeOut(350);   

       $("#leaderboardUser").on("click", function(e){
       		e.preventDefault();
       		location.href = "{{ url('/leaderboardUser') }}";
       });
    });   
  </script>
@stop