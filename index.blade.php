@extends('layouts.master')

@section('title', 'Page Title')

@section('header')
    @parent

@endsection

@section('content')
  <div class="container">
      <div class="row">
        <div class="home-overview">
          <img src="img/index/crown.png" alt="">
          <h1>How it works</h1>
        </div>
      </div>
      <div class="steps-fix">
        <div class="row">
          <div class="col-md-3">
            <div class="steps">
              <div class="row">
                <img src="img/index/register.png" alt="">
                <h2>Register</h2>
              </div>
              <div class="row">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
              </div>
            </div>
          </div>
          <div class="col-md-3">
           <div class="steps">
              <div class="row">
                <img src="img/index/deposit.png" alt="">
                <h2>Deposit</h2>
              </div>
              <div class="row">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit..</p>
              </div>
           </div>
          </div>
          <div class="col-md-3">
            <div class="steps">
              <div class="row">
                <img src="img/index/competition.png" alt="">
                <h2>Select <br>Competition</h2>
              </div>
              <div class="row">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="steps">
              <div class="row">
                <img src="img/index/team.png" alt="">
                <h2>Select a team</h2>
              </div>
              <div class="row">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container">
    	<div class="row">
      	<div class="home-redirections">
      		<a href="/winnertakesall/public/registernow" class="btn btn-warning btn-started">Lets get started</a>
      	</div>
    </div>
    	<div class="row">
      	<div class="home-redirections">
      		<a href="#" class="btn btn-default btn-invite">Invite friends</a>
      	</div>
    	</div>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
</script>
@stop

