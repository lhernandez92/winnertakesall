<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDetailgamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detailgames', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_season')->unsigned();
            $table->integer('id_detail_season')->unsigned();
            $table->integer('id_user')->unsigned();
            $table->integer('selection_team')->nullable();
            $table->enum('state',['pass','denied','null'])->default('null');
            $table->integer('id_betgroup')->unsigned();
            $table->integer('week');

            $table->foreign('id_season')->references('id')->on('seasons');
            $table->foreign('id_detail_season')->references('id')->on('detailseasons');
            $table->foreign('id_user')->references('id')->on('users');
            $table->foreign('id_betgroup')->references('id')->on('betgroups');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detailgames');
    }
}
