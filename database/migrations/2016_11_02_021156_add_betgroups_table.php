<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBetgroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('betgroups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('amount');
            $table->integer('participants_number');
            $table->integer('current_participants');
            $table->integer('id_state')->unsigned();
            $table->integer('id_season')->unsigned();
            $table->integer('first_match');
            $table->integer('last_match');
            $table->integer('number_group');
            $table->string('users_winner')->nullable();
            

            $table->foreign('id_state')->references('id')->on('states');            
            $table->foreign('id_season')->references('id')->on('seasons');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('betgroups');
    }
}
