<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 10);
            $table->string('name');
            $table->string('surname');
            $table->date('birth_date');
            $table->string('email')->unique();
            $table->string('phone_number');
            $table->string('country');
            $table->string('username');
            $table->string('password', 60);
            $table->string('security_question');
            $table->string('security_answer');
            $table->enum('user_type',['member','admin'])->default('member');
            $table->integer('deposit_limit')->nullable();
            $table->string('date_register_deposit_limit')->nullable();
            $table->integer('state_id')->unsigned();
            $table->float('balance')->default(0);

            $table->foreign('state_id')->references('id')->on('states');

            $table->rememberToken();
            $table->timestamps();
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}

