<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSeasonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seasons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('season');
            $table->integer('year');
            $table->integer('current_match');
            $table->integer('total_match');
            $table->integer('id_api_season');
            $table->integer('id_sport')->unsigned();
            $table->integer('id_state')->unsigned();

            $table->foreign('id_sport')->references('id')->on('sports');
            $table->foreign('id_state')->references('id')->on('states');        

            $table->timestamps();  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('seasons');
    }
}
