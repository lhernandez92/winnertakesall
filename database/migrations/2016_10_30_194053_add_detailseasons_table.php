<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDetailseasonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detailseasons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('week');
            $table->integer('id_home_team')->unsigned();
            $table->integer('id_visit_team')->unsigned();
            $table->integer('result_home')->nullable();
            $table->integer('result_visit')->nullable();
            $table->integer('id_season')->unsigned();
            $table->integer('id_state')->unsigned();
            $table->enum('state_match',['SCHEDULED','FINISHED','POSTPONED'])->default('SCHEDULED');
            $table->string('date');
            $table->string('hour');
            
            $table->foreign('id_home_team')->references('id')->on('teams');
            $table->foreign('id_visit_team')->references('id')->on('teams');
            $table->foreign('id_season')->references('id')->on('seasons');
            $table->foreign('id_state')->references('id')->on('states');

            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detailseasons');
    }
}
