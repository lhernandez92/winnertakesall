<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTablepostionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tablepositions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_team')->unsigned();
            $table->integer('games_played');
            $table->integer('points');
            $table->integer('id_season')->unsigned();

            $table->foreign('id_team')->references('id')->on('teams');
            $table->foreign('id_season')->references('id')->on('seasons');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tablepositions');
    }
}
