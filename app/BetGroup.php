<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BetGroup extends Model
{
	protected $table = 'betgroups';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'amount', 'participants_number', 'current_participants', 'id_state', 'id_season', 
        'first_match',  'last_match',  'number_group', 'users_winner',
     ];

    public function detailbetgroups(){
        return $this->hasMany('App\DetailBetGroup');
    }
    public function season(){
        return $this->belongsTo('App\Season');
    }
    public function state(){
        return $this->belongsTo('App\State');
    }        
}
