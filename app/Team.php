<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
	protected $table = 'teams';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id' ,'team', 
     ];

    public function detailseasons(){
        return $this->hasMany('App\DetailSeason');
    }

    public function tablepositions(){
        return $this->hasMany('App\TablePosition');
    }    
}
