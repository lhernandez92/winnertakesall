<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TablePosition extends Model
{
	protected $table = 'tablepositions';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'id_team', 'games_played', 'points', 'id_season',
     ];

    public function season(){
        return $this->belongsTo('App\Season');
    }
    public function team(){
        return $this->belongsTo('App\Team');
    } 
}
