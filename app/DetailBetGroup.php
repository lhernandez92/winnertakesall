<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailBetGroup extends Model
{
	protected $table = 'detailbetgroups';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'id_group', 'id_user', 'date_register',
     ];

    public function betgroup(){
        return $this->belongsTo('App\BetGroup');
    }
    public function user(){
        return $this->belongsTo('App\User');
    } 
}
