<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Season extends Model
{
	protected $table = 'seasons';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id' ,'season', 'year', 'current_match', 'total_match', 'id_api_season','id_sport', 'id_state',
     ];

    public function betgroups(){    	
        return $this->hasMany('App\BetGroup');
    }
    public function detailseasons(){
        return $this->hasMany('App\DetailSeason');
    }    
    public function state(){
        return $this->belongsTo('App\State');
    }
    public function sport(){
        return $this->belongsTo('App\Sport');
    } 
    public function tablepositions(){
        return $this->hasMany('App\TablePosition');
    }        
}
