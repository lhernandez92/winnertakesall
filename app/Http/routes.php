<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//********************	RUTAS CON WEB	********************//
	Route::get('/', function () {
	    return view('index');
	});	

	Route::get('/index', function () {
	    return view('index');
	});			

	Route::get('/registernow',['as' => 'registernow', function () {
		$months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
		$year = Date('Y');
	    return view('registernow',['months' => $months, 'year' => $year]);
	}]);

	Route::get('/terms&conditions', function () {
	    return view('terms-condition');
	});

	Route::get('/contactus', function () {
	    return view('contact-us');
	});	

	Route::get('/aboutus', function () {
	    return view('about-us');
	});	

	Route::get('/responsiblegambling', function () {
	    return view('responsible-gambling');
	});		

	Route::get('/verificationage', function () {
	    return view('age-policy');
	});		

	Route::get('/privacy&policy', function () {
	    return view('privacy-policy');
	});	

	Route::get('/auth/login', [
		'uses'	=>	'Auth\AuthController@getLogin',
		'as'	=>	'auth.login'
	]);	

	Route::get('/logout', [
		'uses'	=>	'Auth\AuthController@getLogout',
		'as'	=>	'auth.logout'
	]);

	Route::get('/api', [
		'uses'	=>	'FixturesController@saveApiData',
		'as'	=>	'api'
	]);	

	Route::get('/getTablePositions', [
		'uses'	=>	'FixturesController@getTablePositions',
		'as'	=>	'getTablePositions'
	]);	

	Route::get('/apiGame', [
		'uses'	=>	'FixturesController@saveDetailGame',
		'as'	=>	'apiGame'
	]);			

	Route::get('/playnow', [
		'uses'	=>	'FixturesController@playnow',
		'as'	=>	'playnow'
	]);	

	Route::post('/resumeGroup', [
		'uses'	=>	'FixturesController@resumeGroup',
		'as'	=>	'resumeGroup'
	]);	

	Route::post('/resumeGroupTest', [
		'uses'	=>	'FixturesController@resumeGroupTest',
		'as'	=>	'resumeGroupTest'
	]);	

	Route::get('/team-selection-guest', [
		'uses'	=>	'FixturesController@selectionTeamGuest',
		'as'	=>	'selectTeamGuest'
	]);

	Route::post('/selectNextWeekGuest', [
		'uses'	=>	'FixturesController@selectionTeamParamGuest',
		'as'	=>	'selectTeamParamGuest'
	]);	

	Route::auth();

	Route::get('password/email','Auth\PasswordController@getEmail');		
	Route::post('password/email','Auth\PasswordController@postEmail');		
	Route::get('password/reset/{token}','Auth\PasswordController@getReset');
	Route::post('password/reset','Auth\PasswordController@postReset');
	
	Route::resource('action/users','UsersController@store');	


	Route::get('/CJS', [
		'uses'	=>	'FixturesController@cronJobSeason',
		'as'	=>	'cronJobSeason'
	]);			

//********************	RUTAS CON AUTORIZACIÓN	********************//
Route::group(['middleware' => ['auth']], function () {

	Route::get('/account', [
		'uses'	=>	'FixturesController@showAccount',
		'as'	=>	'account'
	]);

	Route::get('/type-user', function () {
		if(auth()->user()->user_type == "member"){
			return redirect('/playnow');
		}elseif(auth()->user()->user_type == "admin"){
			return redirect('/betgroups');
		}else{
			return redirect('/');
		}
	});	

	Route::get('/betgroups', [
		'uses'	=>	'FixturesController@validateBetGroup',
		'as'	=>	'validateGroup'
	]);	

	Route::post('/depositlimit', [
		'uses'	=>	'FixturesController@storeDepositLimit',
		'as'	=>	'storeDepositLimit'
	]);

	Route::get('/team-selection', [
		'uses'	=>	'FixturesController@selectionTeam',
		'as'	=>	'selectTeam'
	]);

	Route::get('/updateDetailSeason','FixturesController@updateDetailSeason');

	Route::get('/team-selection/{id}', 'FixturesController@selectionTeamId');	

	Route::get('/fixtureMsg', 'FixturesController@showMsgFixture');	
	Route::get('/showMsgFailPrediction', 'FixturesController@showMsgFailPrediction');	

	Route::post('/selectNextWeek', [
		'uses'	=>	'FixturesController@selectionTeamParam',
		'as'	=>	'selectTeamParam'
	]);	

	Route::post('/register-group', [
		'uses'	=>	'FixturesController@registerBetGroup',
		'as'	=>	'registerGroup'
	]);

	Route::post('/delete-betgroup', [
		'uses'	=>	'FixturesController@deleteBetGroup',
		'as'	=>	'deleteGroup'
	]);				

	/*Route::get('/cleanData', [
		'uses'	=>	'FixturesController@refreshData',
		'as'	=>	'refreshData'
	]);	*/	

	Route::get('/selectNextWeek', [
		'uses'	=>	'FixturesController@selectionTeam',
		'as'	=>	'selectTeamParamGet'
	]);		

	Route::post('/team-selection', [
		'uses'	=>	'FixturesController@saveSelection',
		'as'	=>	'saveSelect'
	]);		

	Route::get('/leaderboard', [
		'uses'	=>	'FixturesController@showLeaderboard',
		'as'	=>	'leaderboard'
	]);

	Route::get('/leaderboardUser', [
		'uses'	=>	'FixturesController@showLeaderboardUser',
		'as'	=>	'leaderboardUser'
	]);		

	Route::post('/leaderboard', [
		'uses'	=>	'FixturesController@showLeaderboardParam',
		'as'	=>	'leaderboardParam'
	]);

	Route::post('/leaderboardUser', [
		'uses'	=>	'FixturesController@showLeaderboardParamUser',
		'as'	=>	'leaderboardParamUser'
	]);

	Route::get('/leaderboardUser/{id}', 'FixturesController@showLeaderboardParamUserId');

	/*Route::get('/leaderboard', function () {
	    return view('leaderboard');
	});*/

	Route::post('/joinBetGroup', [
		'uses'	=>	'FixturesController@joinBetGroup',
		'as'	=>	'joinBetGroup'
	]);			

	Route::get('/auth/logout', 'Auth\AuthController@logout');

	Route::get('/processWeek', [
		'uses'	=>	'FixturesController@processWeek',
		'as'	=>	'processWeek'
	]);		

});

