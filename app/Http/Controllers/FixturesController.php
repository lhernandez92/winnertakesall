<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Response;
use App\Http\Requests;
use GuzzleHttp\Client;
use App\TablePosition;
use App\Season;
use App\DetailSeason;
use App\DetailGame;
use App\BetGroup;
use App\DetailBetGroup;
use App\User;
use App\Team;
use DB;
use URL;
use Redirect;
use Laracasts\Flash\Flash;

class FixturesController extends Controller
{
    //

	public function cronJobSeason(){

		$idAPIseason;
	    //$uri = 'http://api.football-data.org/v1/competitions/426';
	    $uri = 'http://api.football-data.org/v1/competitions/?season=2016';
	    $reqPrefs['http']['method'] = 'GET';
	    $reqPrefs['http']['header'] = 'X-Auth-Token: 1f459607a43b4a9cb9d0063bd054a279';
	    $stream_context = stream_context_create($reqPrefs);
	    $response = file_get_contents($uri, false, $stream_context);
	    $fixturesCompetition = json_decode($response);	    	    
		
		$arrayFixtures;//Se almacena en forma de array la clase devuelta por la API
		$arrayFixtures = $fixturesCompetition;

		for ($i=0; $i < count($arrayFixtures); $i++) { 
			if($arrayFixtures[$i]->league == "PL"){
				$idAPIseason = $arrayFixtures[$i]->id;
			    $uri = 'http://api.football-data.org/v1/competitions/'.$idAPIseason;
			    $reqPrefs['http']['method'] = 'GET';
			    $reqPrefs['http']['header'] = 'X-Auth-Token: 1f459607a43b4a9cb9d0063bd054a279';
			    $stream_context = stream_context_create($reqPrefs);
			    $response = file_get_contents($uri, false, $stream_context);
			    $fixturesCompetition = json_decode($response);	
	    		$arrayFixtures = $fixturesCompetition;
			}
		}

		//dd($arrayFixtures);

	    $uri = $arrayFixtures->_links->teams->href;
	    $reqPrefs['http']['method'] = 'GET';
	    $reqPrefs['http']['header'] = 'X-Auth-Token: 1f459607a43b4a9cb9d0063bd054a279';
	    $stream_context = stream_context_create($reqPrefs);
	    $response = file_get_contents($uri, false, $stream_context);
	    $arrayTeams = json_decode($response);	  
	    $arrayTeams = $arrayTeams->teams;

	    $objSeason = new Season();
	    $objSeason->season = $arrayFixtures->caption;
	    $objSeason->year = $arrayFixtures->year;
	    $objSeason->current_match = $arrayFixtures->currentMatchday;
	    $objSeason->total_match = $arrayFixtures->numberOfMatchdays;
	    $objSeason->id_api_season = $idAPIseason;
	    $objSeason->id_sport = 1;
	    $objSeason->id_state = 1;

	    $TestSeason = DB::table('seasons')	    	
	    	->where('season', $arrayFixtures->caption)
	    	->where('id_sport', 1)
	    	->where('id_state', 1)
	    	->orderBy('id','desc')
	    	->first();

	    if(!$TestSeason){	    	

		    $LastSeason = DB::table('seasons') //Se obtiene la ultima temporada registrada activa
		    	->where('id_state', 1)
		    	->where('id_sport', 1)
		    	->orderBy('id','desc')
		    	->first();

		    if($LastSeason){ //Si existe alguna temporada anterior activa la desactiva para registrar la nueva
		    	$LastSeason->id_state = 2;
		    	$LastSeason->save();
		    }

		    if($objSeason->save()){ //Guarda la nueva temporada
		    	for ($i=0; $i < $arrayFixtures->numberOfTeams; $i++) { 
		    		$test = DB::table('teams')->where('team','=',$arrayTeams[$i]->name)->get();
		    		if(!$test){
			    		$objTeam = new Team();
			    		$objTeam->team = $arrayTeams[$i]->name;
			    		$objTeam->save();
		    		}//Evalua si existe ya registrado ese equipo
		    	}//Recorre la cantidad de equipos para registrar los que no esten

			    $uri = 'http://api.football-data.org/v1/competitions/'.$idAPIseason.'/fixtures';
			    $reqPrefs['http']['method'] = 'GET';
			    $reqPrefs['http']['header'] = 'X-Auth-Token: 1f459607a43b4a9cb9d0063bd054a279';
			    $stream_context = stream_context_create($reqPrefs);
			    $response = file_get_contents($uri, false, $stream_context);
			    $fixtures = json_decode($response);

			    $countMatch = $fixtures->count;
			    $arrayDetailSeason = $fixtures->fixtures;
			    $idSeason = DB::table('seasons')->where('season', $arrayFixtures->caption)->where('id_sport', 1)->where('id_state', 1)->select('id')->orderBy('id','desc')->first();

			    for ($i=0; $i < $countMatch; $i++) { 
			    	$homeTeam = DB::table('teams')->where('team', $arrayDetailSeason[$i]->homeTeamName)->first();
		        	$visitTeam = DB::table('teams')->where('team', $arrayDetailSeason[$i]->awayTeamName)->first();

			    	$objDetailSeason = new DetailSeason();
			    	$objDetailSeason->week = $arrayDetailSeason[$i]->matchday;		   
		        	$objDetailSeason->id_home_team = $homeTeam->id;
		        	$objDetailSeason->id_visit_team = $visitTeam->id;
		        	$objDetailSeason->result_home = $arrayDetailSeason[$i]->result->goalsHomeTeam;
		        	$objDetailSeason->result_visit = $arrayDetailSeason[$i]->result->goalsAwayTeam;
		        	$objDetailSeason->id_season = $idSeason->id;
		        	$objDetailSeason->id_state = 1;
		        	$objDetailSeason->state_match = $arrayDetailSeason[$i]->status;
		        	$date = explode('T', $arrayDetailSeason[$i]->date);
		        	$objDetailSeason->date = $date[0];
		        	$objDetailSeason->hour = $date[1];
		        	$objDetailSeason->save();		    	

			    }//Guarda en la tabla detailseasons los registros de los partidos de la temporada
		    }//Si la temporada es guardada exitosamente evalua los equipos asociados	    
	    }//Si la temporada actual ya existe registrada no realiza ningun proceso (No entra al if)*/

	}

	public function CronologicJobMatch(){
		
	}	

    public function getTablePositions(){//LLENADO DE LA TABLA TABLEPOSITIONS
		//ANTES DE LLEGAR A LLENAR LAS POSICIONES ES NECESARIO A PRINCIPIO DE TEMPORADA QUE LOS NUEVOS
		//EQUIPOS SEAN REGISTRADOS EN LA TABLA TEAMS

		$season = DB::table('seasons')			
			->where('id_sport', 1)
			->where('id_state', 1)
			->orderBy('id','desc')
			->first();

	    $uri = 'http://api.football-data.org/v1/competitions/'.$season->id_api_season.'/leagueTable/?matchday='.$season->current_match;
	    $reqPrefs['http']['method'] = 'GET';
	    $reqPrefs['http']['header'] = 'X-Auth-Token: 1f459607a43b4a9cb9d0063bd054a279';
	    $stream_context = stream_context_create($reqPrefs);
	    $response = file_get_contents($uri, false, $stream_context);
	    $fixtures = json_decode($response);	    	    
		
		$arrayFixtures;//Se almacena en forma de array la clase devuelta por la API
		$arrayFixtures = $fixtures->standing;
		$total = count($arrayFixtures);		
		//dd($arrayFixtures[0]);

		DB::table('tablepositions')->where('id_season', $season->id)->delete();	 			

		for ($i=0; $i < $total; $i++) { 			
			$tableTeams = DB::table('teams')
				->where('team',$arrayFixtures[$i]->teamName)
				->first();

				$test = new TablePosition();
				$test->id_team = $tableTeams->id;
				$test->games_played = $arrayFixtures[$i]->playedGames;
				$test->points = $arrayFixtures[$i]->points;
				$test->id_season = $season->id;
				$test->save();				
		}

    }

   	public function saveApiData(){      

		$season = DB::table('seasons')			
			->where('id_sport', 1)
			->where('id_state', 1)
			->orderBy('id','desc')
			->first();

	    $uri = 'http://api.football-data.org/v1/competitions/'.$season->id_api_season.'/leagueTable/?matchday='.$season->current_match;
	    $reqPrefs['http']['method'] = 'GET';
	    $reqPrefs['http']['header'] = 'X-Auth-Token: 1f459607a43b4a9cb9d0063bd054a279';
	    $stream_context = stream_context_create($reqPrefs);
	    $response = file_get_contents($uri, false, $stream_context);
	    $fixtures = json_decode($response);	    
		
		$arrayFixtures;//Se almacena en forma de array la clase devuelta por la API
		$arrayFixtures = $fixtures->standing;

		return $arrayFixtures;
	}

	public function selectionTeamParam(Request $request){

		$weekTemp = $request->input('weekPivot');
		$banderaButtonValidate = true;

		if($request->type == "prev"){
			$weekTemp = $request->weekPivot - 1;			
		}elseif($request->type == "next"){			
			$weekTemp = $request->weekPivot + 1;			
		}

		if($weekTemp < 1){
			$weekTemp = 1;
		}elseif($weekTemp > 38) {
			$weekTemp = 38;
		}

		$id = $request->input('idGroup');
		$tableLeague = $this->saveApiData();	
		$arrayClassTeam = [null,'hul','lei','sou','wat','mid','stk','eve','tot','cry','wba',
						'bur','swa','mci','sun','bou','mun','ars','liv','che','whu'];								

		$season = DB::table('seasons')
			->where('id_sport', 1)
			->where('id_state', 1)
			->orderBy('id','desc')
			->first();

		$registerGroups = DB::table('detailbetgroups')
			->where('id_user', auth()->user()->id)
			->get();
		
		$arrayTemp = array();
		$arrayTempDate = array();
		$cont = 0;

		for ($i=0; $i < count($registerGroups); $i++) { 
			$objTemp = BetGroup::where('id','=',$registerGroups[$i]->id_group)->where('id_state','=',1)->where('id_season','=',$season->id)->first();
			if($objTemp != null){
				/*if($objTemp->current_participants == $objTemp->participants_number){
					$arrayTemp[$cont] = $objTemp;
					$arrayTempDate[$cont] = $registerGroups[$i]->date_register;	
					$cont++;								
				}*/
				if($objTemp->current_participants > 0){
					$arrayTemp[$cont] = $objTemp;
					$arrayTempDate[$cont] = $registerGroups[$i]->date_register;	
					$cont++;								
				}
			}
		}

		$tableDetails = DB::table('detailgames')						
			->where('id_user',auth()->user()->id)
			->where('id_betgroup',$id)
			->where('id_season', $season->id)		
			->orderBy('id','desc')
			->get();						

		$tableBetGroup = DB::table('betgroups')
			->where('id_season', $season->id)
			->where('id',$id)
			->where('id_state',1)
			->first();	

		$tableDetailsSeason = DB::table('detailseasons')				
			->where('week', $weekTemp)
			->where('id_season', $season->id)
			->get();							

		if($tableDetails == null){			

			for ($i=0; $i < count($tableDetailsSeason); $i++) { 
				$objTemp = get_object_vars($tableDetailsSeason[$i]);
				$test = new DetailSeason($objTemp);
				//***********************************************//				
				if($weekTemp == $season->current_match){					
					$arrayTableHomeTeam[$i] = "";
					$arrayTableVisitTeam[$i] = "";
				}elseif($weekTemp < $season->current_match){
					$banderaButtonValidate = false;
					$arrayTableHomeTeam[$i] = "disabled";
					$arrayTableVisitTeam[$i] = "disabled";
				}elseif($weekTemp > $season->current_match){
					$banderaButtonValidate = false;
					$arrayTableHomeTeam[$i] = "disabled";
					$arrayTableVisitTeam[$i] = "disabled";
				}
				//***********************************************//
				$hourFix = $test->hour;
				$hourFix = explode(":", $hourFix);
				$test->hour = $hourFix[0].":".$hourFix[1];

				$arrayTableDetailSeason[$i] = $test;
			}			

			$response = ['arrayFix' => $arrayTableDetailSeason, 
						'week' => $season->current_match,
						'weekPivot' => $weekTemp,
						'arrayHomeTeam' => $arrayTableHomeTeam,
						'arrayVisitTeam' => $arrayTableVisitTeam,
						'tableLeague' => $tableLeague,
						'teams' => $arrayClassTeam,
						'arrayGroup' => $arrayTemp,
						'validateGroup' => $tableBetGroup,
						'banderaButton' => $banderaButtonValidate,
						'idBetGroup' => $id];

			$view = View::make('ajaxTeamSelection')->with('result',$response);
			$sections = $view->renderSections();
			return response()->json(['result' => $sections['ajaxContent']]);	

		}else{			
			$stateUser = $tableDetails[0]->state;			
			if($stateUser == 'denied'){				
				return response()->json(['result' => 'msgErrorFailPrediction']);	
			}else{				
				//Se obtiene la ultima prediccion realizada por el usuario en el grupo de apuesta seleccionado
				$objUltimaPrediccion = DetailGame::where('id_user','=',auth()->user()->id)->where('id_betgroup','=',$id)->orderBy('id','desc')->first();				
				$objValidacionTemp = DetailSeason::where('id','=',$objUltimaPrediccion->id_detail_season)->first();//Obj para validar si en la semana actual de la liga ya se selecciono un equipo

				for ($i=0; $i < count($tableDetailsSeason); $i++) { 
					$objTemp = get_object_vars($tableDetailsSeason[$i]);
					$test = new DetailSeason($objTemp);
					//***********************************************//

					if($weekTemp == $season->current_match){
						$objHomeTeam = DetailGame::where('selection_team','=',$test->id_home_team)->where('id_user','=',auth()->user()->id)->where('id_betgroup','=',$id)->first();
						$objVisitTeam = DetailGame::where('selection_team','=',$test->id_visit_team)->where('id_user','=',auth()->user()->id)->where('id_betgroup','=',$id)->first();						

						if($objValidacionTemp->week == $weekTemp){
							if($objHomeTeam != null){		
								if($objUltimaPrediccion->id == $objHomeTeam->id){
									$banderaButtonValidate = false;
									$arrayTableHomeTeam[$i] = "checked";
								}else{
									$arrayTableHomeTeam[$i] = "disabled";
								}
							}else{
								$arrayTableHomeTeam[$i] = "disabled";
							}
							if($objVisitTeam != null){
								if($objUltimaPrediccion->id == $objVisitTeam->id){
									$banderaButtonValidate = false;
									$arrayTableVisitTeam[$i] = "checked";
								}else{
									$arrayTableVisitTeam[$i] = "disabled";
								}								
							}else{
								$arrayTableVisitTeam[$i] = "disabled";
							}												
						}else{
							if($objHomeTeam != null){
								$arrayTableHomeTeam[$i] = "disabled";
							}else{
								$arrayTableHomeTeam[$i] = "";
							}
							if($objVisitTeam != null){
								$arrayTableVisitTeam[$i] = "disabled";
							}else{
								$arrayTableVisitTeam[$i] = "";
							}							
						}
					}elseif($weekTemp < $season->current_match){
						$objHomeTeam = DetailGame::where('id_detail_season','=',$test->id)->where('selection_team','=',$test->id_home_team)->where('id_user','=',auth()->user()->id)->where('id_betgroup','=',$id)->first();
						$objVisitTeam = DetailGame::where('id_detail_season','=',$test->id)->where('selection_team','=',$test->id_visit_team)->where('id_user','=',auth()->user()->id)->where('id_betgroup','=',$id)->first();												
						$banderaButtonValidate = false;
						if($objHomeTeam != null){
							$arrayTableHomeTeam[$i] = "checked";
						}else{
							$arrayTableHomeTeam[$i] = "disabled";
						}
						if($objVisitTeam != null){
							$arrayTableVisitTeam[$i] = "checked";
						}else{
							$arrayTableVisitTeam[$i] = "disabled";
						}
					}elseif($weekTemp > $season->current_match){
						$banderaButtonValidate = false;
						$objHomeTeam = DetailGame::where('id_detail_season','=',$test->id)->where('selection_team','=',$test->id_home_team)->where('id_user','=',auth()->user()->id)->where('id_betgroup','=',$id)->first();
						$objVisitTeam = DetailGame::where('id_detail_season','=',$test->id)->where('selection_team','=',$test->id_visit_team)->where('id_user','=',auth()->user()->id)->where('id_betgroup','=',$id)->first();												
						$arrayTableHomeTeam[$i] = "disabled";
						$arrayTableVisitTeam[$i] = "disabled";
					}
					//***********************************************//
					$hourFix = $test->hour;
					$hourFix = explode(":", $hourFix);
					$test->hour = $hourFix[0].":".$hourFix[1];
					
					$arrayTableDetailSeason[$i] = $test;
				}			


				$response = ['arrayFix' => $arrayTableDetailSeason, 
							'week' => $season->current_match,
							'weekPivot' => $weekTemp,
							'arrayHomeTeam' => $arrayTableHomeTeam,
							'arrayVisitTeam' => $arrayTableVisitTeam,
							'tableLeague' => $tableLeague,
							'teams' => $arrayClassTeam,
							'arrayGroup' => $arrayTemp,
							'validateGroup' => $tableBetGroup,
							'banderaButton' => $banderaButtonValidate,
							'idBetGroup' => $id];

				$view = View::make('ajaxTeamSelection')->with('result',$response);
				$sections = $view->renderSections();
				return response()->json(['result' => $sections['ajaxContent']]);	
			}
		}							

	}

	public function selectionTeamId(request $request, $id){

		$banderaButtonValidate = true;
		$tableLeague = $this->saveApiData();	
		$arrayClassTeam = [null,'hul','lei','sou','wat','mid','stk','eve','tot','cry','wba',
						'bur','swa','mci','sun','bou','mun','ars','liv','che','whu'];								

		$season = DB::table('seasons')
			->where('id_sport', 1)
			->where('id_state', 1)
			->orderBy('id','desc')
			->first();

		$registerGroups = DB::table('detailbetgroups')
			->where('id_user', auth()->user()->id)
			->get();
		
		$arrayTemp = array();
		$arrayTempDate = array();
		$cont = 0;

		for ($i=0; $i < count($registerGroups); $i++) { 
			$objTemp = BetGroup::where('id','=',$registerGroups[$i]->id_group)->where('id_state','=',1)->where('id_season','=',$season->id)->first();
			if($objTemp != null){
				/*if($objTemp->current_participants == $objTemp->participants_number){
					$arrayTemp[$cont] = $objTemp;
					$arrayTempDate[$cont] = $registerGroups[$i]->date_register;	
					$cont++;								
				}*/
				if($objTemp->current_participants > 0){
					$arrayTemp[$cont] = $objTemp;
					$arrayTempDate[$cont] = $registerGroups[$i]->date_register;	
					$cont++;								
				}							
			}
		}

		$tableDetails = DB::table('detailgames')						
			->where('id_user',auth()->user()->id)
			->where('id_betgroup',$id)
			->where('id_season', $season->id)		
			->orderBy('id','desc')
			->get();						

		$tableBetGroup = DB::table('betgroups')
			->where('id',$id)
			->where('id_state',1)
			->where('id_season', $season->id)
			->first();

		$tableDetailsSeason = DB::table('detailseasons')				
			->where('week', $season->current_match)
			->where('id_season', $season->id)
			->get();			

		if(($season->current_match <= $tableBetGroup->last_match) || (($tableBetGroup->first_match == 0) && ($tableBetGroup->last_match == 0))){
			
			if($tableDetails == null){		
				for ($i=0; $i < count($tableDetailsSeason); $i++) { 
					$objTemp = get_object_vars($tableDetailsSeason[$i]);
					$test = new DetailSeason($objTemp);
					//***********************************************//
					$arrayTableHomeTeam[$i] = "";
					$arrayTableVisitTeam[$i] = "";
					//***********************************************//
					$hourFix = $test->hour;
					$hourFix = explode(":", $hourFix);
					$test->hour = $hourFix[0].":".$hourFix[1];

					$arrayTableDetailSeason[$i] = $test;
				}			

				return view('team-selection',['arrayFix' => $arrayTableDetailSeason, 
											'week' => $season->current_match,
											'weekPivot' => $season->current_match,
											'arrayHomeTeam' => $arrayTableHomeTeam,
											'arrayVisitTeam' => $arrayTableVisitTeam,
											'tableLeague' => $tableLeague,
											'teams' => $arrayClassTeam,
											'arrayGroup' => $arrayTemp,
											'validateGroup' => $tableBetGroup,
											'banderaButton' => $banderaButtonValidate,
											'idBetGroup' => $id]);			
			}else{
				$stateUser = $tableDetails[0]->state;
				if($stateUser == 'denied'){
					return redirect('/showMsgFailPrediction');
				}else{				
					//Se obtiene la ultima prediccion realizada por el usuario en el grupo de apuesta seleccionado
					$objUltimaPrediccion = DetailGame::where('id_user','=',auth()->user()->id)->where('id_betgroup','=',$id)->orderBy('id','desc')->first();				
					$objValidacionTemp = DetailSeason::where('id','=',$objUltimaPrediccion->id_detail_season)->first();//Obj para validar si en la semana actual de la liga ya se selecciono un equipo

					for ($i=0; $i < count($tableDetailsSeason); $i++) { 
						$objTemp = get_object_vars($tableDetailsSeason[$i]);
						$test = new DetailSeason($objTemp);
						//***********************************************//
						$objHomeTeam = DetailGame::where('selection_team','=',$test->id_home_team)->where('id_user','=',auth()->user()->id)->where('id_betgroup','=',$id)->first();
						$objVisitTeam = DetailGame::where('selection_team','=',$test->id_visit_team)->where('id_user','=',auth()->user()->id)->where('id_betgroup','=',$id)->first();						
						if($objValidacionTemp->week == $season->current_match){
							if($objHomeTeam != null){		
								if($objUltimaPrediccion->id == $objHomeTeam->id){
									$banderaButtonValidate = false;
									$arrayTableHomeTeam[$i] = "checked";
								}else{
									$arrayTableHomeTeam[$i] = "disabled";
								}
							}else{
								$arrayTableHomeTeam[$i] = "disabled";
							}
							if($objVisitTeam != null){
								if($objUltimaPrediccion->id == $objVisitTeam->id){
									$banderaButtonValidate = false;
									$arrayTableVisitTeam[$i] = "checked";
								}else{
									$arrayTableVisitTeam[$i] = "disabled";
								}								
							}else{
								$arrayTableVisitTeam[$i] = "disabled";
							}												
						}else{
							if($objHomeTeam != null){
								$arrayTableHomeTeam[$i] = "disabled";
							}else{
								$arrayTableHomeTeam[$i] = "";
							}
							if($objVisitTeam != null){
								$arrayTableVisitTeam[$i] = "disabled";
							}else{
								$arrayTableVisitTeam[$i] = "";
							}							
						}
						//***********************************************//
						$hourFix = $test->hour;
						$hourFix = explode(":", $hourFix);
						$test->hour = $hourFix[0].":".$hourFix[1];
						
						$arrayTableDetailSeason[$i] = $test;
					}			

					return view('team-selection',['arrayFix' => $arrayTableDetailSeason, 
											'week' => $season->current_match,
											'weekPivot' => $season->current_match,
											'arrayHomeTeam' => $arrayTableHomeTeam,
											'arrayVisitTeam' => $arrayTableVisitTeam,
											'tableLeague' => $tableLeague,										
											'teams' => $arrayClassTeam,
											'arrayGroup' => $arrayTemp,
											'validateGroup' => $tableBetGroup,
											'banderaButton' => $banderaButtonValidate,
											'idBetGroup' => $id]);
				}
			}							
		}else{
			return redirect('/fixtureMsg');
		}

	}

	public function showMsgFixture(){
		$rutaRefresh = URL::to('/account');
		Flash::error("<h4> This week there's not any team available for you, check your bet groups  <a href='$rutaRefresh'>Review my bet groups!</a></h4>");
		return view('team-selection',['msg' => 'error']);
	}

	public function showMsgFailPrediction(){
		$rutaRefresh = URL::to('/playnow');
		Flash::error("<h4> Unlucky, your team did not win, so you are out... <a href='$rutaRefresh'>Join a new game here!</a></h4>");
		return view('team-selection',['msg' => 'error']);		
	}

	public function showLeaderboardParam(Request $request){

		if(auth()->user()->user_type == "member"){
			return redirect('/leaderboardUser');
		}

		$idBetGroup = $request->optGroups;

		$arrayClassTeam = [null,'HUL','LEI','SOU','WAT','MID','STK','EVE','TOT','CRY','WBA',
						'BUR','SWA','MCI','SUN','BOU','MUN','ARS','LIV','CHE','WHU'];		

		$season = DB::table('seasons')			
			->where('id_sport', 1)
			->where('id_state', 1)
			->orderBy('id','desc')
			->first();

		$tableBetGroup = DB::table('betgroups')
			->where('id_season', $season->id)
			->get();

		$arrayTemp = array();
		$grupoWinnerTemp = null;
		$cont = 0;
		//dd($tableBetGroup);
		for ($i=0; $i < count($tableBetGroup); $i++) { 		
			if($tableBetGroup[$i]->participants_number == $tableBetGroup[$i]->current_participants){
				if($idBetGroup == $tableBetGroup[$i]->id){
					$grupoWinnerTemp = $cont;
				}
				$arrayTemp[$cont] = $tableBetGroup[$i];
				$arrayTemp[$cont]->winner = $tableBetGroup[$i]->users_winner;
				$cont++;
			}
		}

		$arrayWinners = array();
		$arrayMembers = array();
		$countPaginator = 0;

		if((count($arrayTemp)) > 0){

			$arrayWinners = explode(",", $arrayTemp[$grupoWinnerTemp]->winner);

			$tableDetailBetGroup = DB::table('detailbetgroups')
				->where('id_group',$idBetGroup)
				->get();

			if((count($tableDetailBetGroup))>0){
				$countPaginator = ceil((count($tableDetailBetGroup)/10));
			}

			for ($i=0; $i < count($tableDetailBetGroup); $i++) { 
				$objUserTemp = DB::table('users')
					->where('id', $tableDetailBetGroup[$i]->id_user)
					->get(['id','name','surname']);

				$indice = array_search($tableDetailBetGroup[$i]->id_user, $arrayWinners);

				if($indice !== false){
					$objUserTemp[0]->winner = $tableDetailBetGroup[$i]->id_user;
				}else{
					$objUserTemp[0]->winner = null;
				}					

				$objDetailUserGame = DB::table('detailgames')
					->where('id_season',$season->id)
					->where('id_user',$objUserTemp[0]->id)
					->where('id_betgroup',$idBetGroup)
					->orderBy('week')
					->get(['selection_team','state','id_betgroup','week']);

				for ($j=0; $j < count($objDetailUserGame); $j++) { 
					$objDetailUserGame[$j]->selection_team = $arrayClassTeam[$objDetailUserGame[$j]->selection_team];
				}

				$arrayMembers[$i] = $objUserTemp;
				$arrayMembers[$i][1] = $objDetailUserGame;
			}
		}

		//dd($arrayMembers);

		return view('leaderboard',['arrayGroup' => $arrayTemp,
			'currentWeek' => $season->current_match,
			'countPaginator' => $countPaginator,
			'arrayMembers' => $arrayMembers,
			'idBetGroup' => $idBetGroup]);							
	}

	public function showLeaderboard(){

		if(auth()->user()->user_type == "member"){
			return redirect('/leaderboardUser');
		}		

		$arrayClassTeam = [null,'HUL','LEI','SOU','WAT','MID','STK','EVE','TOT','CRY','WBA',
						'BUR','SWA','MCI','SUN','BOU','MUN','ARS','LIV','CHE','WHU'];		

		$season = DB::table('seasons')			
			->where('id_sport', 1)
			->where('id_state', 1)
			->orderBy('id','desc')
			->first();

		$tableBetGroup = DB::table('betgroups')
			->where('id_season', $season->id)
			->get();

		$arrayTemp = array();
		$cont = 0;
		//dd($tableBetGroup);
		for ($i=0; $i < count($tableBetGroup); $i++) { 		
			if($tableBetGroup[$i]->participants_number == $tableBetGroup[$i]->current_participants){
				$arrayTemp[$cont] = $tableBetGroup[$i];
				$arrayTemp[$cont]->winner = $tableBetGroup[$i]->users_winner;
				$cont++;
			}
		}

		$arrayWinners = array();
		$arrayMembers = array();
		$countPaginator = 0;

		if((count($arrayTemp)) > 0){

			$arrayWinners = explode(",", $arrayTemp[0]->winner);

			$tableDetailBetGroup = DB::table('detailbetgroups')
				->where('id_group',$arrayTemp[0]->id)
				->get();

			if((count($tableDetailBetGroup))>0){
				$countPaginator = ceil((count($tableDetailBetGroup)/10));
			}

			for ($i=0; $i < count($tableDetailBetGroup); $i++) { 
				$objUserTemp = DB::table('users')
					->where('id', $tableDetailBetGroup[$i]->id_user)
					->get(['id','name','surname']);

				$indice = array_search($tableDetailBetGroup[$i]->id_user, $arrayWinners);

				if($indice !== false){
					$objUserTemp[0]->winner = $tableDetailBetGroup[$i]->id_user;
				}else{
					$objUserTemp[0]->winner = null;
				}					

				$objDetailUserGame = DB::table('detailgames')
					->where('id_season',$season->id)
					->where('id_user',$objUserTemp[0]->id)
					->where('id_betgroup',$arrayTemp[0]->id)
					->orderBy('week')
					->get(['selection_team','state','id_betgroup','week']);

				for ($j=0; $j < count($objDetailUserGame); $j++) { 
					$objDetailUserGame[$j]->selection_team = $arrayClassTeam[$objDetailUserGame[$j]->selection_team];
				}

				$arrayMembers[$i] = $objUserTemp;
				$arrayMembers[$i][1] = $objDetailUserGame;
			}
		}

		//dd($arrayMembers);

		return view('leaderboard',['arrayGroup' => $arrayTemp,
			'currentWeek' => $season->current_match,
			'countPaginator' => $countPaginator,
			'arrayMembers' => $arrayMembers]);					

	}

	public function showLeaderboardUser(){

		if(auth()->user()->user_type == "admin"){
			return redirect('/leaderboard');
		}		

		$arrayClassTeam = [null,'HUL','LEI','SOU','WAT','MID','STK','EVE','TOT','CRY','WBA',
						'BUR','SWA','MCI','SUN','BOU','MUN','ARS','LIV','CHE','WHU'];		

		$season = DB::table('seasons')			
			->where('id_sport', 1)
			->where('id_state', 1)
			->orderBy('id','desc')
			->first();

		$tableDetailBetGroup = DB::table('detailbetgroups')
			->where('id_user', auth()->user()->id)
			->get();

		$arrayTemp = array();
		$cont = 0;
		//dd($tableBetGroup);
		for ($i=0; $i < count($tableDetailBetGroup); $i++) { 	
	
			$tableBetGroup = DB::table('betgroups')
				->where('id_season', $season->id)
				->where('id', $tableDetailBetGroup[$i]->id_group)
				->first();

			if($tableBetGroup != null){
				if($tableBetGroup->participants_number == $tableBetGroup->current_participants){
					$arrayTemp[$cont] = $tableBetGroup;
					$arrayTemp[$cont]->winner = $tableBetGroup->users_winner;
					$cont++;
				}				
			}		

		}

		$arrayWinners = array();
		$arrayMembers = array();
		$countPaginator = 0;

		if((count($arrayTemp)) > 0){

			$arrayWinners = explode(",", $arrayTemp[0]->winner);		

			$tableDetailBetGroup = DB::table('detailbetgroups')
				->where('id_group',$arrayTemp[0]->id)
				->get();

			if((count($tableDetailBetGroup))>0){
				$countPaginator = ceil((count($tableDetailBetGroup)/10));
			}

			for ($i=0; $i < count($tableDetailBetGroup); $i++) { 
				$objUserTemp = DB::table('users')
					->where('id', $tableDetailBetGroup[$i]->id_user)
					->get(['id','name','surname']);

				$indice = array_search($tableDetailBetGroup[$i]->id_user, $arrayWinners);

				if($indice !== false){
					$objUserTemp[0]->winner = $tableDetailBetGroup[$i]->id_user;
				}else{
					$objUserTemp[0]->winner = null;
				}

				$objDetailUserGame = DB::table('detailgames')
					->where('id_season',$season->id)
					->where('id_user',$objUserTemp[0]->id)
					->where('id_betgroup',$arrayTemp[0]->id)
					->orderBy('week')
					->get(['selection_team','state','id_betgroup','week']);

				for ($j=0; $j < count($objDetailUserGame); $j++) { 
					$objDetailUserGame[$j]->selection_team = $arrayClassTeam[$objDetailUserGame[$j]->selection_team];
				}

				$arrayMembers[$i] = $objUserTemp;
				$arrayMembers[$i][1] = $objDetailUserGame;
			}
		}

		return view('leaderboardUser',['arrayGroup' => $arrayTemp,
			'currentWeek' => $season->current_match,
			'countPaginator' => $countPaginator,
			'arrayMembers' => $arrayMembers]);					

	}

	public function showLeaderboardParamUserId(request $request, $id){
		if(auth()->user()->user_type == "admin"){
			return redirect('/leaderboard');
		}

		$idBetGroup = $id;

		$arrayClassTeam = [null,'HUL','LEI','SOU','WAT','MID','STK','EVE','TOT','CRY','WBA',
						'BUR','SWA','MCI','SUN','BOU','MUN','ARS','LIV','CHE','WHU'];		

		$season = DB::table('seasons')			
			->where('id_sport', 1)
			->where('id_state', 1)
			->orderBy('id','desc')
			->first();

		$tableDetailBetGroup = DB::table('detailbetgroups')
			->where('id_user', auth()->user()->id)
			->get();

		$arrayTemp = array();
		$grupoWinnerTemp = null;
		$cont = 0;
		//dd($tableBetGroup);
		for ($i=0; $i < count($tableDetailBetGroup); $i++) { 	
	
			$tableBetGroup = DB::table('betgroups')
				->where('id_season', $season->id)
				->where('id', $tableDetailBetGroup[$i]->id_group)
				->first();

			if($tableBetGroup != null){
				if($tableBetGroup->participants_number == $tableBetGroup->current_participants){
					if($idBetGroup == $tableDetailBetGroup[$i]->id_group){
						$grupoWinnerTemp = $cont;
					}
					$arrayTemp[$cont] = $tableBetGroup;
					$arrayTemp[$cont]->winner = $tableBetGroup->users_winner;
					$cont++;
				}				
			}		

		}

		$arrayWinners = array();
		$arrayMembers = array();
		$countPaginator = 0;

		if((count($arrayTemp)) > 0){

			$arrayWinners = explode(",", $arrayTemp[$grupoWinnerTemp]->winner);

			$tableDetailBetGroup = DB::table('detailbetgroups')
				->where('id_group',$idBetGroup)
				->get();

			if((count($tableDetailBetGroup))>0){
				$countPaginator = ceil((count($tableDetailBetGroup)/10));
			}			

			for ($i=0; $i < count($tableDetailBetGroup); $i++) { 
				$objUserTemp = DB::table('users')
					->where('id', $tableDetailBetGroup[$i]->id_user)
					->get(['id','name','surname']);

				$indice = array_search($tableDetailBetGroup[$i]->id_user, $arrayWinners);

				if($indice !== false){
					$objUserTemp[0]->winner = $tableDetailBetGroup[$i]->id_user;
				}else{
					$objUserTemp[0]->winner = null;
				}					

				$objDetailUserGame = DB::table('detailgames')
					->where('id_season',$season->id)
					->where('id_user',$objUserTemp[0]->id)
					->where('id_betgroup',$idBetGroup)
					->orderBy('week')
					->get(['selection_team','state','id_betgroup','week']);

				for ($j=0; $j < count($objDetailUserGame); $j++) { 
					$objDetailUserGame[$j]->selection_team = $arrayClassTeam[$objDetailUserGame[$j]->selection_team];
				}

				$arrayMembers[$i] = $objUserTemp;
				$arrayMembers[$i][1] = $objDetailUserGame;
			}
		}

		//dd($arrayMembers);

		return view('leaderboardUser',['arrayGroup' => $arrayTemp,
			'currentWeek' => $season->current_match,
			'countPaginator' => $countPaginator,
			'arrayMembers' => $arrayMembers,
			'idBetGroup' => $idBetGroup]);							
	}	

	public function showLeaderboardParamUser(Request $request){

		if(auth()->user()->user_type == "admin"){
			return redirect('/leaderboard');
		}

		$idBetGroup = $request->optGroups;

		$arrayClassTeam = [null,'HUL','LEI','SOU','WAT','MID','STK','EVE','TOT','CRY','WBA',
						'BUR','SWA','MCI','SUN','BOU','MUN','ARS','LIV','CHE','WHU'];		

		$season = DB::table('seasons')			
			->where('id_sport', 1)
			->where('id_state', 1)
			->orderBy('id','desc')
			->first();

		$tableDetailBetGroup = DB::table('detailbetgroups')
			->where('id_user', auth()->user()->id)
			->get();

		$arrayTemp = array();
		$grupoWinnerTemp = null;
		$cont = 0;
		//dd($tableBetGroup);
		for ($i=0; $i < count($tableDetailBetGroup); $i++) { 	
	
			$tableBetGroup = DB::table('betgroups')
				->where('id_season', $season->id)
				->where('id', $tableDetailBetGroup[$i]->id_group)
				->first();

			if($tableBetGroup != null){
				if($tableBetGroup->participants_number == $tableBetGroup->current_participants){
					if($idBetGroup == $tableDetailBetGroup[$i]->id_group){
						$grupoWinnerTemp = $cont;
					}
					$arrayTemp[$cont] = $tableBetGroup;
					$arrayTemp[$cont]->winner = $tableBetGroup->users_winner;
					$cont++;
				}				
			}		

		}

		$arrayWinners = array();
		$arrayMembers = array();
		$countPaginator = 0;

		if((count($arrayTemp)) > 0){

			$arrayWinners = explode(",", $arrayTemp[$grupoWinnerTemp]->winner);

			$tableDetailBetGroup = DB::table('detailbetgroups')
				->where('id_group',$idBetGroup)
				->get();

			if((count($tableDetailBetGroup))>0){
				$countPaginator = ceil((count($tableDetailBetGroup)/10));
			}			

			for ($i=0; $i < count($tableDetailBetGroup); $i++) { 
				$objUserTemp = DB::table('users')
					->where('id', $tableDetailBetGroup[$i]->id_user)
					->get(['id','name','surname']);

				$indice = array_search($tableDetailBetGroup[$i]->id_user, $arrayWinners);

				if($indice !== false){
					$objUserTemp[0]->winner = $tableDetailBetGroup[$i]->id_user;
				}else{
					$objUserTemp[0]->winner = null;
				}					

				$objDetailUserGame = DB::table('detailgames')
					->where('id_season',$season->id)
					->where('id_user',$objUserTemp[0]->id)
					->where('id_betgroup',$idBetGroup)
					->orderBy('week')
					->get(['selection_team','state','id_betgroup','week']);

				for ($j=0; $j < count($objDetailUserGame); $j++) { 
					$objDetailUserGame[$j]->selection_team = $arrayClassTeam[$objDetailUserGame[$j]->selection_team];
				}

				$arrayMembers[$i] = $objUserTemp;
				$arrayMembers[$i][1] = $objDetailUserGame;
			}
		}

		//dd($arrayMembers);

		return view('leaderboardUser',['arrayGroup' => $arrayTemp,
			'currentWeek' => $season->current_match,
			'countPaginator' => $countPaginator,
			'arrayMembers' => $arrayMembers,
			'idBetGroup' => $idBetGroup]);							
	}	

	public function selectionTeam(){	

		$banderaButtonValidate = true;
		$idBetGroup = null;
		$tableLeague = $this->saveApiData();	
		$arrayClassTeam = [null,'hul','lei','sou','wat','mid','stk','eve','tot','cry','wba',
						'bur','swa','mci','sun','bou','mun','ars','liv','che','whu'];

		$season = DB::table('seasons')			
			->where('id_sport', 1)
			->where('id_state', 1)
			->orderBy('id','desc')
			->first();

		$registerGroups = DB::table('detailbetgroups')
			->where('id_user', auth()->user()->id)
			->get();
		
		$arrayTemp = array();
		$arrayTempDate = array();
		$cont = 0;

		for ($i=0; $i < count($registerGroups); $i++) { 
			$objTemp = BetGroup::where('id','=',$registerGroups[$i]->id_group)->where('id_state','=',1)->where('id_season','=',$season->id)->first();
			if($objTemp != null){
				/*if($objTemp->current_participants == $objTemp->participants_number){
					$arrayTemp[$cont] = $objTemp;
					$arrayTempDate[$cont] = $registerGroups[$i]->date_register;	
					$cont++;								
				}*/
				if($objTemp->current_participants > 0){
					$arrayTemp[$cont] = $objTemp;
					$arrayTempDate[$cont] = $registerGroups[$i]->date_register;	
					$cont++;								
				}						
			}
		}															

		if(count($arrayTemp) > 0){
			$tableDetails = DB::table('detailgames')						
				->where('id_user',auth()->user()->id)
				->where('id_season', $season->id)
				->orderBy('id','desc')
				->get();	

			$validarExistRegisterDetailGame = DetailGame::where('id_user','=',auth()->user()->id)->where('id_betgroup','=',$arrayTemp[0]->id)->orderBy('id','desc')->first();
			
			if($validarExistRegisterDetailGame == null){
				$tableDetails = null;	
				$idBetGroup = $arrayTemp[0]->id;						
			}else{
				$idBetGroup = $arrayTemp[0]->id;
			}

		}else{
			$tableDetails = null;	
			$arrayTemp[0] = null;
		}
						

		$tableBetGroup = DB::table('betgroups')
			->where('id',$idBetGroup)
			->where('id_state',1)
			->where('id_season', $season->id)
			->first();

		$tableDetailsSeason = DB::table('detailseasons')				
			->where('week', $season->current_match)
			->where('id_season', $season->id)
			->get();		


		if(($arrayTemp[0] != null) && ($tableBetGroup != null)){

			if(($season->current_match <= $tableBetGroup->last_match) || (($tableBetGroup->first_match == 0) && ($tableBetGroup->last_match == 0))) {

				if($tableDetails == null){			

					for ($i=0; $i < count($tableDetailsSeason); $i++) { 
						$objTemp = get_object_vars($tableDetailsSeason[$i]);
						$test = new DetailSeason($objTemp);
						//***********************************************//
						$arrayTableHomeTeam[$i] = "";
						$arrayTableVisitTeam[$i] = "";
						//***********************************************//
						$hourFix = $test->hour;
						$hourFix = explode(":", $hourFix);
						$test->hour = $hourFix[0].":".$hourFix[1];

						$arrayTableDetailSeason[$i] = $test;
					}			

					return view('team-selection',['arrayFix' => $arrayTableDetailSeason, 
												'week' => $season->current_match,
												'weekPivot' => $season->current_match,
												'arrayHomeTeam' => $arrayTableHomeTeam,
												'arrayVisitTeam' => $arrayTableVisitTeam,
												'tableLeague' => $tableLeague,
												'teams' => $arrayClassTeam,
												'arrayGroup' => $arrayTemp,
												'validateGroup' => $tableBetGroup,
												'banderaButton' => $banderaButtonValidate,
												'idBetGroup' => $idBetGroup]);
				}else{
					$stateUser = $tableDetails[0]->state;
					if($stateUser == 'denied'){
						return redirect('/showMsgFailPrediction');
					}else{				
						//Se obtiene la ultima prediccion realizada por el usuario en el grupo de apuesta seleccionado					
						$objUltimaPrediccion = DetailGame::where('id_user','=',auth()->user()->id)->where('id_betgroup','=',$idBetGroup)->orderBy('id','desc')->first();
						$objValidacionTemp = DetailSeason::where('id','=',$objUltimaPrediccion->id_detail_season)->first();//Obj para validar si en la semana actual de la liga ya se selecciono un equipo

						for ($i=0; $i < count($tableDetailsSeason); $i++) { 
							$objTemp = get_object_vars($tableDetailsSeason[$i]);
							$test = new DetailSeason($objTemp);
							//***********************************************//
							$objHomeTeam = DetailGame::where('selection_team','=',$test->id_home_team)->where('id_user','=',auth()->user()->id)->where('id_betgroup','=',$idBetGroup)->first();
							$objVisitTeam = DetailGame::where('selection_team','=',$test->id_visit_team)->where('id_user','=',auth()->user()->id)->where('id_betgroup','=',$idBetGroup)->first();						
							if($objValidacionTemp->week == $season->current_match){
								if($objHomeTeam != null){		
									if($objUltimaPrediccion->id == $objHomeTeam->id){
										$banderaButtonValidate = false;
										$arrayTableHomeTeam[$i] = "checked";
									}else{
										$arrayTableHomeTeam[$i] = "disabled";
									}
								}else{
									$arrayTableHomeTeam[$i] = "disabled";
								}
								if($objVisitTeam != null){
									if($objUltimaPrediccion->id == $objVisitTeam->id){
										$banderaButtonValidate = false;
										$arrayTableVisitTeam[$i] = "checked";
									}else{
										$arrayTableVisitTeam[$i] = "disabled";
									}								
								}else{
									$arrayTableVisitTeam[$i] = "disabled";
								}												
							}else{
								if($objHomeTeam != null){
									$arrayTableHomeTeam[$i] = "disabled";
								}else{
									$arrayTableHomeTeam[$i] = "";
								}
								if($objVisitTeam != null){
									$arrayTableVisitTeam[$i] = "disabled";
								}else{
									$arrayTableVisitTeam[$i] = "";
								}							
							}
							//***********************************************//
							$hourFix = $test->hour;
							$hourFix = explode(":", $hourFix);
							$test->hour = $hourFix[0].":".$hourFix[1];
							
							$arrayTableDetailSeason[$i] = $test;
						}			

						return view('team-selection',['arrayFix' => $arrayTableDetailSeason, 
												'week' => $season->current_match,
												'weekPivot' => $season->current_match,
												'arrayHomeTeam' => $arrayTableHomeTeam,
												'arrayVisitTeam' => $arrayTableVisitTeam,
												'tableLeague' => $tableLeague,										
												'teams' => $arrayClassTeam,
												'arrayGroup' => $arrayTemp,
												'validateGroup' => $tableBetGroup,
												'banderaButton' => $banderaButtonValidate,
												'idBetGroup' => $idBetGroup]);
					}
				}		
			}else{
				return redirect('/fixtureMsg');
			}
		}else{

			for ($i=0; $i < count($tableDetailsSeason); $i++) { 
				$objTemp = get_object_vars($tableDetailsSeason[$i]);
				$test = new DetailSeason($objTemp);
				//***********************************************//
				$arrayTableHomeTeam[$i] = "";
				$arrayTableVisitTeam[$i] = "";
				//***********************************************//
				$hourFix = $test->hour;
				$hourFix = explode(":", $hourFix);
				$test->hour = $hourFix[0].":".$hourFix[1];

				$arrayTableDetailSeason[$i] = $test;
			}			

			return view('team-selection',['arrayFix' => $arrayTableDetailSeason, 
										'week' => $season->current_match,
										'weekPivot' => $season->current_match,
										'arrayHomeTeam' => $arrayTableHomeTeam,
										'arrayVisitTeam' => $arrayTableVisitTeam,
										'tableLeague' => $tableLeague,
										'teams' => $arrayClassTeam,
										'arrayGroup' => $arrayTemp,
										'validateGroup' => $tableBetGroup,
										'banderaButton' => $banderaButtonValidate,
										'idBetGroup' => $idBetGroup]);
		}


	}

	public function selectionTeamGuest(){

		$tableLeague = $this->saveApiData();	
		$arrayClassTeam = [null,'hul','lei','sou','wat','mid','stk','eve','tot','cry','wba',
						'bur','swa','mci','sun','bou','mun','ars','liv','che','whu'];

		$season = DB::table('seasons')			
			->where('id_sport', 1)
			->where('id_state', 1)
			->orderBy('id','desc')
			->first();

		$tableDetailsSeason = DB::table('detailseasons')				
			->where('week', $season->current_match)
			->where('id_season', $season->id)
			->get();

		for ($i=0; $i < count($tableDetailsSeason); $i++) { 
			$objTemp = get_object_vars($tableDetailsSeason[$i]);
			$test = new DetailSeason($objTemp);
			//***********************************************//
			$arrayTableHomeTeam[$i] = "";
			$arrayTableVisitTeam[$i] = "";
			//***********************************************//
			$hourFix = $test->hour;
			$hourFix = explode(":", $hourFix);
			$test->hour = $hourFix[0].":".$hourFix[1];

			$arrayTableDetailSeason[$i] = $test;
		}			

		return view('team-selection-guest',['arrayFix' => $arrayTableDetailSeason, 
									'week' => $season->current_match,
									'weekPivot' => $season->current_match,
									'arrayHomeTeam' => $arrayTableHomeTeam,
									'arrayVisitTeam' => $arrayTableVisitTeam,
									'tableLeague' => $tableLeague,
									'teams' => $arrayClassTeam]);					
	}

	public function selectionTeamParamGuest(Request $request){	
		$weekTemp = $request->input('weekPivot');

		if($request->type == "prev"){
			$weekTemp = $request->weekPivot - 1;			
		}elseif($request->type == "next"){			
			$weekTemp = $request->weekPivot + 1;			
		}

		if($weekTemp < 1){
			$weekTemp = 1;
		}elseif($weekTemp > 38) {
			$weekTemp = 38;
		}

		$tableLeague = $this->saveApiData();	
		$arrayClassTeam = [null,'hul','lei','sou','wat','mid','stk','eve','tot','cry','wba',
						'bur','swa','mci','sun','bou','mun','ars','liv','che','whu'];								

		$season = DB::table('seasons')
			->where('id_sport', 1)
			->where('id_state', 1)
			->orderBy('id','desc')
			->first();

		$tableDetailsSeason = DB::table('detailseasons')				
			->where('week', $weekTemp)
			->where('id_season', $season->id)
			->get();							
		

			for ($i=0; $i < count($tableDetailsSeason); $i++) { 
				$objTemp = get_object_vars($tableDetailsSeason[$i]);
				$test = new DetailSeason($objTemp);
				//***********************************************//	
				$arrayTableHomeTeam[$i] = "";
				$arrayTableVisitTeam[$i] = "";							
				//***********************************************//
				$hourFix = $test->hour;
				$hourFix = explode(":", $hourFix);
				$test->hour = $hourFix[0].":".$hourFix[1];

				$arrayTableDetailSeason[$i] = $test;
			}			

			$response = ['arrayFix' => $arrayTableDetailSeason, 
						'week' => $season->current_match,
						'weekPivot' => $weekTemp,
						'arrayHomeTeam' => $arrayTableHomeTeam,
						'arrayVisitTeam' => $arrayTableVisitTeam,
						'tableLeague' => $tableLeague,
						'teams' => $arrayClassTeam];

			$view = View::make('ajaxTeamSelectionGuest')->with('result',$response);
			$sections = $view->renderSections();
			return response()->json(['result' => $sections['ajaxContent']]);				
	}

	public function saveSelection(Request $request){
		
		if($request->input('match') != null){
	        if ($request->currentWeek == -1) {

				/*
				$season = DB::table('seasons')
					->orderBy('id','desc')			
					->where('id_sport', 1)
					->where('id_state', 1)
					->first();

				DB::table('seasons')
		            ->where('id', $season->id)
		            ->update(['current_match' => 1]); 							             
				
				DB::table('detailgames')->delete();	*/                  	

	        }else{
				$typeTeam = 0;
				$teamSelected = $request->input('match');
				$selectedGroup = $request->input('idBetGroup');
				
				$season = DB::table('seasons')
					->orderBy('id','desc')			
					->where('id_sport', 1)
					->where('id_state', 1)
					->first();		
				
				$seasonDetail = DB::table('detailseasons')
					->where([
						['week', '=' , $season->current_match],
						['id_home_team', '=' ,$teamSelected],
						['id_season', '=' ,$season->id],
					])
					->orWhere([
						['week', '=' , $season->current_match],
						['id_visit_team', '=' ,$teamSelected],
						['id_season', '=' ,$season->id],
					])
					->first();

				$gameDetail = new DetailGame();
				$gameDetail->id_season = $season->id;
				$gameDetail->id_detail_season = $seasonDetail->id;
				$gameDetail->id_user = auth()->user()->id;
				$gameDetail->selection_team = $teamSelected;
				$gameDetail->state = 'null';
				$gameDetail->week = $season->current_match;
				$gameDetail->id_betgroup = $selectedGroup;
				$gameDetail->save(); 	
	        }
			
		}
		
		return redirect('/account');
	}

	/*public function refreshData(){
		DB::table('seasons')
	        ->where('id', 1)
	        ->update(['current_match' => 1]);  
		
		DB::table('detailgames')->delete();	  	
		return redirect('/playnow');	
	}*/

	public function registerBetGroup(Request $request){

		$bandera = 0;
		$season = DB::table('seasons')
			->where('id_sport', 1)
			->where('id_state', 1)
			->orderBy('id','desc')
			->first();

		$activeBetGroups = DB::table('betgroups')
			->where('id_season', $season->id)
			->where('id_state', 1)
			->distinct('amount')
			->get();

		$totalActiveGroups = count($activeBetGroups);
		
		for ($i=0; $i < $totalActiveGroups; $i++) { 
			if($activeBetGroups[$i]->amount == $request->amount){
				$bandera = 1;
				break;
			}
		}

		if($bandera == 0){
			if($totalActiveGroups < 4){
				$betGroup = new BetGroup();
				$betGroup->amount = $request->amount;
				$betGroup->participants_number = $request->players;
				$betGroup->current_participants = 0;
				$betGroup->id_state = 1;
				$betGroup->id_season = $season->id;

				$betGroup->number_group = 1;
				$betGroup->save();

				Flash::success("<h4> Excelent, you registered a new bet group!.</h4>");
			}else{
				Flash::error("<h4> Sorry, your limit is four bet groups!.</h4>");
			}
		}else{
			Flash::error("<h4> Sorry, already bet groups exist with that amount!.</h4>");	
		}
		
		$activeBetGroups = DB::table('betgroups')
			->where('id_season', $season->id)
			->where('id_state', 1)
			->distinct('amount')
			->get();
		
		return view('betgroups',['msg' => 'ok','actualSeason' => $season->season, 'activeBetGroups' => $activeBetGroups]);

	}

	public function validateBetGroup(){//FALTA AGREGAR VALIDACION DE CUANDO NO EXISTA NINGUNA TEMPORADA ACTIVA

		if(auth()->user()->user_type == "member"){

			return redirect('/account');			

		}elseif(auth()->user()->user_type == "admin"){

			$season = DB::table('seasons')			
				->where('id_sport', 1)
				->where('id_state', 1)
				->orderBy('id','desc')
				->first();

			$activeBetGroups = DB::table('betgroups')
				->where('id_season', $season->id)
				->where('id_state', 1)
				->distinct('amount')
				->get();

			return view('betgroups',['actualSeason' => $season->season, 'activeBetGroups' => $activeBetGroups]);

		}else{

			return redirect('/');
		}		

	}

	public function deleteBetGroup(Request $request){

		$season = DB::table('seasons')			
			->where('id_sport', 1)
			->where('id_state', 1)
			->orderBy('id','desc')
			->first();		

		$validarDeleteGroup = DB::table('betgroups')
			->where('id',$request->idBetGroup)
			->where('id_state',1)
			->where('id_season', $season->id)
			->first();

		if(($validarDeleteGroup->current_participants == 0) && ($validarDeleteGroup->number_group == 1)){
			DB::table('betgroups')
				->where('id',$request->idBetGroup)	  
				->delete();

			Flash::success("<h4> Bet group deleted successfully!.</h4>");			
		}else{
			Flash::error("<h4> Sorry, this bet group is actually in use!.</h4>");			
		}

		$activeBetGroups = DB::table('betgroups')
			->where('id_season', $season->id)
			->where('id_state', 1)
			->distinct('amount')
			->get();		

		return view('betgroups',['actualSeason' => $season->season, 'activeBetGroups' => $activeBetGroups]);
	
	}//cierra la funcion deleteBetGroup

	public function playnow(){

		$season = DB::table('seasons')			
			->where('id_sport', 1)
			->where('id_state', 1)
			->orderBy('id','desc')
			->first();

		$activeBetGroups = DB::table('betgroups')
			->where('id_season', $season->id)
			->where('id_state', 1)
			->orderBy('id','desc')
			->distinct()
			->get();

		$arrayTemp = array();
		$arrayObjTemp = array();
		$id = null;
		for ($i=0; $i < count($activeBetGroups); $i++) { 
			if($i == 0){
				$id = $activeBetGroups[$i]->id;
				array_push($arrayTemp, $activeBetGroups[$i]->amount);
				$arrayObjTemp[$i] = $activeBetGroups[$i];
			}else{

				if(in_array($activeBetGroups[$i]->amount, $arrayTemp)){
					$activeBetGroups[$i] = null;
					$arrayObjTemp[$i] = $activeBetGroups[$i];
				}else{
					array_push($arrayTemp, $activeBetGroups[$i]->amount);
					$arrayObjTemp[$i] = $activeBetGroups[$i];
				}
			}
 		}

 		$arrayObjTemp = array_filter($arrayObjTemp)	;
 		sort($arrayObjTemp);
		$activeBetGroups = $arrayObjTemp;

		$groupDetails = DB::table('betgroups')
			->where('id_season', $season->id)
			->where('id_state', 1)
			->where('id', $id)
			->get();

		return view('playnow',['actualSeason' => $season->season, 'activeBetGroups' => $activeBetGroups, 'groupDetails' => $groupDetails]);

	}//Cierra la funcion playnow

	public function resumeGroup(Request $request){

		$id = $request->input('idGroup');
		$season = DB::table('seasons')			
			->where('id_sport', 1)
			->where('id_state', 1)
			->orderBy('id','desc')
			->first();

		$activeBetGroups = DB::table('betgroups')
			->where('id_season', $season->id)
			->where('id_state', 1)
			->orderBy('id','desc')
			->distinct('amount')
			->get();

		$groupDetails = DB::table('betgroups')
			->where('id_season', $season->id)
			->where('id_state', 1)
			->where('id', $id)
			->get();

		$arrayTemp = array();
		$arrayObjTemp = array();
		for ($i=0; $i < count($activeBetGroups); $i++) { 
			if($i == 0){
				array_push($arrayTemp, $activeBetGroups[$i]->amount);
				$arrayObjTemp[$i] = $activeBetGroups[$i];
			}else{

				if(in_array($activeBetGroups[$i]->amount, $arrayTemp)){
					$activeBetGroups[$i] = null;
					$arrayObjTemp[$i] = $activeBetGroups[$i];
				}else{
					array_push($arrayTemp, $activeBetGroups[$i]->amount);
					$arrayObjTemp[$i] = $activeBetGroups[$i];
				}
			}
 		}

 		$arrayObjTemp = array_filter($arrayObjTemp)	;
 		sort($arrayObjTemp);
		$activeBetGroups = $arrayObjTemp;			

		return view('playnow',['actualSeason' => $season->season, 'activeBetGroups' => $activeBetGroups, 'groupDetails' => $groupDetails]);
	
	}//Cierra la funcion resumeGroup

	public function joinBetGroup(Request $request){

		$fecha = date("Y-m-d");
		$id = $request->input('idGroup');

		$season = DB::table('seasons')			
			->where('id_sport', 1)
			->where('id_state', 1)
			->orderBy('id','desc')
			->first();		

		$balanceValidation = BetGroup::where('id_season','=', $season->id)->where('id_state','=', 1)->where('id','=', $id)->first();
		$balanceUser = User::where('id','=',auth()->user()->id)->first();
		$resultBalance = $balanceUser->balance - $balanceValidation->amount;

		if($resultBalance >= 0){
			$validarUserRegistered = DetailBetGroup::where('id_group','=',$id)->where('id_user','=',auth()->user()->id)->first();

			if($validarUserRegistered == null){

				$objBetGroup = BetGroup::where('id_season','=', $season->id)->where('id_state','=', 1)->where('id','=', $id)->first();
				$memberActives =  $objBetGroup->current_participants + 1;

				if($season->current_match < 34){

					if($objBetGroup->participants_number == $memberActives){
						$objBetGroup->current_participants = $objBetGroup->current_participants + 1;

						if($season->current_match != 1){
							$objBetGroup->first_match = (($season->current_match)+1);

							if(($season->current_match + 20) > $season->total_match){
								$objBetGroup->last_match = $season->total_match;
							}else{
								$objBetGroup->last_match = $season->current_match + 20;
							}									
						}else{
							$objBetGroup->first_match = $season->current_match;
							$objBetGroup->last_match =  20;
						}

						$objBetGroup->save();

						$objDetailBetGroup = new DetailBetGroup();
						$objDetailBetGroup->id_group = $objBetGroup->id;
						$objDetailBetGroup->id_user = auth()->user()->id;
						$objDetailBetGroup->date_register = $fecha;
						$objDetailBetGroup->save();

						$objNewBetGroup = new BetGroup();
						$objNewBetGroup->amount = $objBetGroup->amount;
						$objNewBetGroup->participants_number = $objBetGroup->participants_number;
						$objNewBetGroup->current_participants = 0;
						$objNewBetGroup->id_state = 1;
						$objNewBetGroup->id_season = $season->id;
						$objNewBetGroup->number_group = $objBetGroup->number_group + 1;	

						if(($season->current_match + 20) < $season->total_match){
							$objNewBetGroup->save();				
						}

						$balanceUser->balance = $resultBalance;
						$balanceUser->save();						

					}else{

						$objBetGroup->current_participants = $objBetGroup->current_participants + 1;
						$objBetGroup->save();

						$balanceUser->balance = $resultBalance;
						$balanceUser->save();

						$objDetailBetGroup = new DetailBetGroup();
						$objDetailBetGroup->id_group = $objBetGroup->id;
						$objDetailBetGroup->id_user = auth()->user()->id;
						$objDetailBetGroup->date_register = $fecha;
						$objDetailBetGroup->save();
					}
				}
			}else{//AGREGAR AQUÍ LA VALIDACIÓN CUANDO EL USUARIO YA ESTA REGISTRADO EN EL GRUPO DE APUESTA SELECCIONADO
				$rutaRefresh = URL::to('/playnow');
				Flash::error("<h4> Ops! sorry you are actually registered in this bet group... <br><a href='$rutaRefresh'>Try a new game here!</a></h4>");			
				$groupDetails[0] = new BetGroup();
				$groupDetails[0]->amount = 0;
				return view('playnow',['msg' => 'error', 'groupDetails' => $groupDetails]);					
			}
		}else{
			$rutaRefresh = URL::to('/playnow');
			Flash::error("<h4> Ops! sorry, your balance is too low to registered in this bet group... <br><a href='$rutaRefresh'>Try a new game here!</a></h4>");			
			$groupDetails[0] = new BetGroup();
			$groupDetails[0]->amount = 0;
			return view('playnow',['msg' => 'error', 'groupDetails' => $groupDetails]);				
		}

		return redirect('/account');

	}//Cierra la funcion joinBetGroup

	public function showAccount(){

	
		$season = DB::table('seasons')			
			->where('id_sport', 1)
			->where('id_state', 1)
			->orderBy('id','desc')
			->first();

		$registerGroups = DB::table('detailbetgroups')
			->where('id_user', auth()->user()->id)
			->get();
	
	/* GRUPOS ACTIVOS */

		$arrayTemp = array();
		$arrayTempDate = array();
		$arrayTempGameCode = array();
		$arrayStateMatch = array();
		$cont = 0;
		$date = null;

	/* CIERRA GRUPOS ACTIVOS */

	/* GRUPOS INACTIVOS */

		$arraySeasonInactive = array();
		$arrayTempInactive = array();
		$arrayTempDateInactive = array();
		$arrayTempGameCodeInactive = array();
		$arrayStateMatchInactive = array();

		$contInactive = 0;				

	/* CIERRA GRUPOS INACTIVOS */

		for ($i=0; $i < count($registerGroups); $i++) { 
			$objTemp = BetGroup::where('id','=',$registerGroups[$i]->id_group)->where('id_state','=',1)->where('id_season','=',$season->id)->first();
			if($objTemp != null){
				/*SE CALCULA SI EL ULTIMO EQUIPO SELECCIONADO CONINCIDE CON LA SEMANA ACTUAL PARA MANEJAR
				LOS ENLACES DE MY ACCOUNT Y MOSTRAR LOS ESTADOS 'PLAY NEXT GAME' O 'AWAITING RESULT'*/
				$objDetailGame = DetailGame::where('id_betgroup','=',$registerGroups[$i]->id_group)->where('id_user','=',auth()->user()->id)->orderBy('id_detail_season','desc')->first();
				if($objDetailGame != null){
					$objState = DetailSeason::where('id','=',$objDetailGame->id_detail_season)->first();
					if($objState->week == $season->current_match){
						$arrayStateMatch[$cont] = true;
					}else{
						$arrayStateMatch[$cont] = false;
					}
				}else{
					$arrayStateMatch[$cont] = false;
				}
				$arrayTemp[$cont] = $objTemp;
				$date = $registerGroups[$i]->date_register;
				$date = explode("-", $date);
				$arrayTempDate[$cont] = $date[2] ."/". $date[1] ."/". $date[0];
				$arrayTempGameCode[$cont] = $registerGroups[$i]->id_group;
				$cont++;			
			}else{
				$objTempInactive = BetGroup::where('id','=',$registerGroups[$i]->id_group)->where('id_state','=',2)->first();
				if($objTempInactive != null){

					$season = DB::table('seasons')			
						->where('id_sport', 1)
						->where('id', $objTempInactive->id_season)
						->first();					

					$arraySeasonInactive[$contInactive] = $season->season;
					$arrayTempInactive[$contInactive] = $objTempInactive;
					if($objTempInactive->users_winner != null){

						$banderaTempWinners = false;
						$objTempWinners = explode(",", $objTempInactive->users_winner);
						
						for ($j=0; $j < count($objTempWinners); $j++) { 
							if($objTempWinners[$j] == auth()->user()->id){
								$arrayStateMatchInactive[$contInactive] = "You Won";
								$banderaTempWinners = true;
								break;
							}
						}						

						if($banderaTempWinners == false){
							$arrayStateMatchInactive[$contInactive] = "You lose";
						}

					}else{
						$arrayStateMatchInactive[$contInactive] = "You lose";
					}				
					$date = $registerGroups[$i]->date_register;
					$date = explode("-", $date);
					$arrayTempDateInactive[$contInactive] = $date[2] ."/". $date[1] ."/". $date[0];
					$arrayTempGameCodeInactive[$contInactive] = $registerGroups[$i]->id_group;
					$contInactive++;					
				}
			}
		}		

		return view('account-overview',
			['actualSeason' => $season, 
			'registerGroups' => $arrayTemp, 
			'registerGroupsDate' => $arrayTempDate, 
			'gameCode' => $arrayTempGameCode, 
			'stateGame' => $arrayStateMatch,
			'seasonInactive' => $arraySeasonInactive,
			'registerGroupsInactive' => $arrayTempInactive,
			'registerGroupsDateInactive' => $arrayTempDateInactive,
			'gameCodeInactive' => $arrayTempGameCodeInactive,
			'stateGameInactive' => $arrayStateMatchInactive]);

	}//Cierra la funcion showAccount

	public function resumeGroupTest(Request $request){

		$id = $request->input('idGroup');
		$season = DB::table('seasons')			
			->where('id_sport', 1)
			->where('id_state', 1)
			->orderBy('id','desc')
			->first();

		$activeBetGroups = DB::table('betgroups')
			->where('id_season', $season->id)
			->where('id_state', 1)
			->orderBy('id','desc')
			->distinct('amount')
			->get();

		$groupDetails = DB::table('betgroups')
			->where('id_season', $season->id)
			->where('id_state', 1)
			->where('id', $id)
			->get();

		$arrayTemp = array();
		$arrayObjTemp = array();
		for ($i=0; $i < count($activeBetGroups); $i++) { 
			if($i == 0){
				array_push($arrayTemp, $activeBetGroups[$i]->amount);
				$arrayObjTemp[$i] = $activeBetGroups[$i];
			}else{

				if(in_array($activeBetGroups[$i]->amount, $arrayTemp)){
					$activeBetGroups[$i] = null;
					$arrayObjTemp[$i] = $activeBetGroups[$i];
				}else{
					array_push($arrayTemp, $activeBetGroups[$i]->amount);
					$arrayObjTemp[$i] = $activeBetGroups[$i];
				}
			}
 		}

 		$arrayObjTemp = array_filter($arrayObjTemp)	;
 		sort($arrayObjTemp);
		$activeBetGroups = $arrayObjTemp;			

		$response = array(
			'actualSeason' => $season->season,
			'activeBetGroups' => $activeBetGroups,
			'groupDetails' => $groupDetails
		);

		$view = View::make('ajaxBetGroups')->with('result',$response);
		$sections = $view->renderSections();
		return response()->json(['result' => $sections['ajaxContent']]);
	
	}//Cierra la funcion resumeGroupTest

	public function processWeek(){

		$this->getTablePositions(); //SE ACTUALIZA LA TABLA DE POSICIONES

		$season = DB::table('seasons')			
			->where('id_sport', 1)
			->where('id_state', 1)
			->orderBy('id','desc')
			->first();

		$betGroups = DB::table('betgroups')
			->where('id_season', $season->id)
			->where('id_state', 1)
			->where('users_winner', null)
			->get();

		for($i=0; $i < count($betGroups); $i++){			
		//SE SELECCIONAN TODOS LOS GRUPOS DE APUESTA DE LA TEMPORADA ACTUAL ACTIVOS

			if(($betGroups[$i]->participants_number == $betGroups[$i]->current_participants) && (($betGroups[$i]->first_match <= $season->current_match) && ($betGroups[$i]->last_match >= $season->current_match))){
				//ENTRA UNICAMENTE SI EL BETGROUP SE COMPLETO CON EL TOTAL DE PARTICIPANTES

				$objDetailGameActual = DB::table('detailgames')
					->where('id_season', $season->id)
					->where('id_betgroup', $betGroups[$i]->id)
					->where('week', $season->current_match)
					->get();

				$cantidadRegistrosGroup = count($objDetailGameActual);

				if($cantidadRegistrosGroup != $betGroups[$i]->participants_number){					
					$userRegisterActive = array();

					for($j=0; $j < $cantidadRegistrosGroup; $j++){//LLENADO DE LOS ID'S QUE SI TIENE REGISTRO EN DETAILGAMES
						$userRegisterActive[$j] = $objDetailGameActual[$j]->id_user;
					}

					$complementSelectionTeam = DB::table('detailbetgroups')
						->where('id_group', $betGroups[$i]->id)
						->whereNotIn('id_user', $userRegisterActive)
						->get();

					for($j=0; $j < count($complementSelectionTeam); $j++){
						$teamsSelected = DB::table('detailgames')
							->where('id_season', $season->id)
							->where('id_user', $complementSelectionTeam[$j]->id_user)
							->orderBy('id','desc')
							->get();

						//SE VALIDA QUE EL ULTIMO REGISTRO DEL EQUIPO SELECCIONADO POR EL USUARIO
						//SEA 'PASS' DE LO CONTRARIO NO ES NECESARIA LA ACCION DE AGREGAR UN REGISTRO
						//DE SELECCION DE EQUIPO EN LA SEMANA ACTUAL DEL GRUPO DE APUESTA INDICADO

						$lastRegisterPosition = count($teamsSelected)-1;
						if($teamsSelected[$lastRegisterPosition] != 'denied'){

							$tableTeamsPosition = DB::table('tablepositions')
								->where('id_season', $season->id)
								->orderBy('id', 'desc')
								->get();

							for($k=0; $k < count($tableTeamsPosition); $k++){
								$teamPivot = DB::table('detailgames')
									->where('id_season')									
									->where('id_betgroup', $betGroups[$i]->id)
									->where('id_user', $complementSelectionTeam[$j]->id_user)
									->where('selection_team', $tableTeamsPosition[$k]->id_team)
									->first();

								if($teamPivot == null){
									$teamPivot = $tableTeamsPosition[$k]->id_team;
									break;
								}
							}

							$idDetailSeason = DB::table('detailseasons')
								->where([
									['id_state', 1],
									['id_season', $season->id],
									['week', $season->current_match],
									['id_home_team', $teamPivot],
								])
								->orWhere([
									['id_state', 1],
									['id_season', $season->id],
									['week', $season->current_match],
									['id_visit_team', $teamPivot],
								])
								->first();

							$newDetailGame = new DetailGame();
							$newDetailGame->id_season = $season->id;
							$newDetailGame->id_detail_season = $idDetailSeason->id;
							$newDetailGame->id_user = $complementSelectionTeam[$j]->id_user;
							$newDetailGame->selection_team = $teamPivot;
							$newDetailGame->state = 'null';
							$newDetailGame->id_betgroup = $betGroups[$i]->id;
							$newDetailGame->week = $season->current_match;
							$newDetailGame->save();


						}//SI LA ULTIMA SELECCION REALIZADA ES DENIED NO ES NECESARIO GENERAR UN REGISTRO
						//DEL USUARIO ACTUAL EN LA SEMANA ACTUAL

					}//SE RECORREN TODOS LOS USUARIOS QUE HACE FALTA COMPLEMENTAR LA SELECCION DE EQUIPO
					//EN LA SEMANA ACTUAL EN EL GRUPO DE APUESTA RECORRIDO INDICADO

				}//SI LA CANTIDAD DE REGISTROS EN DETAILGAMES SON IGUALES AL TOTAL DE PARTICIPANTES 
				//NO ES NECESARIO COMPLEMENTAR LA SELECCION DE NINGUN USUARIO DE LO CONTRARIO ENTRA AL IF

			}elseif(($betGroups[$i]->participants_number != $betGroups[$i]->current_participants) && ($betGroups[$i]->current_participants > 0)) {
			//ENTRA SIEMPRE Y CUANDO AL EVALUAR EL GRUPO DE APUESTA ESTE NO ESTE COMPLETO PERO
			//EXISTA EL REGISTRO DE POR LO MENOS UN USUARIO, EN ESTA PARTE SE ELIMINARÁ EL REGISTRO
			//DE DETAILGAMES SÍ ES QUE EL USUARIO ELIGIO EQUIPO ANTES DE QUE SE LLENASE EL BET GROUP

				DB::table('detailgames')
					->where('id_season', $season->id)
					->where('id_betgroup', $betGroups[$i]->id)
					->where('week', $season->current_match)
					->where('state', 'null')
					->delete();				
			}

		}

		//AQUI SE DEBE DE ACTUALIZAR LA TABLA DETAILSEASONS DESDE LA API CON LOS RESULTADOS DE LA SEMANA ACTUAL
		$this->updateDetailSeason();

		$detailGamesActualWeek = DetailGame::where('id_season', $season->id)
			->where('week', $season->current_match)
			->where('state','null')
			->get();		

		for($i=0; $i < count($detailGamesActualWeek); $i++){
			$typeTeam = 0;
			$teamSelected = $detailGamesActualWeek[$i]->selection_team;
			$selectedGroup = $detailGamesActualWeek[$i]->id_betgroup;

			$seasonDetail = DB::table('detailseasons')
				->where([
					['week', '=' , $season->current_match],
					['id_home_team', '=' ,$teamSelected],
					['id_season', '=' ,$season->id],
				])
				->orWhere([
					['week', '=' , $season->current_match],
					['id_visit_team', '=' ,$teamSelected],
					['id_season', '=' ,$season->id],
				])
				->first();

			if($teamSelected == $seasonDetail->id_home_team){
				$typeTeam = 1;
			}elseif($teamSelected == $seasonDetail->id_visit_team){
				$typeTeam = 2;
			}

			if(($seasonDetail->result_home > $seasonDetail->result_visit) && ($typeTeam == 1)){
				$detailGamesActualWeek[$i]->state = 'pass';
			}elseif(($seasonDetail->result_home < $seasonDetail->result_visit) && ($typeTeam == 2)){
				$detailGamesActualWeek[$i]->state = 'pass';
			}elseif(($seasonDetail->result_home > $seasonDetail->result_visit) && ($typeTeam == 2)){
				$detailGamesActualWeek[$i]->state = 'denied';
			}elseif(($seasonDetail->result_home < $seasonDetail->result_visit) && ($typeTeam == 1)){
				$detailGamesActualWeek[$i]->state = 'denied';
			}elseif($seasonDetail->result_home == $seasonDetail->result_visit){
				$detailGamesActualWeek[$i]->state = 'denied';
			}else{
				$detailGamesActualWeek[$i]->state = 'null';
			}			
			$detailGamesActualWeek[$i]->save();
		}

		//SACAR EL GANADOR DE CADA BETGROUP SÍ ES QUE LO HUBIESE, RECORRER EL RANGO DE LAS SEMANAS
		//AL REVES DESDE LA ULTIMA A LA PRIMERA	

		$betGroups = DB::table('betgroups')
			->where('id_season', $season->id)
			->where('id_state', 1)
			->where('users_winner', null)
			->get();

		for($i=0; $i < count($betGroups); $i++){					

			if($betGroups[$i]->participants_number == $betGroups[$i]->current_participants){
				$firstMatch = $betGroups[$i]->first_match;
				$lastMatch = $betGroups[$i]->last_match;

				$usersRegisterGroup = DB::table('detailbetgroups')
					->where('id_group', $betGroups[$i]->id)
					->select('id_user')
					->get();

				$arrayResultWinners = array();
				$arrayResultWinnersDenied = array();
				$bandera = false;
				$banderaPivotDenied = false;

				for($j=$lastMatch; $j >= 1; $j--){

					if($j == $lastMatch){ //FIRST CASE

						for($k=0; $k < count($usersRegisterGroup); $k++){

							$validarSelection = DB::table('detailgames')
								->where('id_betgroup', $betGroups[$i]->id)
								->where('id_season', $season->id)
								->where('id_user', $usersRegisterGroup[$k]->id_user)
								->where('week', $j)
								->first();

							if($validarSelection != null){
			
								if($validarSelection->state == "pass"){
									array_push($arrayResultWinners, $validarSelection->id_user);
									$bandera = true;
								}elseif($validarSelection->state == "denied"){//SI EL ESTADO DEL ULTIMO REGISTRO ENCONTRADO
									array_push($arrayResultWinnersDenied, $validarSelection->id_user);
								}

							}//EVALUA SI HAY SELEECION DE ALGUN USUARIO EN LA SEMANA CORRESPONDIENTE

						}//RECORRE TODO EL PULL DE USUARIOS DE DICHO BETGROUP

						if($bandera == true){
							break; //SALE DEL RECORRIDO DE LAS SEMANAS PARA AVANZAR AL SIGUIENTE BETGROUP
						}else{
							if(count($arrayResultWinnersDenied) > 0){
								$banderaPivotDenied = true;
								break; //SALE DEL RECORRIDO DE LAS SEMANAS PARA AVANZAR AL SIGUIENTE BETGROUP
							}
						}						
					}elseif($j < $lastMatch){//CIERRA EL PRIMER CASO CUANDO EL JUEGO LLEGO HASTA LA ULTIMA SEMANA

						for($k=0; $k < count($usersRegisterGroup); $k++){

							$validarSelection = DB::table('detailgames')
								->where('id_betgroup', $betGroups[$i]->id)
								->where('id_season', $season->id)
								->where('id_user', $usersRegisterGroup[$k]->id_user)
								->where('week', $j)
								->first();

							if($validarSelection != null){
			
								if($validarSelection->state == "pass"){
									array_push($arrayResultWinners, $validarSelection->id_user);
								}elseif($validarSelection->state == "denied"){//SI EL ESTADO DEL ULTIMO REGISTRO ENCONTRADO
									array_push($arrayResultWinnersDenied, $validarSelection->id_user);
								}

							}//EVALUA SI HAY SELEECION DE ALGUN USUARIO EN LA SEMANA CORRESPONDIENTE

						}//RECORRE TODO EL PULL DE USUARIOS DE DICHO BETGROUP

						if(count($arrayResultWinners) == 1){
							$bandera = true;
							break; //SALE DEL RECORRIDO DE LAS SEMANAS PARA AVANZAR AL SIGUIENTE BETGROUP
						}else{
							if((count($arrayResultWinnersDenied) > 0) && (count($arrayResultWinners) == 0)){
								$banderaPivotDenied = true;
								break; //SALE DEL RECORRIDO DE LAS SEMANAS PARA AVANZAR AL SIGUIENTE BETGROUP
							}
						}									

					}//CIERRA TODOS LOS DEMAS CASOS CUANDO EL JUEGO LLEGO HASTA ALGUNA SEMANA ANTES DE LA ULTIMA

				}//CIERRA EL FOR QUE RECORRE TODAS LAS SEMANAS DEL BETGROUP DE MANERA INVERSA

				if($bandera == true) {

					$cadenaWinners = "";
					for ($m=0; $m < count($arrayResultWinners); $m++) { 
						if($m == 0){
							$cadenaWinners = $arrayResultWinners[$m];
						}else{
							$cadenaWinners = $cadenaWinners .",". $arrayResultWinners[$m];
						}						
					}

					$updateBetGroupActual = DB::table('betgroups')
						->where('id', $betGroups[$i]->id)
						->update([
							'users_winner' => $cadenaWinners,
							'id_state' => 2,
							]);

					$getPrice = DB::table('betgroups')			
						->where('id',  $betGroups[$i]->id)
						->first();

					$price = ((($getPrice->participants_number)*($getPrice->amount))*0.85);
					$price = ($price / (count($arrayResultWinners)));
					$price = round($price, 2);

					for ($m=0; $m < count($arrayResultWinners); $m++) { 
						$getWinners = DB::table('users')			
							->where('id',  $arrayResultWinners[$m])
							->first();
						$getWinners->balance = $getWinners->balance + $price;
						$getWinners->save();
					}					

				}elseif($banderaPivotDenied == true){

					$cadenaWinners = "";
					for ($m=0; $m < count($arrayResultWinnersDenied); $m++) { 
						if($m == 0){
							$cadenaWinners = $arrayResultWinnersDenied[$m];
						}else{
							$cadenaWinners = $cadenaWinners .",". $arrayResultWinnersDenied[$m];
						}						
					}

					$updateBetGroupActual = DB::table('betgroups')
						->where('id', $betGroups[$i]->id)
						->update([
							'users_winner' => $cadenaWinners,
							'id_state' => 2,
							]);

					$getPrice = DB::table('betgroups')			
						->where('id',  $betGroups[$i]->id)
						->first();

					$price = ((($getPrice->participants_number)*($getPrice->amount))*0.85);
					$price = ($price / (count($arrayResultWinnersDenied)));
					$price = round($price, 2);

					for ($m=0; $m < count($arrayResultWinnersDenied); $m++) { 
						$getWinners = DB::table('users')			
							->where('id',  $arrayResultWinnersDenied[$m])
							->first();
						$getWinners->balance = $getWinners->balance + $price;
						$getWinners->save();
					}												
				}

			}//CIERRA LA EVALUACION DE QUE EL GRUPO A ANALIZAR ESTA LLENO				

		}	


		$currentWeek = ($season->current_match) + 1;
		$season = DB::table('seasons')
			->where('id', $season->id)
			->update(['current_match' => $currentWeek]);  

		return redirect('/betgroups');
	}

	public function storeDepositLimit(Request $request){

		$fecha = date("Y-m-d");

		$user = DB::table('users')
			->where('id', auth()->user()->id)
			->update(['deposit_limit' => $request->depositlimit, 
				'date_register_deposit_limit' => $fecha]);

		Flash::success("<h4> The configuration about your deposit limit was done successfully! </h4>");
		return redirect('/account');
	}

	public function updateDetailSeason(){

		$season = DB::table('seasons')			
			->where('id_sport', 1)
			->where('id_state', 1)
			->orderBy('id','desc')
			->first();		

	    $uri = 'http://api.football-data.org/v1/competitions/'.$season->id_api_season.'/fixtures';
	    $reqPrefs['http']['method'] = 'GET';
	    $reqPrefs['http']['header'] = 'X-Auth-Token: 1f459607a43b4a9cb9d0063bd054a279';
	    $stream_context = stream_context_create($reqPrefs);
	    $response = file_get_contents($uri, false, $stream_context);
	    $fixtures = json_decode($response);

	    $countMatch = $fixtures->count;
	    $arrayDetailSeason = $fixtures->fixtures;	  	    

	    for ($i=0; $i < $countMatch; $i++) {

	    	if($arrayDetailSeason[$i]->matchday == $season->current_match){
		    	$homeTeam = DB::table('teams')->where('team', $arrayDetailSeason[$i]->homeTeamName)->first();
		    	$visitTeam = DB::table('teams')->where('team', $arrayDetailSeason[$i]->awayTeamName)->first();

		    	$objDetailSeason = DB::table('detailseasons')
		    		->where('id_home_team', $homeTeam->id)
		    		->where('id_visit_team', $visitTeam->id)
		    		->where('week', $arrayDetailSeason[$i]->matchday)
		    		->where('id_season', $season->id)
		    		->where('id_state', 1)
		    		->update([
		    			'result_home' => $arrayDetailSeason[$i]->result->goalsHomeTeam,
		    			'result_visit' => $arrayDetailSeason[$i]->result->goalsAwayTeam,
		    			'state_match' => $arrayDetailSeason[$i]->status
		    			]);  	    		    		
	    	} 

	    }
	}

}//Cierra el controlador
