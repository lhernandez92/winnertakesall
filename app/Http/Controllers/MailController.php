<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Mail;
use Session;
use Redirect;
use App\Http\Requests;

class MailController extends Controller
{
    public function sendContactEmail(Request $request){
    	Mail::send('emails.contact', $request->all(), function($msj){
    		$msj->subject('WinnerTakesAll Account Activation Required');
    		$msj->to('luisf.hernandez92@gmail.com');    		
    	});
    	return redirect('/');
    }
}
