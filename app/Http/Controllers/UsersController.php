<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use Laracasts\Flash\Flash;
use DB;
use Mail;
use Session;
use Redirect;
use Auth;
use Validator;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class UsersController extends Controller
{
    //

    public function store(Request $request){  
        //SE VALIDA SI EL EMAIL INGRESADO YA EXISTE
        $validarEmail = false;
        $usuarios = DB::table('users')->get();
        for ($i=0; $i < count($usuarios); $i++) { 
            if($usuarios[$i]->email == $request->input('email')){
                $validarEmail = true;
                break;
            }
        }

        $request->flash();

        if($request->input('state_id') != null){
            //LLENADO DE DATOS EN EL OBJETO TEMPORAL $tempUser
            $catchResult;
        	$tempUser = new User($request->all());   	
        	$tempUser->title = $request->input('title');
        	$tempUser->name = $request->input('name');
        	$tempUser->surname = $request->input('surname');
        	$birthDate = $request->input('year')."-".$request->input('month')."-".$request->input('day');
        	$tempUser->birth_date = Date("Y-m-d",strtotime($birthDate));
        	$tempUser->email = $request->input('email');
        	$tempUser->phone_number = $request->input('phone_number');
        	$tempUser->country = $request->input('country');
        	$tempUser->username = $request->input('username');
        	$tempUser->password = bcrypt($request->input('password'));
        	$tempUser->security_question = $request->input('security_question');
        	$tempUser->security_answer = $request->input('security_answer');
        	$tempUser->user_type = "member";
        	$tempUser->state_id = 1;
            $tempUser->balance = 150;

            //PATRONES DE VALIDACION
            $email = "/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/";
            $phone = "/^[[:digit:]]+$/";

            //VALIDACIONES
            if(($tempUser->name == "") || (strlen($tempUser->name) < 1)){
                Flash::error("<h5> Error! The Field: NAME must have at least 4 letters.</h5>");    
            }elseif(($tempUser->surname == "") || (strlen($tempUser->surname) < 1)){
                Flash::error("<h5> Error! The Field: SURNAME must have at least 4 letters.</h5>");
            }elseif(($request->input('year') == "-1") || ($request->input('month') == "-1") || ($request->input('day') == "-1")){
                Flash::error("<h5> Error! The Field: DATE OF BIRTH is not a valid date.</h5>");
            }elseif(!preg_match($email, $tempUser->email)){
                Flash::error("<h5> Error! The Field: EMAIL is not valid.</h5>");
            }elseif($validarEmail == true){
                Flash::error("<h5> Error! There is a Member registered using that Email.</h5>");
            }elseif(!preg_match($phone, $tempUser->phone_number)){
                Flash::error("<h5> Error! The Field: PHONE NUMBER must be only digits.</h5>");
            }elseif($tempUser->country == ""){
                Flash::error("<h5> Error! The Field: COUNTRY is not valid.</h5>");
            }elseif($tempUser->username == ""){
                Flash::error("<h5> Error! The Field: USERNAME must not be in blank.</h5>");
            }elseif(strlen($tempUser->username) < 5){
                Flash::error("<h5> Error! The Field: USERNAME must have at least 5 letters.</h5>");
            }elseif(strlen($tempUser->password) < 6){
                Flash::error("<h5> Error! The Field: PASSWORD must have at least 6 letters.</h5>");
            }elseif($request->input('password') == "confirmpassword123!"){
                Flash::error("<h5> Error! The Fields: PASSWORD and CONFIRM PASSWORD does not match.</h5>");
            }elseif($tempUser->security_answer == ""){
                Flash::error("<h5> Error! The Field: SECURTY ANSWER must not be in blank.</h5>");
            }else{    
                                       
                if($tempUser->save()){
                    if(Auth::attempt(['email' => $tempUser->email, 'password' => $request->input('password')])) {
                        Mail::send('emails.contact', ['tempUser' => $tempUser], function($msj) use ($tempUser){
                            $msj->subject('WinnerTakesAll Account Activation Required');
                            $msj->to($tempUser->email);            
                        });                                    
                        Flash::success("<h5>Congratulations! Now, you are part of Winner Takes All, check your email account.</h5>"); 
                    }else{
                        Flash::success("<h5>Congratulations! The user was registered but not logged, try manually login.</h5>"); 
                    }

                    return Redirect::route('registernow');
                }else{
                    Flash::error("<h5>Error: The user can't be registered.</h5>");
                }

            }
                        
            return Redirect::route('registernow')->withInput($request->except('password'));

        }else{
            Flash::error("<h5>Error: Please! confirm your age is over 18 years.</h5>");
            return Redirect::route('registernow')->withInput($request->except('password'));
        }
    }//CIERRA LA FUNCIÓN STORE DEL CONTROLADOR        

    public function redirIndex(){
        return redirect('/');
    }
}
