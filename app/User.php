<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id' ,'title','name', 'surname', 'birth_date', 'email', 'phone_number', 
        'country', 'username', 'security_question', 'security_answer', 'user_type', 'state_id',
        'deposit_limit', 'date_register_deposit_limit', 'balance',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 
    ];

    public function state(){
        return $this->belongsTo('App\State');
    }    
}
