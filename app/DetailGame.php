<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailGame extends Model
{
	protected $table = 'detailgames';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'id_season', 'id_detail_season', 'id_user', 'selection_team', 'state', 'id_betgroup', 'week',
     ];
  
    public function user(){
        return $this->belongsTo('App\User');
    }    
    public function season(){
        return $this->belongsTo('App\Season');
    }
    public function detailseason(){
        return $this->belongsTo('App\DetailSeason');
    }    
    public function betgroup(){
        return $this->belongsTo('App\BetGroup');
    }    
}
