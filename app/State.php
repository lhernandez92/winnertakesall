<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
	protected $table = 'states';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id' ,'name', 
     ];

    public function users(){
        return $this->hasMany('App\User');
    }
    public function betgroups(){
        return $this->hasMany('App\BetGroup');
    }
    public function seasons(){
        return $this->hasMany('App\Season');
    }
    public function detailseasons(){
        return $this->hasMany('App\DetailSeason');
    }        
}
