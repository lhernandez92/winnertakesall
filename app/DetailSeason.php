<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Team;

class DetailSeason extends Model
{
	protected $table = 'detailseasons';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'week', 'id_home_team', 'id_visit_team', 'result_home', 'result_visit', 
        'id_season', 'id_state', 'state_match', 'date', 'hour',
     ];
     
    public function detailgames(){
        return $this->hasMany('App\DetailGame');
    }    
    public function state(){
        return $this->belongsTo('App\State');
    }
    public function team(){
        return $this->belongsTo('App\Team');
    }
    public function season(){
        return $this->belongsTo('App\Season');
    }
    public function name_team($id){
        $test = Team::find($id);
        return $test->team;
    }
}
